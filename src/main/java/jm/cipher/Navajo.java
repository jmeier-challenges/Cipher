package jm.cipher;

import java.util.HashMap;
import java.util.Map;

/**
 * User: johann
 * Date: 03.10.12
 * Time: 14:32
 *
 * http://www.kryptographiespielplatz.de/?aG=ee2f58553fe73d72eeaad6c222bc5b2cb8acb03f
 */
public class Navajo extends AbstractCipher {
    private Map<String, String> navajoToEnglish;

    public static void main(String[] args) throws Exception {
        Navajo cipher = new Navajo();
        String ct = "GLOE-IH A-KHA DIBEH-YAZZIE CHUO KLIZZIE BE-LA-SANA TSAH AH-TAD BE-TKAH A-CHI DIBEH THAN-ZIE BE-TKAH AH-NAH YEH-HES TSAH BE-TKAH A-WOH A-KHA DIBEH-YAZZIE DIBEH-YAZZIE DZEH AH-LOSZ BE-TKAH BA-AH-NE-DI-TININ AH-JAH GAH NASH-DOIE-TSO";
        System.out.println(cipher.decrypt(ct));
    }

    public Navajo() {
        navajoToEnglish = new HashMap<String, String>();
        initWords();
        initChars();
    }

    @Override
    public String encrypt(String pt) throws Exception {
        return "not implemented";
    }

    @Override
    public String decrypt(String ct) throws Exception {
        StringBuilder pt = new StringBuilder();
        String[] lines = ct.split("\n");
        for (String line : lines) {
            String[] words = line.split(" ");
            for (String word : words) {
                if (navajoToEnglish.containsKey(word)) {
                    pt.append(navajoToEnglish.get(word));
                } else {
                    pt.append(word);
                }
            }
            pt.append("\n");
        }
        return pt.toString();
    }

    @Override
    public String getDisplayname() {
        return "Navajo";
    }

    @Override
    public String getName() {
        return "Navajo";
    }

    @Override
    public String getDescription() {
        return "Navajo wurde von den Amerikanern im 2. Weltkrieg benutzt.\n Space ist der Trenner der einzelnen Navajowoertern";
    }

    @Override
    public boolean canEncrypt() {
        return false;
    }

    void initWords() {
        navajoToEnglish.put("DIN-NEH-IH", "CORPS");
        navajoToEnglish.put("ASHIH-HI", "DIVISION");
        navajoToEnglish.put("TABAHA", "REGIMENT");
        navajoToEnglish.put("TACHEENE", "BATTALION");
        navajoToEnglish.put("NAKIA", "COMPANY");
        navajoToEnglish.put("HAS-CLISH-NIH", "PLATOON");
        navajoToEnglish.put("YO-IH", "SECTION");
        navajoToEnglish.put("DEBEH-LI-ZINI", "SQUAD");
        navajoToEnglish.put("BIH-KEH-HE", "COMMANDING GEN.");
        navajoToEnglish.put("SO-NA-KIH", "MAJOR GEN.");
        navajoToEnglish.put("SO-A-LA-IH", "BRIGADIER GEN.");
        navajoToEnglish.put("ATSAH-BESH-LE-GAI", "COLONEL");
        navajoToEnglish.put("CHE-CHIL-BE-TAH-BESH-LEGAI", "LT. COLONEL");
        navajoToEnglish.put("CHE-CHIL-BE-TAH-OLA", "MAJOR");
        navajoToEnglish.put("BESH-LEGAI-NAH-KIH", "CAPTAIN");
        navajoToEnglish.put("BESH-LEGAI-A-LAH-IH", "LIEUTENANT");
        navajoToEnglish.put("HASH-KAY-GI-NA-TAH", "COMMANDING OFFICER");
        navajoToEnglish.put("BIH-DA-HOL-NEHI", "EXECUTIVE OFFICER");
        navajoToEnglish.put("ZHIN-NI", "AFRICA");
        navajoToEnglish.put("BEH-HGA", "ALASKA");
        navajoToEnglish.put("NE-HE-MAH", "AMERICA");
        navajoToEnglish.put("CHA-YES-DESI", "AUSTRALIA");
        navajoToEnglish.put("TOH-TA", "BRITAIN");
        navajoToEnglish.put("CEH-YEHS-BESI", "CHINA");
        navajoToEnglish.put("DA-GHA-HI", "FRANCE");
        navajoToEnglish.put("BESH-BE-CHA-HE", "GERMANY");
        navajoToEnglish.put("TKIN-KE-YAH", "ICELAND");
        navajoToEnglish.put("AH-LE-GAI", "INDIA");
        navajoToEnglish.put("DOH-HA-CHI-YALI-TCHI", "ITALY");
        navajoToEnglish.put("BEH-NA-ALI-TSOSIE", "JAPAN");
        navajoToEnglish.put("KE-YAH-DA-NA-LHE", "PHILIPPINE");
        navajoToEnglish.put("SILA-GOL-CHI-IH", "RUSSIA");
        navajoToEnglish.put("SHA-DE-AH-NE-HI-MAH", "SOUTH AMERICA");
        navajoToEnglish.put("DEBA-DE-NIH", "SPAIN");
        navajoToEnglish.put("WO-TAH-DE-NE-IH", "PLANES");
        navajoToEnglish.put("GINI", "DIVE BOMBER");
        navajoToEnglish.put("TAS-CHIZZIE", "TORPEDO PLANE");
        navajoToEnglish.put("NE-AS-JAH", "OBS. PLAN");
        navajoToEnglish.put("DA-HE-TIH-HI", "FIGHTER PLANE");
        navajoToEnglish.put("JAY-SHO", "BOMBER PLANE");
        navajoToEnglish.put("GA-GIH", "PATROL PLANE");
        navajoToEnglish.put("ATSAH", "TRANSPORT");
        navajoToEnglish.put("TOH-DINEH-IH", "SHIPS");
        navajoToEnglish.put("LO-TSO", "BATTLESHIP");
        navajoToEnglish.put("TSIDI-MOFFA-YE-HI", "AIRCRAFT CARRIER");
        navajoToEnglish.put("BESH-LO", "SUBMARINE");
        navajoToEnglish.put("CHA", "MINE SWEEPER");
        navajoToEnglish.put("CA-LO", "DESTROYER");
        navajoToEnglish.put("DINEH-NAY-YE-HI", "TRANSPORT");
        navajoToEnglish.put("LO-TSO-YAZZIE", "CRUISER");
        navajoToEnglish.put("TSE-E", "MOSQUITO BOAT");
        navajoToEnglish.put("ATSAH-BE-YAZ", "JANUARY");
        navajoToEnglish.put("WOZ-CHEIND", "FEBRUARY");
        navajoToEnglish.put("TAH-CHILL", "MARCH");
        navajoToEnglish.put("TAH-TSO", "APRIL");
        navajoToEnglish.put("TAH-TSOSIE", "MAY");
        navajoToEnglish.put("BE-NE-EH-EH-JAH-TSO", "JUNE");
        navajoToEnglish.put("BE-NE-TA-TSOSIE", "JULY");
        navajoToEnglish.put("BE-NEEN-TA-TSO", "AUGUST");
        navajoToEnglish.put("GHAW-JIH", "SEPTEMBER");
        navajoToEnglish.put("NIL-CHI-TSOSIE", "OCTOBER");
        navajoToEnglish.put("NIL-CHI-TSO", "NOVEMBER");
        navajoToEnglish.put("YAS-NIL-TES", "DECEMBER");
        navajoToEnglish.put("YE-TSAN", "ABANDON");
        navajoToEnglish.put("WOLA-CHI-A-MOFFA-GAHN", "ABOUT");
        navajoToEnglish.put("WOLA-CHEE-BE-YIED", "ABREAST");
        navajoToEnglish.put("UL-SO", "ACCOMPLISH");
        navajoToEnglish.put("BE-KA-HO", "ACCORDING");
        navajoToEnglish.put("HANOT-DZIED", "ACKNOWLEDGE");
        navajoToEnglish.put("AH-HA-TINH", "ACTION");
        navajoToEnglish.put("AH-HA-TINH-Y", "ACTIVITY");
        navajoToEnglish.put("BEH-GHA", "ADEQUATE");
        navajoToEnglish.put("IH-HE-DE-NDEL", "ADDITION");
        navajoToEnglish.put("YI-CHIN-HA-TSE", "ADDRESS");
        navajoToEnglish.put("BE-GAHI", "ADJACENT");
        navajoToEnglish.put("HAS-TAI-NEL-KAD", "ADJUST");
        navajoToEnglish.put("NAS-SEY", "ADVANCE");
        navajoToEnglish.put("NA-NETIN", "ADVISE");
        navajoToEnglish.put("BE-ZONZ", "AERIAL");
        navajoToEnglish.put("LANH", "AFFIRMATIVE");
        navajoToEnglish.put("BI-KHA-DI", "AFTER");
        navajoToEnglish.put("BE-NA-GNISH", "AGAINST");
        navajoToEnglish.put("EDA-ELE-TSOOD", "AID");
        navajoToEnglish.put("NILCHI", "AIR");
        navajoToEnglish.put("NILCHI-BEGHAN", "AIRDOME");
        navajoToEnglish.put("HA-IH-DES-EE", "ALERT");
        navajoToEnglish.put("TA-A-TAH", "ALL");
        navajoToEnglish.put("NIH-HI-CHO", "ALLIES");
        navajoToEnglish.put("WOLACHEE-SNEZ", "ALONG");
        navajoToEnglish.put("EH-DO", "ALSO");
        navajoToEnglish.put("NA-KEE-GO-NE-NAN-DEY-HE", "ALTERNATE");
        navajoToEnglish.put("KHAC-DA", "AMBUSH");
        navajoToEnglish.put("BEH-ELI-DOH-BE-CAH-ALI-TAS-AI", "AMMUNITION");
        navajoToEnglish.put("CHAL", "AMPHIBIOUS");
        navajoToEnglish.put("DO", "AND");
        navajoToEnglish.put("DEE-CAHN", "ANGLE");
        navajoToEnglish.put("IH-NAY-TANI", "ANNEX");
        navajoToEnglish.put("BEH-HA-O-DZE", "ANNOUNCE");
        navajoToEnglish.put("WOL-LA-CHEE-TSIN", "ANTI");
        navajoToEnglish.put("NI-JOL-LIH", "ANTICIPATE");
        navajoToEnglish.put("TAH-HA-DAH", "ANY");
        navajoToEnglish.put("YE-KA-HA-YA", "APPEAR");
        navajoToEnglish.put("BI-CHI-OL-DAH", "APPROACH");
        navajoToEnglish.put("TO-KUS-DAN", "APPROXIMATE");
        navajoToEnglish.put("GAH-TSO", "ARE");
        navajoToEnglish.put("HAZ-A-GIH", "AREA");
        navajoToEnglish.put("BESH-YE-HA-DA-DI-TEH", "ARMOR");
        navajoToEnglish.put("LEI-CHA-IH-YIL-KNEE-IH", "ARMY");
        navajoToEnglish.put("IL-DAY", "ARRIVE");
        navajoToEnglish.put("BE-AL-DOH-TSO-LANI", "ARTILLERY");
        navajoToEnglish.put("AHCE", "AS");
        navajoToEnglish.put("ALTSEH-E-JAH-HE", "ASSAULT");
        navajoToEnglish.put("DE-JI-KASH", "ASSEMBLE");
        navajoToEnglish.put("BAH-DEH-TAHN", "ASSIGN");
        navajoToEnglish.put("AH-DI", "AT");
        navajoToEnglish.put("AL-TAH-JE-JAY", "ATTACK");
        navajoToEnglish.put("BO-O-NE-TAH", "ATTEMPT");
        navajoToEnglish.put("GIHA", "ATTENTION");
        navajoToEnglish.put("HANI-BA-AH-HO-ZIN", "AUTHENTICATOR");
        navajoToEnglish.put("BE-BO-HO-SNEE", "AUTHORIZE");
        navajoToEnglish.put("TA-SHOZ-TEH-IH", "AVAILABLE");
        navajoToEnglish.put("KLAILH", "BAGGAGE");
        navajoToEnglish.put("NE-TAH", "BANZAI");
        navajoToEnglish.put("BESH-NA-ELT", "BARGE");
        navajoToEnglish.put("BESH-BA-WA-CHIND", "BARRAGE");
        navajoToEnglish.put("BIH-CHAN-NI-AH", "BARRIER");
        navajoToEnglish.put("BIH-TSEE-DIH", "BASE");
        navajoToEnglish.put("BIH-BE-AL-DOH-TKA-IH", "BATTERY");
        navajoToEnglish.put("DA-AH-HI-DZI-TSIO", "BATTLE");
        navajoToEnglish.put("TOH-AH-HI-GHINH", "BAY");
        navajoToEnglish.put("AH-ZHOL", "BAZOOKA");
        navajoToEnglish.put("TSES-NAH", "BE");
        navajoToEnglish.put("TAH-BAHN", "BEACH");
        navajoToEnglish.put("TSES-NAH-NES-CHEE", "BEEN");
        navajoToEnglish.put("BIH-TSE-DIH", "BEFORE");
        navajoToEnglish.put("HA-HOL-ZIZ", "BEGIN");
        navajoToEnglish.put("TSES-NAH-SNEZ", "BELONG");
        navajoToEnglish.put("BI-TAH-KIZ", "BETWEEN");
        navajoToEnglish.put("BILH-LA-DI", "BEYOND");
        navajoToEnglish.put("EHL-NAS-TEH", "BIVOUAC");
        navajoToEnglish.put("A-YE-SHI", "BOMB");
        navajoToEnglish.put("DINEH-BA-WHOA-BLEHI", "BOOBY TRAP");
        navajoToEnglish.put("YE-CHIE-TSAH", "BORNE");
        navajoToEnglish.put("KA-YAH-BI-NA-HAS-DZOH", "BOUNDARY");
        navajoToEnglish.put("DOLA-ALTH-WHOSH", "BULL DOZER");
        navajoToEnglish.put("TSAS-KA", "BUNKER");
        navajoToEnglish.put("NEH-DIH", "BUT");
        navajoToEnglish.put("BE-GHA", "BY");
        navajoToEnglish.put("BESH-LKOH", "CABLE");
        navajoToEnglish.put("NAHL-KIHD", "CALIBER");
        navajoToEnglish.put("TO-ALTSEH-HOGAN", "CAMP");
        navajoToEnglish.put("DI-NES-IH", "CAMOUFLAGE");
        navajoToEnglish.put("YAH-DI-ZINI", "CAN");
        navajoToEnglish.put("BE-AL-DOH-TSO-DEY-DIL-DON-IGI", "CANNONEER");
        navajoToEnglish.put("BE-NEL-AH", "CAPACITY");
        navajoToEnglish.put("YIS-NAH", "CAPTURE");
        navajoToEnglish.put("YO-LAILH", "CARRY");
        navajoToEnglish.put("BIT-SAH", "CASE");
        navajoToEnglish.put("BIH-DIN-NE-DEY", "CASUALTY");
        navajoToEnglish.put("BI-NIH-NANI", "CAUSE");
        navajoToEnglish.put("TSA-OND", "CAVE");
        navajoToEnglish.put("DA-TEL-JAY", "CEILING");
        navajoToEnglish.put("JISH-CHA", "CEMETARY");
        navajoToEnglish.put("ULH-NE-IH", "CENTER");
        navajoToEnglish.put("THLA-GO-A-NAT-ZAH", "CHANGE");
        navajoToEnglish.put("HA-TALHI-YAZZIE", "CHANNEL");
        navajoToEnglish.put("AH-TAH-GI-JAH", "CHARGE");
        navajoToEnglish.put("TA-NEE", "CHEMICAL");
        navajoToEnglish.put("NAS-PAS", "CIRCLE");
        navajoToEnglish.put("AH-HEH-HA-DAILH", "CIRCUIT");
        navajoToEnglish.put("ALTH-AH-A-TEH", "CLASS");
        navajoToEnglish.put("YO-AH-HOL-ZHOD", "CLEAR");
        navajoToEnglish.put("TSE-YE-CHEE", "CLIFF");
        navajoToEnglish.put("UL-CHI-UH-NAL-YAH", "CLOSE");
        navajoToEnglish.put("TA-BAS-DSISSI", "COAST GUARD");
        navajoToEnglish.put("YIL-TAS", "CODE");
        navajoToEnglish.put("NAKI-ALH--DEH-DA-AL-ZHIN", ":");
        navajoToEnglish.put("ALTH-KAY-NE-ZIH", "COLUMN");
        navajoToEnglish.put("DA-AH-HI-JIH-GANH", "COMBAT");
        navajoToEnglish.put("AL-TKAS-EI", "COMBINATION");
        navajoToEnglish.put("HUC-QUO", "COME");
        navajoToEnglish.put("TSA-NA-DAHL", ",");
        navajoToEnglish.put("NAI-EL-NE-HI", "COMMERCIAL");
        navajoToEnglish.put("HUC-QUO-LA-JISH", "COMMIT");
        navajoToEnglish.put("HA-NEH-AL-ENJI", "COMMUNICATION");
        navajoToEnglish.put("BE-KI-ASZ-JOLE", "CONCEAL");
        navajoToEnglish.put("TA-LA-HI-JIH", "CONCENTRATION");
        navajoToEnglish.put("WHE-HUS-DIL", "CONCUSSION");
        navajoToEnglish.put("AH-HO-TAI", "CONDITION");
        navajoToEnglish.put("BE-KE-YA-TI", "CONFERENCE");
        navajoToEnglish.put("NA-NIL-IN", "CONFIDENTIAL");
        navajoToEnglish.put("TA-A-NEH", "CONFIRM");
        navajoToEnglish.put("A-KEH-DES-DLIN", "CONQUER");
        navajoToEnglish.put("NE-TSA-CAS", "CONSIDER");
        navajoToEnglish.put("BILH", "CONSIST");
        navajoToEnglish.put("AH-HIH-HI-NIL", "CONSOLIDATE");
        navajoToEnglish.put("AHL-NEH", "CONSTRUCT");
        navajoToEnglish.put("AH-HI-DI-DAIL", "CONTACT");
        navajoToEnglish.put("TA-YI-TEH", "CONTINUE");
        navajoToEnglish.put("NAI-GHIZ", "CONTROL");
        navajoToEnglish.put("TKAL-KAH-O-NEL", "CONVOY");
        navajoToEnglish.put("BEH-EH-HO-ZIN-NA-AS-DZOH", "COORDINATE");
        navajoToEnglish.put("WOLTAH-AL-KI-GI-JEH", "COUNTER ATTACK");
        navajoToEnglish.put("CO-JI-GOH", "COURSE");
        navajoToEnglish.put("AH-TOH", "CRAFT");
        navajoToEnglish.put("TOH-NIL-TSANH", "CREEK");
        navajoToEnglish.put("AL-N-AS-DZOH", "CROSS");
        navajoToEnglish.put("SHUSH-YAHZ", "CUB");
        navajoToEnglish.put("US-DZOH", "-");
        navajoToEnglish.put("HA-YELI-KAHN", "DAWN");
        navajoToEnglish.put("AH-KIN-CIL-TOH", "DEFENSE");
        navajoToEnglish.put("NAHL-KIHD", "DEGREE");
        navajoToEnglish.put("BE-SITIHN", "DELAY");
        navajoToEnglish.put("BE-BIH-ZIHDE", "DELIVER");
        navajoToEnglish.put("AH-DEEL-TAHI", "DEMOLITION");
        navajoToEnglish.put("HO-DILH-CLA", "DENSE");
        navajoToEnglish.put("DA-DE-YAH", "DEPART");
        navajoToEnglish.put("HOGAN", "DEPARTMENT");
        navajoToEnglish.put("YE-KHI-DEL-NEI", "DESIGNATE");
        navajoToEnglish.put("AH-DA-AH-HO-DZAH", "DESPERATE");
        navajoToEnglish.put("AL-CHA-NIL", "DETACH");
        navajoToEnglish.put("BE-BEH-SHA", "DETAIL");
        navajoToEnglish.put("AH-DEEL-TAHI", "DETONATOR");
        navajoToEnglish.put("NA-NE-KLAH", "DIFFICULT");
        navajoToEnglish.put("LE-EH-GADE", "DIG IN");
        navajoToEnglish.put("AH-JI-GO", "DIRECT");
        navajoToEnglish.put("EH-HA-JAY", "DISEMBARK");
        navajoToEnglish.put("LA-CHAI-EN-SEIS-BE-JAY", "DISPATCH");
        navajoToEnglish.put("HIH-DO-NAL", "DISPLACE");
        navajoToEnglish.put("BE-SEIS-NA-NEH", "DISPLAY");
        navajoToEnglish.put("A-HO-TEY", "DISPOSITION");
        navajoToEnglish.put("NAH-NEH", "DISTRIBUTE");
        navajoToEnglish.put("BE-THIN-YA-NI-CHE", "DISTRICT");
        navajoToEnglish.put("TSE-LE", "DO");
        navajoToEnglish.put("BEH-EH-HO-ZINZ", "DOCUMENT");
        navajoToEnglish.put("AH-NOL-KAHL", "DRIVE");
        navajoToEnglish.put("DI-GISS-YAHZIE", "DUD");
        navajoToEnglish.put("DI-GISS-TSO", "DUMMY");
        navajoToEnglish.put("TA-LAHI-NE-ZINI-GO", "EACH");
        navajoToEnglish.put("WHO-DZAH", "ECHELON");
        navajoToEnglish.put("BE-BA-HI", "EDGE");
        navajoToEnglish.put("BE-DELH-NEED", "EFFECTIVE");
        navajoToEnglish.put("YEA-GO", "EFFORT");
        navajoToEnglish.put("AH-NA-NAI", "ELEMENT");
        navajoToEnglish.put("ALI-KHI-HO-NE-OHA", "ELEVATE");
        navajoToEnglish.put("HA-BEH-TO-DZIL", "ELIMINATE");
        navajoToEnglish.put("EH-HO-JAY", "EMBARK");
        navajoToEnglish.put("HO-NEZ-CLA", "EMERGENCY");
        navajoToEnglish.put("LA-AZ-NIL", "EMPLACEMENT");
        navajoToEnglish.put("YE-NAS-TEH", "ENCIRCLE");
        navajoToEnglish.put("BI-KHANH", "ENCOUNTER");
        navajoToEnglish.put("A-HA-NE-HO-TA", "ENGAGE");
        navajoToEnglish.put("CHIDI-BI-TSI-TSINE", "ENGINE");
        navajoToEnglish.put("DAY-DIL-JAH-HE", "ENGINEER");
        navajoToEnglish.put("NIH-TSA-GOH-AL-NEH", "ENLARGE");
        navajoToEnglish.put("BIH-ZIH-A-DA-YI-LAH", "ENLIST");
        navajoToEnglish.put("TA-A-TAH", "ENTIRE");
        navajoToEnglish.put("E-GAD-AH-NE-LIH", "ENTRENCH");
        navajoToEnglish.put("A-ZAH-GI-YA", "ENVELOP");
        navajoToEnglish.put("YA-HA-DE-TAHI", "EQUIPMENT");
        navajoToEnglish.put("YEH-ZIHN", "ERECT");
        navajoToEnglish.put("A-ZEH-HA-GE-YAH", "ESCAPE");
        navajoToEnglish.put("HAS-TAY-DZAH", "ESTABLISH");
        navajoToEnglish.put("BIH-KE-TSE-HOD-DES-KEZ", "ESTIMATE");
        navajoToEnglish.put("HA-NA", "EVACUATE");
        navajoToEnglish.put("NEH-DIH", "EXCEPT");
        navajoToEnglish.put("NA-WOL-NE", "EXCEPT");
        navajoToEnglish.put("ALH-NAHL-YAH", "EXCHANGE");
        navajoToEnglish.put("A-DO-NIL", "EXECUTE");
        navajoToEnglish.put("AH-DEL-TAHI", "EXPLOSIVE");
        navajoToEnglish.put("SHIL-LOH", "EXPEDITE");
        navajoToEnglish.put("NE-TDALE", "EXTEND");
        navajoToEnglish.put("AL-TSAN-AH-BAHM", "EXTREME");
        navajoToEnglish.put("CHA-AL-EIND", "FAIL");
        navajoToEnglish.put("YEES-GHIN", "FAILURE");
        navajoToEnglish.put("MAI-BE-HE-AHGAN", "FARM");
        navajoToEnglish.put("DZEH-CHI-YON", "FEED");
        navajoToEnglish.put("CLO-DIH", "FIELD");
        navajoToEnglish.put("TOH-BAH-HA-ZSID", "FIERCE");
        navajoToEnglish.put("BA-EH-CHEZ", "FILE");
        navajoToEnglish.put("TAH-AH-KWO-DIH", "FINAL");
        navajoToEnglish.put("COH-AH-GHIL-TLID", "FLAME THROWER");
        navajoToEnglish.put("DAH-DI-KAD", "FLANK");
        navajoToEnglish.put("WO-CHI", "FLARE");
        navajoToEnglish.put("MA-E-AS-ZLOLI", "FLIGHT");
        navajoToEnglish.put("TA-NA-NE-LADI", "FORCE");
        navajoToEnglish.put("BE-CHA", "FORM");
        navajoToEnglish.put("BE-CHA-YE-LAILH", "FORMATION");
        navajoToEnglish.put("AH-NA-SOZI", "FORTIFICATION");
        navajoToEnglish.put("AH-NA-SOZI-YAZZIE", "FORTIFY");
        navajoToEnglish.put("TEHI", "FORWARD");
        navajoToEnglish.put("BESH-YAZZIE", "FRAGMENTATION");
        navajoToEnglish.put("HA-TALHI-TSO", "FREQUENCY");
        navajoToEnglish.put("NEH-HECHO-DA-NE", "FRIENDLY");
        navajoToEnglish.put("BI-TSAN-DEHN", "FROM");
        navajoToEnglish.put("YEAS-NIL", "FURNISH");
        navajoToEnglish.put("WO-NAS-DI", "FURTHER");
        navajoToEnglish.put("YAH-A-DA-HAL-YON-IH", "GARRISON");
        navajoToEnglish.put("CHIDI-BI-TOH", "GASOLINE");
        navajoToEnglish.put("NI-MA-SI", "GRENADE");
        navajoToEnglish.put("NI-DIH-DA-HI", "GUARD");
        navajoToEnglish.put("NAH-E-THLAI", "GUIDE");
        navajoToEnglish.put("LHI-TA-A-TA", "HALL");
        navajoToEnglish.put("ALH-NIH-JAH-A-QUHE", "HALF TRACK");
        navajoToEnglish.put("TA-AKWAI-I", "HALT");
        navajoToEnglish.put("BET-SEEN", "HANDLE");
        navajoToEnglish.put("JO", "HAVE");
        navajoToEnglish.put("NA-HA-TAH-TA-BA-HOGAN", "HEADQUARTER");
        navajoToEnglish.put("WO-TAH-TA-EH-DAHN-OH", "HELD");
        navajoToEnglish.put("WO-TAH", "HIGH");
        navajoToEnglish.put("BE-AL-DOH-BE-CA-BIH-DZIL-IGI", "HIGH EXPLOSIVE");
        navajoToEnglish.put("WO-TAH-HO-NE-TEH", "HIGHWAY");
        navajoToEnglish.put("WO-TKANH", "HOLD");
        navajoToEnglish.put("A-ZEY-AL-IH", "HOSPITAL");
        navajoToEnglish.put("A-NAH-NE-DZIN", "HOSTILE");
        navajoToEnglish.put("BE-EL-DON-TS-QUODI", "HOWITZER");
        navajoToEnglish.put("WO-CHI", "ILLUMINATE");
        navajoToEnglish.put("SHIL-LOH", "IMMEDIATELY");
        navajoToEnglish.put("A-HE-DIS-GOH", "IMPACT");
        navajoToEnglish.put("BA-HAS-TEH", "IMPORTANT");
        navajoToEnglish.put("HO-DOL-ZHOND", "IMPROVE");
        navajoToEnglish.put("EL-TSOD", "INCLUDE");
        navajoToEnglish.put("HO-NALH", "INCREASE");
        navajoToEnglish.put("BA-HAL-NEH", "INDICATE");
        navajoToEnglish.put("TA-NEH-NAL-DAHI", "INFANTRY");
        navajoToEnglish.put("YE-GHA-NE-JEH", "INFILTRATE");
        navajoToEnglish.put("BEH-ED-DE-DLID", "INITIAL");
        navajoToEnglish.put("EHD-TNAH", "INSTALL");
        navajoToEnglish.put("NAS-NIL", "INSTALLATION");
        navajoToEnglish.put("NA-NE-TGIN", "INSTRUCT");
        navajoToEnglish.put("HO-YA", "INTELLIGENCE");
        navajoToEnglish.put("DZEEL", "INTENSE");
        navajoToEnglish.put("YEL-NA-ME-JAH", "INTERCEPT");
        navajoToEnglish.put("AH-NILH-KHLAI", "INTERFERE");
        navajoToEnglish.put("AH-TAH-HA-NE", "INTERPRET");
        navajoToEnglish.put("NA-ALI-KA", "INVESTIGATE");
        navajoToEnglish.put("A-TAH", "INVOLVE");
        navajoToEnglish.put("SEIS", "IS");
        navajoToEnglish.put("SEIS-KEYAH", "ISLAND");
        navajoToEnglish.put("BIH-TSA-NEL-KAD", "ISOLATE");
        navajoToEnglish.put("WOH-DI-CHIL", "JUNGLE");
        navajoToEnglish.put("NAZ-TSAID", "KILL");
        navajoToEnglish.put("NAS-TSAID-A-KHA-AH-YEH-HA-DILH", "KILOCYCLE");
        navajoToEnglish.put("NA-NISH", "LABOR");
        navajoToEnglish.put("KAY-YAH", "LAND");
        navajoToEnglish.put("TKA-GHIL-ZHOD", "LAUNCH");
        navajoToEnglish.put("AH-NA-GHAI", "LEADER");
        navajoToEnglish.put("DE-BE-YAZIE-HA-A-AH", "LEAST");
        navajoToEnglish.put("DAH-DE-YAH", "LEAVE");
        navajoToEnglish.put("NISH-CLA-JIH-GOH", "LEFT");
        navajoToEnglish.put("BI-OH", "LESS");
        navajoToEnglish.put("DIL-KONH", "LEVEL");
        navajoToEnglish.put("DA-A-HE-GI-ENEH", "LIAISON");
        navajoToEnglish.put("BA-HAS-AH", "LIMIT");
        navajoToEnglish.put("NI-DAS-TON", "LITTER");
        navajoToEnglish.put("A-KWE-EH", "LOCATE");
        navajoToEnglish.put("UT-DIN", "LOSS");
        navajoToEnglish.put("A-KNAH-AS-DONIH", "MACHINE GUN");
        navajoToEnglish.put("NA-E-LAHI", "MAGNETIC");
        navajoToEnglish.put("HASTNI-BEH-NA-HAI", "MANAGE");
        navajoToEnglish.put("NA-NA-O-NALTH", "MANEUVER");
        navajoToEnglish.put("KAH-YA-NESH-CHAI", "MAP");
        navajoToEnglish.put("BEL-DIL-KHON", "MAXIMUM");
        navajoToEnglish.put("CHITI-A-NAYL-INIH", "MECHANIC");
        navajoToEnglish.put("CHIDI-DA-AH-HE-GONI", "MECHANIZED");
        navajoToEnglish.put("A-ZAY", "MEDICAL");
        navajoToEnglish.put("MIL-AH-HEH-AH-DILH", "MEGACYCLE");
        navajoToEnglish.put("NA-EL-NEHI-TSIN-NA-AILH", "MERCHANT SHIP");
        navajoToEnglish.put("HANE-AL-NEH", "MESSAGE");
        navajoToEnglish.put("SILAGO-KEH-GOH", "MILITARY");
        navajoToEnglish.put("NA-AS-TSO-SI-A-YE-DO-TISH", "MILLIMETER");
        navajoToEnglish.put("HA-GADE", "MINE");
        navajoToEnglish.put("BE-OH", "MINIMUM");
        navajoToEnglish.put("AH-KHAY-EL-KIT-YAZZIE", "MINUTE");
        navajoToEnglish.put("AL-NESHODI", "MISSION");
        navajoToEnglish.put("O-ZHI", "MISTAKE");
        navajoToEnglish.put("HA-TAO-DI", "MOPPING");
        navajoToEnglish.put("THLA-NA-NAH", "MORE");
        navajoToEnglish.put("BE-AL-DOH-CID-DA-HI", "MORTAR");
        navajoToEnglish.put("NA-HOT-NAH", "MOTION");
        navajoToEnglish.put("CHIDE-BE-TSE-TSEN", "MOTOR");
        navajoToEnglish.put("KA-HA-TENI", "NATIVE");
        navajoToEnglish.put("TAL-KAH-SILAGO", "NAVY");
        navajoToEnglish.put("YE-NA-ZEHN", "NECESSARY");
        navajoToEnglish.put("DO-YA-SHO-DA", "NEGATIVE");
        navajoToEnglish.put("NA-NES-DIZI", "NET");
        navajoToEnglish.put("DO-NEH-LINI", "NEUTRAL");
        navajoToEnglish.put("DOH-A-TA-H-DAH", "NORMAL");
        navajoToEnglish.put("NI-DAH-THAN-ZIE", "NOT");
        navajoToEnglish.put("NE-DA-TAZI-THIN", "NOTICE");
        navajoToEnglish.put("KUT", "NOW");
        navajoToEnglish.put("BEH-BIH-KE-AS-CHINIGH", "NUMBER");
        navajoToEnglish.put("BI-NE-YEI", "OBJECTIVE");
        navajoToEnglish.put("HAL-ZID", "OBSERVE");
        navajoToEnglish.put("DA-HO-DESH-ZHA", "OBSTACLE");
        navajoToEnglish.put("YEEL-TSOD", "OCCUPY");
        navajoToEnglish.put("TOH-NI-TKAL-LO", "OF");
        navajoToEnglish.put("BIN-KIE-JINH-JIH-DEZ-JAY", "OFFENSIVE");
        navajoToEnglish.put("TA-LAI-DI", "ONCE");
        navajoToEnglish.put("TA-EI-TAY-A-YAH", "ONLY");
        navajoToEnglish.put("YE-NAHL-NISH", "OPERATE");
        navajoToEnglish.put("ASH-GA-ALIN", "OPPORTUNITY");
        navajoToEnglish.put("NE-HE-TSAH-JIH-SHIN", "OPPOSITION");
        navajoToEnglish.put("EH-DO-DAH-GOH", "OR");
        navajoToEnglish.put("TCHIL-LHE-SOI", "ORANGE");
        navajoToEnglish.put("BE-EH-HO-ZINI", "ORDER");
        navajoToEnglish.put("LEI-AZ-JAH", "ORDNANCE");
        navajoToEnglish.put("DAS-TEH-DO", "ORIGINATE");
        navajoToEnglish.put("LA-E-CIH", "OTHER");
        navajoToEnglish.put("CLO-DIH", "OUT");
        navajoToEnglish.put("BE-KA-HAS-TSOZ", "OVERLAY");
        navajoToEnglish.put("ATSANH", "(");
        navajoToEnglish.put("A-YO-AD-DO-NEH", "PARTICULAR");
        navajoToEnglish.put("DA-SHA-JAH", "PARTY");
        navajoToEnglish.put("NA-ELI-YA", "PAY");
        navajoToEnglish.put("TAH-NI-DES-TANH", "PENALIZE");
        navajoToEnglish.put("YAL", "PERCENT");
        navajoToEnglish.put("DA-AHL-ZHIN", ".");
        navajoToEnglish.put("DA-AL-ZHIN-THIN-MOASI", "PERIODIC");
        navajoToEnglish.put("GOS-SHI-E", "PERMIT");
        navajoToEnglish.put("DA-NE-LEI", "PERSONNEL");
        navajoToEnglish.put("BEH-CHI-MA-HAD-NIL", "PHOTOGRAPH");
        navajoToEnglish.put("BI-SO-DIH-DOT-SAHI-BI-TSAH", "PILL BOX");
        navajoToEnglish.put("BIL-DAH-HAS-TANH-YA", "PINNED DOWN");
        navajoToEnglish.put("TSIDI", "PLANE");
        navajoToEnglish.put("DIL-DI-GHILI", "PLASMA");
        navajoToEnglish.put("BE-SO-DE-DEZ-AHE", "POINT");
        navajoToEnglish.put("TKOSH-JAH-DA-NA-ELT", "PONTOON");
        navajoToEnglish.put("BILH-HAS-AHN", "POSITION");
        navajoToEnglish.put("TA-HA-AH-TAY", "POSSIBLE");
        navajoToEnglish.put("SAH-DEI", "POST");
        navajoToEnglish.put("HASH-TAY-HO-DIT-NE", "PREPARE");
        navajoToEnglish.put("KUT", "PRESENT");
        navajoToEnglish.put("BIH-TSE-DIH", "PREVIOUS");
        navajoToEnglish.put("ALTSEH-NAN-DAY-HI-GIH", "PRIMARY");
        navajoToEnglish.put("HANE-PESODI", "PRIORITY");
        navajoToEnglish.put("DA-TSI", "PROBABLE");
        navajoToEnglish.put("NA-NISH-TSOH", "PROBLEM");
        navajoToEnglish.put("NAY-NIH-JIH", "PROCEED");
        navajoToEnglish.put("NAH-SAI", "PROGRESS");
        navajoToEnglish.put("AH-CHANH", "PROTECT");
        navajoToEnglish.put("YIS-NIL", "PROVIDE");
        navajoToEnglish.put("DINL-CHI", "PURPLE");
        navajoToEnglish.put("COH-NA-CHANH", "PYROTECHNIC");
        navajoToEnglish.put("AH-JAH-A", "?");
        navajoToEnglish.put("SHIL-LOH", "QUICK");
        navajoToEnglish.put("ESAT-TSANH", "RADAR");
        navajoToEnglish.put("DEZJAY", "RAID");
        navajoToEnglish.put("A-DE-GEH-HI", "RAILHEAD");
        navajoToEnglish.put("KONH-NA-AL-BANSI-BI-THIN", "RAILROAD");
        navajoToEnglish.put("A-LAH-NA-O-GLALIH", "RALLYING");
        navajoToEnglish.put("AN-ZAH", "RANGE");
        navajoToEnglish.put("GAH-EH-YAHN", "RATE");
        navajoToEnglish.put("NA-A-JAH", "RATION");
        navajoToEnglish.put("CHUSH-KA", "RAVINE");
        navajoToEnglish.put("IL-DAY", "REACH");
        navajoToEnglish.put("KUT", "READY");
        navajoToEnglish.put("BE-KA-DENH", "REAR");
        navajoToEnglish.put("SHOZ-TEH", "RECEIPT");
        navajoToEnglish.put("CHE-HO-TAI-TAHN", "RECOMMEND");
        navajoToEnglish.put("HA-A-CIDI", "RECONNAISSANCE");
        navajoToEnglish.put("TA-HA-NE-AL-YA", "RECONNOITER");
        navajoToEnglish.put("GAH-AH-NAH-KLOLI", "RECORD");
        navajoToEnglish.put("LI-CHI", "RED");
        navajoToEnglish.put("TSA-ZHIN", "REEF");
        navajoToEnglish.put("EH-NA-COH", "REEMBARK");
        navajoToEnglish.put("NA-NA-COH", "REFIRE");
        navajoToEnglish.put("NA-YEL-N", "REGULATE");
        navajoToEnglish.put("NAL-DZIL", "REINFORCE");
        navajoToEnglish.put("AGANH-TOL-JAY", "RELIEF");
        navajoToEnglish.put("NAH-JIH-CO-NAL-YA", "RELIEVE");
        navajoToEnglish.put("HA-DIT-ZAH", "REORGANIZE");
        navajoToEnglish.put("NI-NA-DO-NIL", "REPLACEMENT");
        navajoToEnglish.put("WHO-NEH", "REPORT");
        navajoToEnglish.put("TKA-NAZ-NILI", "REPRESENTATIVE");
        navajoToEnglish.put("JO-KAYED-GOH", "REQUEST");
        navajoToEnglish.put("HESH-J-E", "RESERVE");
        navajoToEnglish.put("BA-HO-CHINI", "RESTRICT");
        navajoToEnglish.put("AH-HOS-TEEND", "RETIRE");
        navajoToEnglish.put("JI-DIN-NES-CHANH", "RETREAT");
        navajoToEnglish.put("NA-DZAH", "RETURN");
        navajoToEnglish.put("WHO-NEH", "REVEAL");
        navajoToEnglish.put("NA-SI-YIZ", "REVERT");
        navajoToEnglish.put("BA-NAS-CLA", "REVETMENT");
        navajoToEnglish.put("GAH-GHIL-KEID", "RIDGE");
        navajoToEnglish.put("BE-AL-DO-HOSTEEN", "RIFLEMAN");
        navajoToEnglish.put("TOH-YIL-KAL", "RIVER");
        navajoToEnglish.put("A-YE-SHI-NA-TAH-IH", "ROBOT BOMB");
        navajoToEnglish.put("LESZ-YIL-BESHI", "ROCKET");
        navajoToEnglish.put("YEH-MAS", "ROLL");
        navajoToEnglish.put("NAZ-PAS", "ROUND");
        navajoToEnglish.put("GAH-BIH-TKEEN", "ROUTE");
        navajoToEnglish.put("NIH-DZID-TEIH", "RUNNER");
        navajoToEnglish.put("A-TKEL-YAH", "SABOTAGE");
        navajoToEnglish.put("A-TKEL-EL-INI", "SABOTEUR");
        navajoToEnglish.put("CHA-LE-GAI", "SAILOR");
        navajoToEnglish.put("NA-HAS-GLAH", "SALVAGE");
        navajoToEnglish.put("BIH-LA-SANA-CID-DA-HI", "SAT");
        navajoToEnglish.put("LHE-CHI", "SCARLET, RED");
        navajoToEnglish.put("BEH-EH-HO-ZINI", "SCHEDULE");
        navajoToEnglish.put("HA-A-SID-AL-SIZI-GIH", "SCOUT");
        navajoToEnglish.put("BESH-NA-NES-DIZI", "SCREEN");
        navajoToEnglish.put("TKAL-KAH-DINEH-IH", "SEAMAN");
        navajoToEnglish.put("BAH-HAS-TKIH", "SECRET");
        navajoToEnglish.put("YOEHI", "SECTOR");
        navajoToEnglish.put("YE-DZHE-AL-TSISI", "SECURE");
        navajoToEnglish.put("YEEL-STOD", "SEIZE");
        navajoToEnglish.put("BE-TAH-HAS-GLA", "SELECT");
        navajoToEnglish.put("DA-AHL-ZHIN-BI-TSA-NA-DAHL", ";");
        navajoToEnglish.put("DZEH-CID-DA-HI", "SET");
        navajoToEnglish.put("DI-BAH-NESH-GOHZ", "SHACKLE");
        navajoToEnglish.put("BE-AL-DOH-BE-CA", "SHELL");
        navajoToEnglish.put("TAH-BAHN", "SHORE");
        navajoToEnglish.put("BOSH-KEESH", "SHORT");
        navajoToEnglish.put("BOSH-KEESH", "SIDE");
        navajoToEnglish.put("YE-EL-TSANH", "SIGHT");
        navajoToEnglish.put("NA-EH-EH-GISH", "SIGNAL");
        navajoToEnglish.put("ALAH-IH-NE-TIH", "SIMPLEX");
        navajoToEnglish.put("TKIN-CID-DA-HI", "SIT");
        navajoToEnglish.put("A-HO-TAY", "SITUATE");
        navajoToEnglish.put("LIT", "SMOKE");
        navajoToEnglish.put("OH-BEHI", "SNIPER");
        navajoToEnglish.put("E-YIH-SIH", "SPECIAL");
        navajoToEnglish.put("YO-ZONS", "SPEED");
        navajoToEnglish.put("AH-NA-HO-NEIL", "SPORADIC");
        navajoToEnglish.put("EEL-TSAY-I", "SPOTTER");
        navajoToEnglish.put("KLESH-SO-DILZIN", "SPRAY");
        navajoToEnglish.put("NAH-GHIZI", "SQUADRON");
        navajoToEnglish.put("NE-OL", "STORM");
        navajoToEnglish.put("NA-WO-GHI-GOID", "STRAFF");
        navajoToEnglish.put("CHY-NE-DE-DAHE", "STRAGGLER");
        navajoToEnglish.put("NA-HA-TAH", "STRATEGY");
        navajoToEnglish.put("TOH-NI-LIH", "STREAM");
        navajoToEnglish.put("DZHEL", "STRENGTH");
        navajoToEnglish.put("DESZ-TSOOD", "STRETCH");
        navajoToEnglish.put("NAY-DAL-GHAL", "STRIKE");
        navajoToEnglish.put("HA-TIH-JAH", "STRIP");
        navajoToEnglish.put("NIL-TA", "STUBBORN");
        navajoToEnglish.put("NA-NISH-YAZZIE", "SUBJECT");
        navajoToEnglish.put("TKAL-CLA-YI-YAH", "SUBMERGE");
        navajoToEnglish.put("A-NIH-LEH", "SUBMIT");
        navajoToEnglish.put("AL-KHI-NAL-DZL", "SUBORDINATE");
        navajoToEnglish.put("YAH-TAY-GO-E-ELAH", "SUCCEED");
        navajoToEnglish.put("UT-ZAH", "SUCCESS");
        navajoToEnglish.put("UT-ZAH-HA-DEZ-BIN", "SUCCESSFUL");
        navajoToEnglish.put("UT-ZAH-SID", "SUCCESSIVE");
        navajoToEnglish.put("YIS-CLEH", "SUCH");
        navajoToEnglish.put("TO-HO-NE", "SUFFER");
        navajoToEnglish.put("SHIN-GO-BAH", "SUMMARY");
        navajoToEnglish.put("TKA-GO-NE-NAN-DEY-HE", "SUPPLEMENTARY");
        navajoToEnglish.put("NAL-YEH-HI", "SUPPLY");
        navajoToEnglish.put("NALGA-HI-TSIN-NAH-AILH", "SUPPLY SHIP");
        navajoToEnglish.put("BA-AH-HOT-GLI", "SUPPORT");
        navajoToEnglish.put("NE-NA-CHA", "SURRENDER");
        navajoToEnglish.put("NAZ-PAS", "SURROUND");
        navajoToEnglish.put("YIS-DA-YA", "SURVIVE");
        navajoToEnglish.put("DI-BA-TSA-AS-ZHI-BI-TSIN", "SYSTEM");
        navajoToEnglish.put("E-CHIHN", "TACTICAL");
        navajoToEnglish.put("GAH-TAHN", "TAKE");
        navajoToEnglish.put("CHAY-DA-GAHI", "TANK");
        navajoToEnglish.put("CHAY-DA-GAHI-NAIL-TSAIDI", "TANK DESTROYER");
        navajoToEnglish.put("WOL-DONI", "TARGET");
        navajoToEnglish.put("TAZI-NA-EH-DIL-KID", "TASK");
        navajoToEnglish.put("DEH-NA-AS-TSO-SI", "TEAM");
        navajoToEnglish.put("ALI-KHI-HO-NE-OHA", "TERRACE");
        navajoToEnglish.put("TASHI-NA-HAL-THIN", "TERRAIN");
        navajoToEnglish.put("KA-YAH", "TERRITORY");
        navajoToEnglish.put("TAZI-CHA", "THAT");
        navajoToEnglish.put("CHA-GEE", "THE");
        navajoToEnglish.put("BIH", "THEIR");
        navajoToEnglish.put("TA-ZI-KWA-I-BE-KA-DI", "THEREAFTER");
        navajoToEnglish.put("CHA-GI-O-EH", "THESE");
        navajoToEnglish.put("CHA-GEE", "THEY");
        navajoToEnglish.put("DI", "THIS");
        navajoToEnglish.put("TA-BILH", "TOGETHER");
        navajoToEnglish.put("LO-BE-CA", "TORPEDO");
        navajoToEnglish.put("TA-AL-SO", "TOTAL");
        navajoToEnglish.put("BEH-NA-AL-KAH-HI", "TRACER");
        navajoToEnglish.put("HANE-BA-NA-AS-DZOH", "TRAFFIC DIAGRAM");
        navajoToEnglish.put("COH-NAI-ALI-BAHN-SI", "TRAIN");
        navajoToEnglish.put("A-HAH-DA-A-CHA", "TRANSPORTATION");
        navajoToEnglish.put("E-GADE", "TRENCH");
        navajoToEnglish.put("TKA-IH", "TRIPLE");
        navajoToEnglish.put("NAL-DEH-HI", "TROOP");
        navajoToEnglish.put("CHIDO-TSO", "TRUCK");
        navajoToEnglish.put("ALTH-AH-A-TEH", "TYPE");
        navajoToEnglish.put("BI-YAH", "UNDER");
        navajoToEnglish.put("DO-BAY-HOSEN-E", "UNIDENTIFIED");
        navajoToEnglish.put("DA-AZ-JAH", "UNIT");
        navajoToEnglish.put("NO-DA-EH-NESH-GOHZ", "UNSHACKLE");
        navajoToEnglish.put("UH-QUO-HO", "UNTIL");
        navajoToEnglish.put("NA-HOS-AH-GIH", "VICINITY");
        navajoToEnglish.put("CHAH-HO-OH-LHAN-IH", "VILLAGE");
        navajoToEnglish.put("NAY-ES-TEE", "VISIBILITY");
        navajoToEnglish.put("TA-EH-YE-SY", "VITAL");
        navajoToEnglish.put("BILH-HE-NEH", "WARNING");
        navajoToEnglish.put("NE-TEH", "WAS");
        navajoToEnglish.put("TKOH", "WATER");
        navajoToEnglish.put("YILH-KOLH", "WAVE");
        navajoToEnglish.put("BEH-DAH-A-HI-JIH-GANI", "WEAPON");
        navajoToEnglish.put("TO-HA-HA-DLAY", "WELL");
        navajoToEnglish.put("GLOE-EH-NA-AH-WO-HAI", "WHEN");
        navajoToEnglish.put("GLOE-IH-QUI-AH", "WHERE");
        navajoToEnglish.put("GLOE-IH-A-HSI-TLON", "WHICH");
        navajoToEnglish.put("GLOE-IH-DOT-SAHI", "WILL");
        navajoToEnglish.put("BESH-TSOSIE", "WIRE");
        navajoToEnglish.put("BILH", "WITH");
        navajoToEnglish.put("BILH-BIGIH", "WITHIN");
        navajoToEnglish.put("TA-GAID", "WITHOUT");
        navajoToEnglish.put("CHIZ", "WOOD");
        navajoToEnglish.put("CAH-DA-KHI", "WOUND");
        navajoToEnglish.put("A-DEL-TAHL", "YARD");
        navajoToEnglish.put("ASTSANH", ")");
    }

    private void initChars() {
        navajoToEnglish.put("BE-TKAH", " ");
        navajoToEnglish.put("WOL-LA-CHEE", "A");
        navajoToEnglish.put("BE-LA-SANA", "A");
        navajoToEnglish.put("TSE-NILL", "A");
        navajoToEnglish.put("NA-HASH-CHID", "B");
        navajoToEnglish.put("SHUSH", "B");
        navajoToEnglish.put("TOISH-JEH", "B");
        navajoToEnglish.put("MOASI", "C");
        navajoToEnglish.put("TLA-GIN", "C");
        navajoToEnglish.put("BA-GOSHI", "C");
        navajoToEnglish.put("BE", "D");
        navajoToEnglish.put("CHINDI", "D");
        navajoToEnglish.put("LHA-CHA-EH", "D");
        navajoToEnglish.put("AH-JAH", "E");
        navajoToEnglish.put("DZEH", "E");
        navajoToEnglish.put("AH-NAH", "E");
        navajoToEnglish.put("CHUO", "F");
        navajoToEnglish.put("TSA-E-DONIN-EE", "F");
        navajoToEnglish.put("MA-E", "F");
        navajoToEnglish.put("AH-TAD", "G");
        navajoToEnglish.put("KLIZZIE", "G");
        navajoToEnglish.put("JEHA", "G");
        navajoToEnglish.put("TSE-GAH", "H");
        navajoToEnglish.put("CHA", "H");
        navajoToEnglish.put("LIN", "H");
        navajoToEnglish.put("TKIN", "I");
        navajoToEnglish.put("YEH-HES", "I");
        navajoToEnglish.put("A-CHI", "I");
        navajoToEnglish.put("TKELE-CHO-G", "J");
        navajoToEnglish.put("AH-YA-TSINNE", "J");
        navajoToEnglish.put("YIL-DOI", "J");
        navajoToEnglish.put("JAD-HO-LONI", "K");
        navajoToEnglish.put("BA-AH-NE-DI-TININ", "K");
        navajoToEnglish.put("KLIZZIE-YAZZIE", "K");
        navajoToEnglish.put("DIBEH-YAZZIE", "L");
        navajoToEnglish.put("AH-JAD", "L");
        navajoToEnglish.put("NASH-DOIE-TSO", "L");
        navajoToEnglish.put("TSIN-TLITI", "M");
        navajoToEnglish.put("BE-TAS-TNI", "M");
        navajoToEnglish.put("NA-AS-TSO-SI", "M");
        navajoToEnglish.put("TSAH", "N");
        navajoToEnglish.put("A-CHIN", "N");
        navajoToEnglish.put("A-KHA", "O");
        navajoToEnglish.put("TLO-CHIN", "O");
        navajoToEnglish.put("NE-AHS-JAH", "O");
        navajoToEnglish.put("CLA-GI-AIH", "P");
        navajoToEnglish.put("BI-SO-DIH", "P");
        navajoToEnglish.put("NE-ZHONI", "P");
        navajoToEnglish.put("CA-YEILTH", "Q");
        navajoToEnglish.put("GAH", "R");
        navajoToEnglish.put("DAH-NES-TSA", "R");
        navajoToEnglish.put("AH-LOSZ", "R");
        navajoToEnglish.put("DIBEH", "S");
        navajoToEnglish.put("KLESH", "S");
        navajoToEnglish.put("D-AH", "T");
        navajoToEnglish.put("A-WOH", "T");
        navajoToEnglish.put("THAN-ZIE", "T");
        navajoToEnglish.put("SHI-DA", "U");
        navajoToEnglish.put("NO-DA-IH", "U");
        navajoToEnglish.put("A-KEH-DI-GLINI", "V");
        navajoToEnglish.put("GLOE-IH", "W");
        navajoToEnglish.put("AL-NA-AS-DZOH", "X");
        navajoToEnglish.put("TSAH-AS-ZIH", "Y");
        navajoToEnglish.put("BESH-DO-TLIZ", "Z");
    }
}
