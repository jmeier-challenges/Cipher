package jm.cipher;

import jm.cipher.data.IAsciiBinaryData;

/**
 * User: johann
 * Date: 09.03.11
 * Time: 10:16
 */

public class AsciiBinary extends AbstractCipher {
    private int bitLength;
    private IAsciiBinaryData data;

    public AsciiBinary() {
        setBitLength(7);
    }

    public String decrypt(String ct) {
        initializeData();
        StringBuffer pt = new StringBuffer();
        int bitLenght = getBitLength();
        StringBuffer bin = new StringBuffer();
        for (int i = 0; i < ct.length(); i++) {
            char c = ct.charAt(i);
            if (isBinaryDigit(c)) {
                bin.append(c);
                if (bin.length() == bitLenght) {
                    int pos = Integer.parseInt(bin.toString(), 2);
                    pt.append((char) pos);
                    bin = new StringBuffer();
                }
            }
        }
        return pt.toString();
    }

    public void initializeData() {
        if (data != null) {
            try {
                bitLength = Integer.parseInt(data.getBitLength());
            } catch (NumberFormatException ex) {
                // ignore
            }
        }
    }

    public String getDisplayname() {
        return "ASCII to Binary";
    }

    public String getName() {
        return "AsciiBinary";
    }

    public String getDescription() {
        return "ASCII to Binary";
    }

    public void setIData(Object data) {
        setData((IAsciiBinaryData) data);
    }

    private boolean isBinaryDigit(char c) {
        return '0' == c || '1' == c;
    }

    public String encrypt(String pt) {
        initializeData();
        StringBuffer ct = new StringBuffer();
        for (int i = 0; i < pt.length(); i++) {
            char c = pt.charAt(i);
            ct.append(getBinString(c));
        }
        return ct.toString();
    }

    private String getBinString(int c) {
        int bitLength = getBitLength();
        String bin = Integer.toBinaryString(c);
        if (bin.length() < bitLength) {
            for (int i = bin.length(); i < bitLength; i++) {
                bin = "0" + bin;
            }
        } else {
            bin = bin.substring(bin.length()-bitLength);
        }
        return bin;
    }

    public int getBitLength() {
        return bitLength;
    }

    public void setBitLength(int bitlength) {
        this.bitLength = bitlength;
    }

    public void setData(IAsciiBinaryData data) {
        this.data = data;
    }

    @Override
    public void setMemento(Memento memento) {
        if (memento instanceof AsciiBinaryMemento) {
            setBitLength(((AsciiBinaryMemento) memento).bitLength);
            data.setBitLength(String.valueOf(((AsciiBinaryMemento) memento).bitLength));
        }
    }

    @Override
    public Memento getMemento() {
        AsciiBinaryMemento memento = new AsciiBinaryMemento();
        memento.bitLength = bitLength;
        return memento;
    }
}

class AsciiBinaryMemento extends Memento {
    int bitLength;
}

