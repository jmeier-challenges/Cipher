package jm.cipher;

import jm.cipher.codec.*;
import jm.cipher.data.IBinaryData;

import java.util.HashMap;
import java.util.Map;

/**
 * User: T05365A
 * Date: 31.07.12
 * Time: 15:31
 */
public class BinaryData extends AbstractCipher {
    public IBinaryData data;

    public static Map<String, ICipher> coders;
    static {
        coders = new HashMap<String, ICipher>();
        coders.put("Base32", new Base32());
        coders.put("Base64", new Base64());
        coders.put("Base85", new Base85());
        coders.put("UU", new UUCoder());
	    coders.put("GC-Code", new GCCode());
    }


    @Override
    public String encrypt(String pt) throws Exception {
        return coders.get(data.getCoder()).encrypt(pt);
    }

    @Override
    public String decrypt(String ct) throws Exception {
        return coders.get(data.getCoder()).decrypt(ct);
    }

    @Override
    public String getDisplayname() {
        return "BinaryData";
    }

    @Override
    public String getName() {
        return "BinaryData";
    }

    @Override
    public String getDescription() {
        return "BinaryData encode/decode";
    }

    @Override
    public void initializeData() {
        super.initializeData();    //To change body of overridden methods use File | Settings | File Templates.
    }

    @Override
    public void setIData(Object data) {
        setData((IBinaryData) data);
    }

    public void setData(IBinaryData data) {
        this.data = data;
    }
}
