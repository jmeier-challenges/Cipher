package jm.cipher;

import jm.cipher.data.IADFGXData;
import jm.cipher.transposition.ColumnarTransposition;
import jm.tools.Text;

/**
 * User: T05365A
 * Date: 01.10.12
 * Time: 12:22
 */
public class ADFGX extends AbstractCipher implements IADFGXData {
    public static final String[] COMMON_ALPHABETS = {
            "abcdefghiklmnopqrstuvwxyz",
            "abcdefghijklmnopqrstuvwxyz0123456789"
    };
	private String key1;
	private String key2;
	private String ptAlphabet;
	private String ctAlphabet;

	private Polybios polybios;
	private ColumnarTransposition transposition;
	private IADFGXData data;

	public static void main(String[] args) throws Exception {
		ADFGX cipher = new ADFGX();
		cipher.setKey1("wikipedia");
		cipher.setKey2("beobachtungsliste");
		String ct = "GXGGA DDDGD XXAFA DDFAA XAFDF FXFDG DXGAG GAAXF AGADF AAADG\n" +
			"FAXXA DADFF FDDAD FGAXG XAFXG XFXDA FAGFX XFAXG FDXFF DFAGX\n" +
			"XGXXA DGXGF XDFFD GAXXF FFFGD X\n";
		//String ct = cipher.encrypt("munitionierung beschleunigen punkt soweit nicht eingesehen auch bei tag");
		System.out.println(ct);
		System.out.println(cipher.decrypt(ct));
	}

	public ADFGX() {
		polybios = new Polybios();
		transposition = new ColumnarTransposition();
		setPtAlphabet("abcdefghiklmnopqrstuvwxyz");
		setCtAlphabet("ADFGX");
		setKey1("");
		setKey2("");

		setIData(this);
	}

	@Override
	public String encrypt(String pt) throws Exception {
		polybios.setPtAlphabet(data.getPtAlphabet());
		polybios.setCtAlphabet(data.getCtAlphabet());
		polybios.setKey(data.getKey1());
		polybios.setReverse(true);
		String ct = polybios.encrypt(pt);

		transposition.setAlphabet(data.getCtAlphabet());
		transposition.setKey(data.getKey2());

		ct = transposition.encrypt(Text.getStripped(ct, data.getCtAlphabet()));

		return ct;
	}

	@Override
	public String decrypt(String ct) throws Exception {
		transposition.setAlphabet(data.getCtAlphabet());
		transposition.setKey(data.getKey2());
		String pt = transposition.decrypt(Text.getStripped(ct, data.getCtAlphabet()));

		polybios.setPtAlphabet(data.getPtAlphabet());
		polybios.setCtAlphabet(data.getCtAlphabet());
		polybios.setKey(data.getKey1());
		polybios.setReverse(true);
		pt = polybios.decrypt(pt);

		return pt;
	}

	@Override
	public String getDisplayname() {
		return "ADFG(V)X";
	}

	@Override
	public String getName() {
		return "ADFGX";
	}

	@Override
	public String getDescription() {
		return "ADFGX und ADFGVX aus dem 1. Weltkrieg. Bei ADFGX ist J = I!";
	}

	@Override
	public String getKey1() {
		return key1;
	}

	@Override
	public void setKey1(String key1) {
		this.key1 = key1;
	}

	@Override
	public String getKey2() {
		return key2;
	}

	@Override
	public void setKey2(String key2) {
		this.key2 = key2;
	}

	@Override
	public String getPtAlphabet() {
		return ptAlphabet;
	}

	@Override
	public void setPtAlphabet(String ptAlphabet) {
		this.ptAlphabet = ptAlphabet;
	}

	@Override
	public String getCtAlphabet() {
		return ctAlphabet;
	}

	@Override
	public void setCtAlphabet(String ctAlphabet) {
		this.ctAlphabet = ctAlphabet;
	}

	@Override
	public void setIData(Object data) {
		this.data = (IADFGXData) data;
	}

    @Override
    public void setMemento(Memento memento) {
        ADFGXMemento adfgxMemento = (ADFGXMemento) memento;

        data.setCtAlphabet(adfgxMemento.ctAlphabet);
        data.setPtAlphabet(adfgxMemento.ptAlphabet);
        data.setKey1(adfgxMemento.key1);
        data.setKey2(adfgxMemento.key2);
    }

    @Override
    public Memento getMemento() {
        ADFGXMemento adfgxMemento = new ADFGXMemento();
        adfgxMemento.ctAlphabet = data.getCtAlphabet();
        adfgxMemento.ptAlphabet = data.getPtAlphabet();
        adfgxMemento.key1 = data.getKey1();
        adfgxMemento.key2 = data.getKey2();
        return adfgxMemento;
    }

    class ADFGXMemento extends Memento {
        public String key1;
        public String key2;
        public String ptAlphabet;
        public String ctAlphabet;
    }
}
