package jm.cipher;

import jm.cipher.data.IAlphabetNumberData;
import jm.math.Number;
import jm.tools.Text;

import java.util.List;

/**
 * User: Johann
 * Date: 22.05.11
 * Time: 11:13
 */
public class AlphabetNumber extends AbstractCipher {
    private String fromDigits = "0123456789";
    private String toDigits = "0123456789";
    private String alphabet = "abcdefghijklmnopqrstuvwxyz";
    private IAlphabetNumberData data;

    public String encrypt(String pt) {
        initializeData();
        StringBuffer ct = new StringBuffer();
        for (int i = 0; i < pt.length(); i++) {
            char c = pt.charAt(i);
            int pos = getAlphabet().indexOf(c);
            if (pos >= 0) {
                String n = Integer.toString(pos);
                String number = jm.math.Number.convert(n, getToDigits(), getFromDigits());
                ct.append(number).append(" ");
            } else {
                ct.append(c).append(" ");
            }
        }
        return ct.toString();
    }

    public String decrypt(String ct) {
        initializeData();
        StringBuffer pt = new StringBuffer();
	    List<String> numbers = Text.getWords(ct);
        for (String number : numbers) {
            try {
                String n = Number.convert(number, getFromDigits(), getToDigits());
                int c = Integer.parseInt(n);
                pt.append(getAlphabet().charAt(c));
            } catch (NumberFormatException ex) {
                pt.append('?');
                System.err.println(ex);
            } catch (StringIndexOutOfBoundsException e) {
                pt.append('?');
                System.err.println(e);
            }
        }
        return pt.toString();
    }

    public void initializeData() {
        if (data != null) {
            setAlphabet(data.getAlphabet());
            setFromDigits(data.getFromDigits());
        }
    }

    public String getDisplayname() {
        return "Alphabet to Number";
    }

    public String getName() {
        return "AlphabetNumber";
    }

    public String getDescription() {
        return "Alphabet to Number";
    }

    public void setIData(Object data) {
        setData((IAlphabetNumberData) data);
    }

    public String getFromDigits() {
        return fromDigits;
    }

    public void setFromDigits(String fromDigits) {
        this.fromDigits = fromDigits;
    }

    public String getToDigits() {
        return toDigits;
    }

    public String getAlphabet() {
        return alphabet;
    }

    public void setAlphabet(String alphabet) {
        this.alphabet = alphabet;
    }

    public void setData(IAlphabetNumberData data) {
        this.data = data;
    }

    public static void main(String[] args) {
        AlphabetNumber an = new AlphabetNumber();
        an.setFromDigits("01");

        String pt = "hello";
        String ct = an.encrypt(pt);
        System.out.println(an.decrypt(ct));
    }
}
