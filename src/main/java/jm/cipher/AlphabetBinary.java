package jm.cipher;

import jm.cipher.data.IAlphabetBinaryData;

/**
 * User: johann
 * Date: 09.03.11
 * Time: 09:56
 */
public class AlphabetBinary extends AbstractCipher {
    private static String ABC = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private String alphabet;
    private int bitLength;
    private IAlphabetBinaryData data;

    public AlphabetBinary() {
        setAlphabet(ABC);
        setBitLength(5);
    }

    public String decrypt(String ct) {
        initializeData();
        StringBuffer pt = new StringBuffer();
        String alphabet = getAlphabet();
        int bitLenght = getBitLength();
        StringBuffer bin = new StringBuffer();
        for (int i = 0; i < ct.length(); i++) {
            char c = ct.charAt(i);
            if (isBinaryDigit(c)) {
                bin.append(c);
                if (bin.length() == bitLenght) {
                    try {
                        int pos = Integer.parseInt(bin.toString(), 2);
                        if (pos < alphabet.length()) {
                            pt.append(alphabet.charAt(pos));
                        } else {
                            pt.append("?");
                        }
                    } catch (NumberFormatException ex) {
                        pt.append("?");
                    }
                    bin = new StringBuffer();
                }
            }
        }
        return pt.toString();
    }

    public void initializeData() {
        if (data != null) {
            alphabet = data.getAlphabet();
            try {
                bitLength = Integer.parseInt(data.getBitLength());
            } catch (NumberFormatException ex) {
                // ignore
            }
        }
    }

    public String getDisplayname() {
        return "Alphabet to Binary";
    }

    public String getName() {
        return "AlphabetBinary";
    }

    public String getDescription() {
        return "Alphabet to Binary";
    }

    public void setIData(Object data) {
        setData((IAlphabetBinaryData) data);
    }

    private boolean isBinaryDigit(char c) {
        return '0' == c || '1' == c;
    }

    public String encrypt(String pt) {
        initializeData();
        StringBuffer ct = new StringBuffer();
        String alphabet = getAlphabet();
        for (int i = 0; i < pt.length(); i++) {
            char c = pt.charAt(i);
            int pos = alphabet.indexOf(c);
            if (pos == -1) {
                ct.append(c);
            } else {
                ct.append(getBinString(pos));
            }
        }
        return ct.toString();
    }

    private String getBinString(int pos) {
        int bitLength = getBitLength();
        String bin = Integer.toBinaryString(pos);
        if (bin.length() < bitLength) {
            for (int i = bin.length(); i < bitLength; i++) {
                bin = "0" + bin;
            }
        } else {
            bin = bin.substring(bin.length()-bitLength);
        }
        return bin;
    }

    public String getAlphabet() {
        return alphabet;
    }

    public void setAlphabet(String alphabet) {
        this.alphabet = alphabet;
    }

    public int getBitLength() {
        return bitLength;
    }

    public void setBitLength(int bitLength) {
        this.bitLength = bitLength;
    }

    private void setData(IAlphabetBinaryData data) {
        this.data = data;
    }

}
