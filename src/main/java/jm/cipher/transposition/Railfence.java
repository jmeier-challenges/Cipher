package jm.cipher.transposition;

import jm.cipher.AbstractCipher;
import jm.tools.Language;
import jm.cipher.Memento;
import jm.cipher.transposition.data.IRailFenceData;
import jm.tools.Text;
import jm.util.BestN;

import java.util.ArrayList;
import java.util.List;

/**
 * User: T05365A
 * Date: 21.08.12
 * Time: 09:46
 */
public class Railfence extends AbstractCipher implements IRailFenceData {
    private int startRail = 0;
    private int railCount = 2;
    private IRailFenceData data;

    public static void main(String[] args) throws Exception {
        Railfence cipher = new Railfence();


	    String ct = "dnetlhseedheswloteateftaafcl";
	    String trimmedCt = Text.getStripped(ct);
    }

    public Railfence() {
        setIData(this);
    }

    @Override
    public String getDisplayname() {
        return "Railfence";
    }

    @Override
    public String getName() {
        return "Railfence";
    }

    @Override
    public String getDescription() {
        return "Railfence";
    }

    @Override
    public void setRailCount(int count) {
        railCount = count;
    }

    @Override
    public int getRailCount() {
        return railCount;
    }

    @Override
    public void setStartRail(int rail) {
        startRail = rail;
    }

    @Override
    public int getStartRail() {
        return startRail;
    }

    public int[] ptToCtIndex(int length) {
        List<List<Integer>> ptToCt = new ArrayList<List<Integer>>();
        for (int i = 0; i < data.getRailCount(); i++) {
            ptToCt.add(new ArrayList<Integer>());
        }

        int currentRail = data.getStartRail() % data.getRailCount();
        int diff = 1;
        for (int i = 0; i < length; i++) {

            List<Integer> rail = ptToCt.get(currentRail);
            rail.add(i);

            if (diff == 1 && currentRail == data.getRailCount() - 1) {
                diff *= -1;
            } else if (diff == -1 && currentRail == 0) {
                diff *= -1;
            }

            currentRail += diff;
        }

        int[] indexes = new int[length];
        int pos = 0;
        for (List<Integer> rail : ptToCt) {
            for (Integer integer : rail) {
                indexes[pos++] =  integer;
            }
        }

        return indexes;
    }

    @Override
    public String encrypt(String pt) throws Exception {
        int length = pt.length();
        int[] indexe = ptToCtIndex(length);
        char[] ct = new char[length];
        for (int i = 0; i < length; i++) {
            ct[i] = pt.charAt(indexe[i]);
        }
        return new String(ct);
    }

    @Override
    public String decrypt(String ct) throws Exception {
        int length = ct.length();
        int[] indexe = ptToCtIndex(length);
        char[] pt = new char[length];
        for (int i = 0; i < length; i++) {
            pt[indexe[i]] = ct.charAt(i);
        }
        return new String(pt);
    }

    @Override
    public void setIData(Object data) {
        this.data = (IRailFenceData) data;
    }

    @Override
    public void setMemento(Memento memento) {
        if (memento instanceof RailfenceMemento) {
            data.setRailCount(((RailfenceMemento)memento).railCount);
            data.setStartRail(((RailfenceMemento) memento).startRail);
        }
    }

    @Override
    public Memento getMemento() {
        RailfenceMemento memento = new RailfenceMemento();
        memento.railCount = data.getRailCount();
        memento.startRail = data.getStartRail();
        return memento;
    }

    private class RailfenceMemento extends Memento {
        public int railCount;
        public int startRail;
    }
}
