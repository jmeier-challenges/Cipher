package jm.cipher.transposition;

import jm.cipher.AbstractCipher;
import jm.cipher.Memento;
import jm.cipher.data.ITranspositionData;
import jm.tools.Text;

import java.util.Arrays;

/**
 * User: johann
 * Date: 10.03.11
 * Time: 09:28
 */
public class ColumnarTransposition extends AbstractCipher implements ITranspositionData {
    public static final String[] CommonAlphabets = {Text.LC_ALPHABET, Text.UC_ALPHABET};
    private ITranspositionData data;
    private int[] permutation = null;

    public static void main(String[] args) {
        int max = 9;
        if (args.length == 1) {
            try {
                max = Integer.parseInt(args[0]);
            } catch (NumberFormatException ex) {
                System.out.println("First parameter must be an integer!\nUsing " + max + " instead.");
            }
        }
        System.out.println("Args length = " + args.length);
        System.out.println("Max keylength = " + max);
        ColumnarTransposition trans = new ColumnarTransposition();
	    trans.setKey("wolfang");
	    String pt = "holla die waldfee";
	    String ct = trans.encrypt(pt);
	    System.out.println(pt);
	    System.out.println(ct);
	    System.out.println(trans.decrypt(ct));

    }

    public String getDisplayname() {
        return "ColumnarTransposition";
    }

    public String getName() {
        return "ColumnarTransposition";
    }

    public String getDescription() {
        return "ColumnarTransposition";
    }

    public String getKey() {
        return data.getKey();
    }

    public void setKey(String key) {
        data.setKey(key);
        if (key != null) {
            setKeyLength(key.length());
        } else {
            setKeyLength(0);
        }
    }

    public int getKeyLength() {
        return data.getKeyLength();
    }

    public void setKeyLength(int length) {
        data.setKeyLength(length);
    }

    @Override
    public void setIData(Object data) {
        this.data = (ITranspositionData) data;
    }

    public ColumnarTransposition() {
        setIData(new TranspositionData());
    }

    @Override
    public Memento getMemento() {
        TranspositionData memento = new TranspositionData();
        memento.setKey(getKey());
        memento.setKeyLength(getKeyLength());
	    memento.setBlockwise(isBlockwise());
        memento.setAlphabet(getAlphabet());
        return memento;
    }

    @Override
    public void setMemento(Memento memento) {
        data.setKey(((TranspositionData)memento).getKey());
        data.setKeyLength(((TranspositionData) memento).getKeyLength());
        data.setBlockwise(((TranspositionData) memento).isBlockwise());
        data.setAlphabet(((TranspositionData) memento).getAlphabet());
    }

    public int[] getDecryptPermutation() {
        int[] permutation = getPermutation();
        int[] decryptPermutation = new int[permutation.length];

        for (int i = 0; i < permutation.length; i++) {
            decryptPermutation[permutation[i]] = i;
        }

        return decryptPermutation;
    }

    private int[] getPermutation() {
        if (permutation != null) {
            return permutation;
        }
        String key = data.getKey();
        if (key == null || key.isEmpty()) {
            int keyLength = data.getKeyLength();
            permutation = new int[keyLength];
            for (int i = 0; i < keyLength; i++) {
                permutation[i] = i;
            }
        } else {
            permutation = new int[key.length()];

            char[] chars = key.toCharArray();
            Arrays.sort(chars);

            for (int j = 0; j < key.length(); j++) {
                char c = key.charAt(j);
                for (int i = 0; i < chars.length; i++) {
                    if (chars[i] == c) {
                        permutation[i] = j;
	                    chars[i] = 0;
                        break;
                    }
                }
            }
        }
        return permutation;
    }

	public void setPermutation(int[] aPermutation) {
		permutation = aPermutation;
	}

    public String encrypt(String pt) {
        if (data.isBlockwise()) {
            return encryptBlockwise(pt);
        }
        int[] permutation = getPermutation();
	    System.out.println("permutation: " + Arrays.toString(permutation));

        int keyLength = permutation.length;
        int length = pt.length();
        int rowCount = length / keyLength;
        if (length % keyLength != 0) {
            rowCount++;
        }

        Character[][] grid = new Character[rowCount][keyLength];

        int pos = 0;
        for (int row = 0; row < rowCount; row++) {
            for (int col = 0; col < keyLength; col++) {
                if (pos < length) {
                    grid[row][col] = pt.charAt(pos++);
                } else {
                    grid[row][col] = null;
                }
            }
        }

        StringBuilder builder = new StringBuilder();

        for (int col = 0; col < keyLength; col++) {
            for (int row = 0; row < rowCount; row++) {
                Character c = grid[row][permutation[col]];
                if (c != null) {
                    builder.append(c);
                }
            }
        }

        return builder.toString();
    }

    private String encryptBlockwise(String pt) {
        int len = pt.length();
        StringBuilder ct = new StringBuilder(len);
        int[] permutation = getPermutation();
        for (int i = 0; i < pt.length(); i++) {
            int col = (i/permutation.length)*permutation.length + permutation[i % permutation.length];
            if (col < len) {
                ct.append(pt.charAt(col));
            }
        }
        return ct.toString();
    }

    private String decryptBlockwise(String ct) {
        int len = ct.length();
        StringBuilder pt = new StringBuilder(len);
        int[] permutation = getDecryptPermutation();

        for (int i = 0; i < ct.length(); i++) {
            int col = (i/permutation.length)*permutation.length + permutation[i % permutation.length];
            if (col < len) {
                pt.append(ct.charAt(col));
            }
        }
        return pt.toString();
    }

    public String decrypt(String ct) {
        if (data.isBlockwise()) {
            return decryptBlockwise(ct);
        }
        int[] permutation = getPermutation();

        int keyLength = permutation.length;
        int length = ct.length();
        int rowCount = length / keyLength;
        if (length % keyLength != 0) {
            rowCount++;
        }

        int completeCols = length % keyLength;
        if (completeCols == 0) {
            completeCols = keyLength;
        }

        Character[][] rows = new Character[rowCount][keyLength];


        int pos = 0;
        for (int col = 0; col < keyLength; col++) {
            for (int row = 0; row < rowCount; row++) {
                int realCol = permutation[col];
                if (realCol < completeCols || row < rowCount - 1) {
                    rows[row][realCol] = ct.charAt(pos++);
                } else {
                    rows[row][realCol] = null;
                }
            }
        }

        StringBuilder builder = new StringBuilder();
        for (int row = 0; row < rowCount; row++) {
            for (int col = 0; col < keyLength; col++) {
                Character c = rows[row][col];
                if (c != null) {
                    builder.append(c);
                }
            }
        }

        return builder.toString();
    }

    public void setBlockwise(boolean b) {
        data.setBlockwise(b);
    }

    public boolean isBlockwise() {
        return data.isBlockwise();
    }

    @Override
    public void setAlphabet(String alphabet) {
        data.setAlphabet(alphabet);
    }

    @Override
    public String getAlphabet() {
        return data.getAlphabet();
    }

    private class TranspositionData extends Memento implements ITranspositionData {
        private String key;
        private int keyLength;
        private boolean blockwise;
        private String alphabet;

        @Override
        public void setKey(String key) {
            this.key = key;
        }

        @Override
        public String getKey() {
            return key;
        }

        @Override
        public void setKeyLength(int keyLength) {
            this.keyLength = keyLength;
        }

        @Override
        public int getKeyLength() {
            return keyLength;
        }

        @Override
        public void setBlockwise(boolean blockwise) {
            this.blockwise = blockwise;
        }

        @Override
        public boolean isBlockwise() {
            return blockwise;
        }

        @Override
        public void setAlphabet(String alphabet) {
            this.alphabet = alphabet;
        }

        @Override
        public String getAlphabet() {
            return alphabet;
        }
    }
}
