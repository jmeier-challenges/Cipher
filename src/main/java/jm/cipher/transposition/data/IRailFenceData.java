package jm.cipher.transposition.data;

/**
 * User: T05365A
 * Date: 21.08.12
 * Time: 09:48
 */
public interface IRailFenceData {
    void setRailCount(int count);
    int getRailCount();

    void setStartRail(int rail);
    int getStartRail();
}
