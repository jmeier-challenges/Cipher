package jm.cipher;

import jm.cipher.data.IVigenereData;
import jm.tools.Text;

/*
http://www.sichere.it/cryptologia.php?language=DE
 */
public class Vigenere extends AbstractCipher implements IVigenereData {
    public static final String[] CommonAlphabets = {Text.LC_ALPHABET, Text.UC_ALPHABET};

    private IVigenereData data;

	public static void main(String[] args) throws Exception {
		Vigenere viggi = new Vigenere();
		viggi.setKey("clyrtjsrigvdp");
		String ct = "hzsigrfvbnmhttenvzxyvmkmhttwtcmjzzmtpgdgrxbjrvsvkvgdrfnvowvzmtapnuvamitvraubmgyvxbzmxlbklg";

		System.out.println(ct);
		System.out.println(viggi.decrypt(ct));
	}

	public Vigenere() {
		setIData(new VigenereData());
        setAlphabet(Text.LC_ALPHABET);
	}

    public String getDisplayname() {
        return "Vigenere";
    }

	public String decrypt(String ct, int[] key) {
		StringBuilder pt = new StringBuilder(ct.length());
        int keylength = key.length;
        int keyPos = 0;
        String alphabet = getAlphabet();
        int alphabetLength = alphabet.length();
        for (char c : ct.toCharArray()) {
            int index = alphabet.indexOf(c);
            if (index < 0) {
                pt.append(c);

            } else {
                index = (alphabet.indexOf(c) -key[keyPos % keylength] + alphabetLength) % alphabetLength;
                pt.append(alphabet.charAt(index));
                keyPos = ++keyPos % keylength;
            }
        }
		return pt.toString();
	}

	public String getName() {
        return "Vigenere";
    }

    public String getDescription() {
        return "Vigenere";
    }

    public void setIData(Object iData) {
        data = (IVigenereData) iData;
    }

/*
	 * Dies ist ein zu verschluesselnder Text
	 * akeyakeyakeyakeyakeyakeyakeyakeyakeyakey
	 *
	 */

	@Override
	public String encrypt(String pt) {
		String alphabet = getAlphabet();
		int alphabetLength = alphabet.length();

		String key = getKey();
		int keyLength = key.length();

		StringBuilder ct = new StringBuilder(pt.length());
		int keyPos = 0;

		for (char pc : pt.toCharArray()) {
			if (alphabet.indexOf(pc) < 0) {
				ct.append(pc);
			} else {
				char keyC = key.charAt(keyPos);
				int shift = alphabet.indexOf(keyC);
				int posPc = alphabet.indexOf(pc);
				ct.append(alphabet.charAt((posPc + shift)% alphabetLength));
                keyPos = ++keyPos % keyLength;
			}
		}

		return ct.toString();
	}

	public String decrypt(String ct) {
		String key = getKey();
		int keyLength = key.length();

		String alphabet = getAlphabet();
		int alphabetLength = alphabet.length();

		StringBuilder pt = new StringBuilder(ct.length());
		int keyPos = 0;

		for (char cc : ct.toCharArray()) {
			if (alphabet.indexOf(cc) < 0) {
				pt.append(cc);
			} else {
				char keyC = key.charAt(keyPos);
				int shift = alphabet.indexOf(keyC);
				int posCc = alphabet.indexOf(cc);
				pt.append(alphabet.charAt((posCc + alphabetLength - shift) % alphabetLength));
                keyPos = ++keyPos % keyLength;
			}
		}

		return pt.toString();
	}

    public String getKey() {
        return data.getKey();
    }

    public void setKey(String key) {
        data.setKey(key);
    }

    @Override
    public void setAlphabet(String alphabet) {
        data.setAlphabet(alphabet);
    }

    @Override
    public String getAlphabet() {
        return data.getAlphabet();
    }

    class VigenereData implements IVigenereData {
		private String key;
        private String alphabet;

		@Override
		public String getKey() {
			return key;
		}

		@Override
		public void setKey(String aKey) {
			key = aKey;
		}

        @Override
        public void setAlphabet(String alphabet) {
            this.alphabet = alphabet;
        }

        @Override
        public String getAlphabet() {
            return alphabet;
        }
    }

}

