package jm.cipher.dataimpl;

import jm.cipher.Memento;
import jm.cipher.data.IHandyData;

public class HandyData extends Memento implements IHandyData {

	private HANDY_TYPE type;

	public HANDY_TYPE getType() {
		return type;
	}

	public void setType(HANDY_TYPE type) {
		this.type = type;
	}
}