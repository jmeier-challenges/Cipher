package jm.cipher.dataimpl;

import jm.cipher.Memento;
import jm.cipher.data.ICaesarData;

public class CaesarData extends Memento implements ICaesarData {
		private String key;
        private String alphabet;
        private boolean preserveCase;

		@Override
		public String getKey() {
			return key;
		}

        @Override
        public void setAlphabet(String alphabet) {
            this.alphabet = alphabet;
        }

        @Override
        public String getAlphabet() {
            return alphabet;
        }

        @Override
        public void setPreserveCase(boolean b) {
            preserveCase = b;
        }

        @Override
        public boolean isPreserverCase() {
            return preserveCase;
        }

        @Override
		public void setKey(String aKey) {
			key = aKey;
		}
	}