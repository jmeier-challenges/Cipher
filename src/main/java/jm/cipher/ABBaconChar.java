package jm.cipher;

public class ABBaconChar extends BaconCharImpl {
	public boolean isBaconAImpl(final char c) {
		return Character.isLetter(c) && (c == 'a' || c == 'A');
	}
	public boolean isBaconBImpl(final char c) {
		return Character.isLetter(c) && (c == 'b' || c == 'B');
	}
}
