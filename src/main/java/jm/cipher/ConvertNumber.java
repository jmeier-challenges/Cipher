package jm.cipher;

import jm.cipher.data.IConvertNumberData;

/**
 * User: T05365A
 * Date: 10.08.12
 * Time: 14:14
 */
public class ConvertNumber extends AbstractCipher implements IConvertNumberData {
    IConvertNumberData data;
    private String toDigits;
    private String fromDigits;

    @Override
    public String encrypt(String pt) throws Exception {
        StringBuilder ct = new StringBuilder();
        String[] numbers = pt.split("[ \n]");
        for (String number : numbers) {
            if (!number.isEmpty()) {
                ct.append(jm.math.Number.convert(number, data.getFromDigits(), data.getToDigits())).append(" ");
            }
        }
        return ct.toString();
    }

    @Override
    public String decrypt(String ct) throws Exception {
        StringBuilder pt = new StringBuilder();
        String[] numbers = ct.split("[ \n]");
        for (String number : numbers) {
            if (!number.isEmpty()) {
                pt.append(jm.math.Number.convert(number, data.getFromDigits(), data.getToDigits())).append(" ");
            }
        }
        return pt.toString();
    }

    @Override
    public String getDisplayname() {
        return "Convert Number";
    }

    @Override
    public String getName() {
        return "ConvertNumber";
    }

    @Override
    public String getDescription() {
        return "Convert Number";
    }

    @Override
    public String getFromDigits() {
        return fromDigits;
    }

    @Override
    public void setFromDigits(String digits) {
        fromDigits = digits;
    }

    @Override
    public String getToDigits() {
        return toDigits;
    }

    @Override
    public void setToDigits(String digits) {
        toDigits = digits;
    }

    @Override
    public void setIData(Object data) {
        this.data = (IConvertNumberData) data;
    }

    @Override
    public void setMemento(Memento memento) {
        if (memento instanceof ConvertNumberMemento) {
            data.setFromDigits(((ConvertNumberMemento)memento).fromDigits);
            data.setToDigits(((ConvertNumberMemento) memento).toDigits);
        }
    }

    @Override
    public Memento getMemento() {
        ConvertNumberMemento memento = new ConvertNumberMemento();
        memento.fromDigits = data.getFromDigits();
        memento.toDigits = data.getToDigits();
        return memento;
    }

    private class ConvertNumberMemento extends Memento {
        String fromDigits;
        String toDigits;
    }
}
