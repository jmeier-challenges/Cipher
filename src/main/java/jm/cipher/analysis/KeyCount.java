package jm.cipher.analysis;

import java.util.Comparator;

public class KeyCount implements Comparable<Object> {
    public static final CountComparator CountComparator = new CountComparator(false);
    public static final KeyComparator KeyComparator = new KeyComparator(false);

    public static final CountComparator CountReverseComparator = new CountComparator(true);
    public static final KeyComparator KeyReverseComparator = new KeyComparator(true);

    private String key;
    private Integer count;

    public KeyCount(String key, Integer count) {
        this.key = key;
        this.count = count;
    }

    @Override
    public int compareTo(Object o) {
        if (o instanceof KeyCount) {
            return CountReverseComparator.compare(this, (KeyCount) o);
        } else {
            return -1;
        }
    }

    public void increment() {
        count++;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof KeyCount) {
            KeyCount keyCount = (KeyCount) obj;
            return equals(key, keyCount.key);
        } else {
            return super.equals(obj);
        }
    }

    private boolean equals(Object left, Object right) {
        if (left != null) {
            return left.equals(right);
        } else if (right != null) {
            return right.equals(left);
        } else {
            return true;
        }
    }

    @Override
    public String toString() {
        return "{" + key + ',' + count + '}';
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    static class CountComparator implements Comparator<KeyCount> {
        private int reverse;

        CountComparator(boolean reverse) {
            this.reverse = reverse ? -1 : 1;
        }

        @Override
        public int compare(KeyCount leftO, KeyCount rightO) {
            KeyCount left = leftO;
            KeyCount right = rightO;

            if (left.equals(right)) {
                return 0;
            } else if (left.count != null) {
                if (left.count.equals(right.count)) {
                    return left.key.compareTo(right.key);
                } else {
                    return left.count.compareTo(right.count) * reverse;
                }
            } else if (right.count == 0) {
                return 0;
            } else {
                return -1;
            }
        }
    }

    static class KeyComparator implements Comparator<KeyCount> {
        private int reverse;

        public KeyComparator(boolean reverse) {
            this.reverse = reverse ? -1 : 1;
        }

        @Override
        public int compare(KeyCount left, KeyCount right) {
            if (left == null && right == null) {
                return 0;
            } else if (left == null) {
                return -1;
            } else if (right == null) {
                return -1;
            }

            if (left.key != null) {
                return left.key.compareTo(right.key) * reverse;
            } else if (right == null) {
                return 0;
            } else {
                return -1;
            }
        }
    }
}


