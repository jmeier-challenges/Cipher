package jm.cipher.analysis;

import jm.tools.Text;

import java.util.*;

/**
 * User: johann
 * Date: 07.10.12
 * Time: 08:15
 */
public class TextAnalysis {
    private String text;
    private List<KeyCount> characterCount;
    private List<KeyCount> bigramCount;
    private List<KeyCount> trigramCount;
    private List<KeyCount> quadgramCount;

    public static void main(String[] args) {
        String text = "wolfgang ist ein ganz toller kerl und er macht leider viel zu wenig sort";

        TextAnalysis textAnalysis = new TextAnalysis(text);

        System.out.println(textAnalysis.characterCount);

        System.out.println(textAnalysis.bigramCount);

        System.out.println(textAnalysis.trigramCount);

        System.out.println(textAnalysis.quadgramCount);

    }

    public TextAnalysis(String text) {
        this.text = text;
        characterCount = new ArrayList<KeyCount>();
        bigramCount = new ArrayList<KeyCount>();
        trigramCount = new ArrayList<KeyCount>();
        quadgramCount = new ArrayList<KeyCount>();
        count();
        Collections.sort(characterCount);
        Collections.sort(bigramCount);
        Collections.sort(trigramCount);
        Collections.sort(quadgramCount);
    }

    private void count() {
        int length = text.length();
        for (int i = 0; i < length; i++) {
            String c = text.substring(i, i + 1);
            add(characterCount, c);
        }

        String stripped = Text.getStripped(text.toLowerCase(), Text.LC_ALPHABET);

        length = stripped.length();
        for (int i = 0; i < stripped.length(); i++) {
            if (length - i >= 2) {
                add(bigramCount, stripped.substring(i, i + 2));
            }
            if (length - i >= 3) {
                add(trigramCount, stripped.substring(i, i + 3));
            }
            if (length - i >= 4) {
                add(quadgramCount, stripped.substring(i, i + 4));
            }
        }
    }

    private void add(List<KeyCount> counts, String key) {
        KeyCount keyCount = new KeyCount(key, 1);
        int pos = counts.indexOf(keyCount);
        if (pos < 0) {
            counts.add(keyCount);
        } else {
            counts.get(pos).increment();
        }
    }

    public List<KeyCount> getCharacterFrequency() {
        return characterCount;
    }

    public List<KeyCount> getBigramFrequency() {
        return bigramCount;
    }

    public List<KeyCount> getTrigramFrequency() {
        return trigramCount;
    }

    public List<KeyCount> getQuadgrammFrequency() {
        return quadgramCount;
    }

}
