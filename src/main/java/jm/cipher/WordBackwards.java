package jm.cipher;

/**
 * User: T05365A
 * Date: 24.07.12
 * Time: 16:54
 */
public class WordBackwards extends AbstractCipher {
    private String wordDelimiter = " \n";

    public String encrypt(String pt) throws Exception {
        String[] words = pt.split(wordDelimiter);
        StringBuffer ct = new StringBuffer();
        for (String word : words) {
            StringBuffer pw = new StringBuffer(word);
            ct.append(pw.reverse()).append(" ");
        }
        return ct.toString();
    }

    public String decrypt(String ct) throws Exception {
        return encrypt(ct);
    }

    public String getDisplayname() {
        return "Word backwards";
    }

    public String getName() {
        return "Wordbackwards";
    }

    public String getDescription() {
        return "Writes every word from back to front";
    }

    public static void main(String[] args) throws Exception {
        WordBackwards cipher = new WordBackwards();
        String ct = "nebeis thca llun reiv snie nuen reiv\n" +
                "fneuf thca reiv fneuf llun fle llun\n";
        System.out.println(cipher.decrypt(ct));
    }
}
