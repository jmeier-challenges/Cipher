package jm.cipher;

import jm.cipher.data.IVariableBaseToIntData;

import java.util.List;

public class VariableBaseToInt extends AbstractCipher implements IVariableBaseToIntData {
    private IVariableBaseToIntData data;

    @Override
    public String encrypt(String pt) throws Exception {
        return "not yet implemented";
    }

    @Override
    public String decrypt(String ct) throws Exception {
        StringBuilder pt = new StringBuilder();
        for (String number : ct.split("[ \n]")) {
            pt.append(decrypt(number, data.getBaseChars())).append(" ");
        }
        return pt.toString();
    }

    @Override
    public String getDisplayname() {
        return "Variable base to int converison";
    }

    @Override
    public String getName() {
        return "VariableBaseToInt";
    }

    @Override
    public String getDescription() {
        return "Convert numbers with different bases to integer. The characters for each position can be given. If only" +
                "one base is given, it is used for every position";
    }

    @Override
    public void setIData(Object data) {
        setData((IVariableBaseToIntData) data);
    }

    public void setData(IVariableBaseToIntData data) {
        this.data = data;
    }

    @Override
    public List<String> getBaseChars() {
        return data.getBaseChars();
    }

    static long decrypt(String ct, List<String> baseChars) {
        long result = 0;
        int size = ct.length() - 1;
        for (int pos = 0; pos < ct.length(); pos++) {
            char c = ct.charAt(pos);
            String base = getBaseChars(pos, baseChars);
            long mult = getMult(size - pos, baseChars);
            int value = base.indexOf(c);
            result +=  value * mult;
        }

        return result;
    }

    static long getMult(int length, List<String> baseChars) {

        if (baseChars == null) {
            return 0;
        }

        long mult = 1;

        int size = baseChars.size() - 1;

        for (int i = 0; i < length; i++) {

            String chars = getBaseChars(size - i, baseChars);

            mult *= chars.length();
        }

        return mult;
    }

    static String getBaseChars(int pos, List<String> baseChars) {
        if (baseChars == null || baseChars.isEmpty() || pos < 0) {
            return "";
        }

        return baseChars.get(pos % baseChars.size());
    }
}
