package jm.cipher;

import jm.cipher.data.ICaesarData;
import jm.math.ModuloArithmetik;
import jm.tools.Text;

public class Caesar extends AbstractCipher implements ICaesarData {
    public static final String[] CommonAlphabets = {Text.LC_ALPHABET, Text.LC_ALPHABET + Text.UC_ALPHABET};

    private ICaesarData data;

	public static void main(String[] args) {
		// Test
        Caesar caesar = new Caesar();
        String ct = "Uzvjve Trtyv yrkkv zty jtyfe jvzk uvi Mviöwwvekcztylex rlw uvd Jtyzid, mvijgirty vi ufty jtyfe rlwxileu jvzevi Rlwdrtylex vze svjfeuvivj Vicvsezj. Leu zty jfcckv eztyk vekkäljtyk nviuve...  Ertyuvd urj Iäkjvc uree zixveunree xvcöjk nri jgirty zty xvf_ileevi leu jgrisifvktyve re leu wirxkv, fs jzv uvee Cljk rlw uzvjvj Kvzc yäkkve. Uzvjv nri mfiyreuve leu jf drtykve nzi lej yvlkv rlw uve Nvx. Cvzuvi nri rsvi yvlkv eztyk lejvi svjkvi Jlty- sqn. Wzeuv-Krx, uvee jfnfyc Jkrxv 1 leu Jkrxv 2 bfeekve nzi cvkqkveuczty eli ulity vzeve KA zuvekzwzqzvive (Zty eveev bvzev Erdve, rsvi zty svurebv dzty xreq yviqczty wüi Uvzev Yzcwv!). Svzuv Drcv nrive nzi uire, jltykve rsvi jtyvzesri eztyk xverl xvelx. Vxrc, zd Zeevive uvj Xvsäluv nliuve nzi uree nzvuvi ql Bzeuvie rlw vzevd Rsvekvlvijgzvcgcrkq. Urj Ulityjkivleve uvi Iäldcztybvzkve rlw uvi Jltyv erty Zewf'j yrk Dvxrjgrß xvdrtyk. Szj rlw vzev bfeekve nzi rlty rccv wzeuve. Yzvi brd uree vze uizkkvj leu cvkqkvj Drc uvi KA qld Vzejrkq (Gvzeczty, gvzeczty, zty nvzß ). Rccvj Nvzkviv bfeekve nzi uree rsvi lexcrlscztyvinvzjv fyev wivduvj Qlkle wzeuve leu cöjve. Jfdzk eryd urj Xreqv vze xlkvj Veuv leu nzi bfeekve mvixeüxk erty Yrljv rsqzvyve.  Czvsvi Fnevi, mzvcve Ureb wüi uve Jgrß. Rlty nvee uzvjv Trtyvrik eztyk Uvzev Zuvv zjk, jf yrjk Ul ufty urj Xreqv gviwvbk erty Jkvze kirejwvizvik. Xifjjvj Bzef!  KWKT  Trtyv-Evnszv";
		caesar.setPreserveCase(true);
        caesar.setAlphabet(Text.LC_ALPHABET);
        caesar.setShift(19);
        String pt = caesar.decrypt(ct);
        System.out.println(pt);
	}


	public Caesar() {
		data = new CaesarData();
		data.setAlphabet(Text.LC_ALPHABET);
	}

	public String encrypt(String pt) {
        return crypt(pt, getShift());
    }

	private String crypt(String pt, int shift) {
		StringBuilder ct = new StringBuilder(pt.length());
		String alphabet = data.getAlphabet();
        int alphabetLength = alphabet.length();

		for (char c : pt.toCharArray()) {
            boolean uppercase = Character.isUpperCase(c);
            int index = getIndex(c);
            if (index < 0) {
                ct.append(c);
            } else {
                ct.append(getChar((index + shift + alphabetLength) % alphabetLength, uppercase));
            }
        }
		return ct.toString();
	}

    private char getChar(int index, boolean uppercase) {
        String alphabet = getAlphabet();
        if (isPreserverCase()) {
            if (uppercase) {
                return alphabet.toUpperCase().charAt(index);
            } else {
                return alphabet.toLowerCase().charAt(index);
            }
        } else {
            return alphabet.charAt(index);
        }
    }

    private int getIndex(char c) {
        String alphabet = getAlphabet();
        if (isPreserverCase()) {
            boolean uppercase = Character.isUpperCase(c);
            if (uppercase) {
                return alphabet.toUpperCase().indexOf(c);
            } else {
                return alphabet.toLowerCase().indexOf(c);
            }

        } else {
            return alphabet.indexOf(c);
        }
    }

    public String decrypt(String ct) {
        return crypt(ct, -getShift());
    }

    public String getDisplayname() {
        return "Caesar";
    }

    public String getName() {
        return "Caesar";
    }

    public String getDescription() {
        return "Caesar";
    }

    public void setIData(Object data) {
	    this.data = (ICaesarData) data;
    }


    @Override
    public Memento getMemento() {
        CaesarData memento = new CaesarData();
        memento.setAlphabet(data.getAlphabet());
        memento.setKey(data.getKey());
        memento.setPreserveCase(data.isPreserverCase());

        return memento;
    }

    @Override
    public void setMemento(Memento memento) {
        if (memento instanceof CaesarData) {
            data.setAlphabet(((CaesarData) memento).getAlphabet());
            data.setKey(((CaesarData) memento).getKey());
            data.setPreserveCase(((CaesarData) memento).isPreserverCase());
        }
    }

    @Override
    public void setAlphabet(String alphabet) {
        data.setAlphabet(alphabet);
    }

    @Override
    public String getAlphabet() {
        return data.getAlphabet();
    }

    @Override
    public void setPreserveCase(boolean b) {
        data.setPreserveCase(b);
    }

    @Override
    public boolean isPreserverCase() {
        return data.isPreserverCase();
    }

    public void setKey(String key) {

		data.setKey(key);
	}

	public String getKey() {
		String key = data.getKey();
		if (!Text.isEmpty(key)) {
			try {
				String alphabet = data.getAlphabet();
				ModuloArithmetik moduloArithmetik = new ModuloArithmetik(alphabet.length());

				int shift = Integer.parseInt(key);

				key = String.valueOf(alphabet.charAt((int) moduloArithmetik.getModulo(shift - 1)));
			} catch (NumberFormatException ex) {
				// ignore
			}
		}
		return key;
	}

	public int getShift() {
		String key = getKey();
		String ptAlphabet = getAlphabet();
		ModuloArithmetik moduloArithmetik = new ModuloArithmetik(ptAlphabet.length());
		return (int) moduloArithmetik.getModulo(ptAlphabet.indexOf(key.charAt(0)) + 1);
	}

	public void setShift(int shift) {
		String ptAlphabet = getAlphabet();
        setKey(String.valueOf(ptAlphabet.charAt((shift - 1) % ptAlphabet.length())));
	}

	class CaesarData extends Memento implements ICaesarData {
		private String key;
        private String alphabet;
        private boolean preserveCase;

		@Override
		public String getKey() {
			return key;
		}

        @Override
        public void setAlphabet(String alphabet) {
            this.alphabet = alphabet;
        }

        @Override
        public String getAlphabet() {
            return alphabet;
        }

        @Override
        public void setPreserveCase(boolean b) {
            preserveCase = b;
        }

        @Override
        public boolean isPreserverCase() {
            return preserveCase;
        }

        @Override
		public void setKey(String aKey) {
			key = aKey;
		}
	}
}


