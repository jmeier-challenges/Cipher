package jm.cipher;

import jm.cipher.data.IBifidData;
import jm.tools.Text;

/**
 * User: T05365A
 * Date: 02.10.12
 * Time: 10:00
 *
 * Breaking: http://cmup.fc.up.pt/cmup/v2/include/filedb.php?id=120&table=publicacoes&field=file
 */
public class Bifid extends AbstractCipher implements IBifidData {
	public static final String[] COMMON_ALPHABETS = {
		"abcdefghiklmnopqrstuvwxyz",
		"abcdefghijklmnopqrstuvwxyz0123456789"
	};
	private String equalChars;
	private boolean reverse;
	private String ptAlphabet;
	private String ctAlphabet;
	private int n;
	private String key;

	private Polybios polybios;

	public Bifid() {
		polybios = new Polybios();
		setEqualChars("ji");
		setReverse(false);
		setPtAlphabet(Polybios.COMMON_ALPHABETS[0]);
		setCtAlphabet("012345");
		setN(5);
	}

	public static void main(String[] args) throws Exception {
		Bifid cipher = new Bifid();

		String pt = "franz jagt im komplett verwahrlosten taxi quer durch bayern";
		//String ct = "icwbrm nndudv intcqw tddqfe dlfpqi rykete gcfsuo cqziwn fuh";
		pt = "abstract. in this paper we describe a fully automated ciphertext-only cryptanalysis attack on the" +
		"bifid cipher, for which the original text language is known. we have implemented this attack using" +
		"python. we use an easily computable statistical function to find the period of the cipher, and then" +
		"the key-table is generated in a fairly efficient way. the process is directed in such a way that strongly" +
		"narrows the search space of possible solutions. this results in a feasible attack to a bifid cryptogram," +
		"provided that its length is enough for accurate statistical analysis.";

		String ct = cipher.encrypt(pt);

		System.out.println(ct);
		System.out.println(cipher.decrypt(ct));
		String s = "11 44 41 14 23 42 24 31 31 45 11 14 14 21 11 24 33 51 44 33 14 11 12 32 14 41 54 33 35 14 53 41 31 35 42 41 44 11 23 34 21 12 22 11 23 21 42 34 52 21 24 21 34 22 23 13 41 54 31 32 41 21 24 23 35 35 12 15 12 33 31 31 34 11 42 24 14 41 12 44 23 23 54 23 35 14 41 13 11 42 35 13 33 44 11 31 44 14 24 42 11 32 43 14 23 34 32 23 14 21 31 42 31 32 42 11 23 21 41 31 42 13 42 12 15 41 13 12 42 13 14 14 11 23 12 12 43 51 22 21 21 34 51 54 21 34 31 14 42 41 24 11 41 12 34 41 21 51 54 21 44 44 33 23 53 14 43 54 42 14 11 41 24 31 11 32 33 44 21 31 43 34 42 33 44 22 44 14 43 44 23 12 11 42 13 11 44 11 24 31 12 22 11 45 34 32 41 33 43 52 11 14 21 42 44 31 32 42 24 13 34 22 23 41 11 44 14 14 41 42 44 21 13 13 13 54 24 12 34 21 34 43 43 43 51 55 22 54 53 32 42 51 15 11 41 54 42 14 54 34 53 52 45 34 43 14 32 45 41 31 14 34 31 44 13 54 34 35 24 14 43 45 35 21 42 23 43 34 35 42 42 43 11 45 34 11 32 51 25 43 53 42 32 53 11 54 25 15 25 34 54 43 43 14 41 35 53 43 25 44 34 32 55 35 13 51 34 14 34 25 54 12 15 34 14 43 44 31 11 53 34 44 34 41 43 44 35 55 24 44 41 43 53 45 35 21 34 43 53 43 55 54 41 21 54 32 53 52 14 54 43 11 14 21 45 11 43 45 34 21 44 35 52 43 53 34 34 42 53 45 44 33 53 31 21 44 31 43 42 43 21 43 12 24 23 43 53 51 23 33 51 35 41 54 33 42 15 34 15 44 43 34 34 32 53 51 43 43 11 51 34 21 51 44 13 54 41 24 14 43 24 54 42 21 25 24 14 45 44 31 44 43 15 32 43 43 53 45 23 14 21 33 52 14 53 41 44 34 43 11 13 11 43 43 ";
		s = Text.getStripped(s, cipher.getCtAlphabet());
		s = cipher.getTransposition2(s);
		System.out.println(s);
	}

	private String getTransposition1(String ct) {
		StringBuilder builder = new StringBuilder(ct.length());
		// even
		for (int i = 0; i < ct.length(); i += 2) {
			builder.append(ct.charAt(i));
		}
		// ods
		for (int i = 1; i < ct.length(); i += 2) {
			builder.append(ct.charAt(i));
		}

		return builder.toString();
	}

	@Override
	public String encrypt(String pt) throws Exception {
		String stripped = Text.replaceEquals(pt, getEqualChars());
		stripped = Text.getStripped(stripped, getPtAlphabet());
		Polybios polybios = getPolybios();
		String ct = polybios.encrypt(stripped);
		ct = Text.getStripped(ct, getCtAlphabet());
		ct = getTransposition1(ct);
		ct = polybios.decrypt(ct);

		return ct.toString();
	}

	@Override
	public String decrypt(String ct) throws Exception {
		Polybios polybios = getPolybios();
		String pt = polybios.encrypt(ct);
		pt = Text.getStripped(pt, getCtAlphabet());
		pt = getTransposition2(pt);
		pt = polybios.decrypt(pt);
		return pt;
	}

	private String getTransposition2(String pt) {
		int len = pt.length();
		StringBuilder builder = new StringBuilder();
		if (len % 2 == 0) {
			int half = len / 2;
			for (int i = 0; i < half; i++) {
				builder.append(pt.charAt(i)).append(pt.charAt(i + half));
			}

		} else {
			throw new RuntimeException("Bfid.getTransposition2: length of pt must be a multiple of 2!");
		}
		return builder.toString();
	}

	private Polybios getPolybios() {
		polybios.setKey(getKey());
		polybios.setReverse(isReverse());
		polybios.setEqualChars(getEqualChars());
		polybios.setPtAlphabet(getPtAlphabet());
		polybios.setCtAlphabet(getCtAlphabet());
		polybios.setN(getN());
		return polybios;
	}


	@Override
	public String getDisplayname() {
		return "Bifid";
	}

	@Override
	public String getName() {
		return "Bifid";
	}

	@Override
	public String getDescription() {
		return "Bifid";
	}

	@Override
	public void setKey(String key) {
		this.key = key;
	}

	@Override
	public String getKey() {
		return key;
	}

	@Override
	public void setPtAlphabet(String alphabet) {
		ptAlphabet = alphabet;
	}

	@Override
	public String getPtAlphabet() {
		return ptAlphabet;
	}

	@Override
	public void setN(int n) {
		this.n = n;
	}

	@Override
	public int getN() {
		return n;
	}

	@Override
	public void setCtAlphabet(String alphabet) {
		ctAlphabet = alphabet;
	}

	@Override
	public String getCtAlphabet() {
		return ctAlphabet;
	}

	@Override
	public void setReverse(boolean reverse) {
		this.reverse = reverse;
	}

	@Override
	public boolean isReverse() {
		return reverse;
	}

	@Override
	public void setEqualChars(String equalChars) {
		this.equalChars = equalChars;
	}

	@Override
	public String getEqualChars() {
		return equalChars;
	}
}
