package jm.cipher;

/**
 * User: T05365A
 * Date: 31.07.12
 * Time: 14:20
 */
public abstract class AbstractCipher implements ICipher {

    @Override
    public void initializeData() {
    }

    @Override
    public void setIData(Object data) {
    }

    @Override
    public boolean canEncrypt() {
        return true;
    }

    @Override
    public boolean canDecrypt() {
        return true;
    }

    @Override
    public String toString() {
        return getDisplayname();
    }

    public void setMemento(Memento memento) {
        // do nothing
    }

    public Memento getMemento() {
        return new Memento();
    }

}
