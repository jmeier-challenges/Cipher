package jm.cipher;

import jm.cipher.data.IPolybiosData;
import jm.math.Matrix;
import jm.tools.Text;

/**
 * User: Johann
 * Date: 23.03.11
 * Time: 21:30
 */
public class Polybios extends AbstractCipher implements IPolybiosData {
	public static final String[] COMMON_ALPHABETS = {
		"abcdefghiklmnopqrstuvwxyz",
		"abcdefghijklmnopqrstuvwxyz0123456789"
	};
	private String ptAlphabet;
	private String ctAlphabet;
	private String key;
	private int n;
	private String equalChars;
	private boolean reverse;

    private char[][] square;
	private IPolybiosData data;


    public static void main(String[] args) {
        Polybios polybios = new Polybios();
	    polybios.initSquare();
	    polybios.setCtAlphabet("abcde");
	    polybios.setKey("wolfgang");
	    String text = "ja die munition";
	    String ct = polybios.encrypt(text);
	    System.out.println(ct);
	    System.out.println(polybios.decrypt(ct));
    }

	public Polybios() {
		setN(5);
		setCtAlphabet("123456");
		setKey("");
		setEqualChars("ji");
		setReverse(false);
		setIData(this);
	}

	public String encrypt(String pt) {
		initSquare();
		String stripped = Text.replaceEquals(pt, data.getEqualChars());
		stripped = Text.getStripped(stripped, data.getPtAlphabet());

		StringBuffer ct = new StringBuffer();
		for (char c : stripped.toCharArray()) {
			ct.append(getNumber(c)).append(" ");
		}
		return ct.toString();
	}

	public String decrypt(String ct) {
		initSquare();
		String stripped = Text.getStripped(ct, data.getCtAlphabet());
        StringBuffer pt = new StringBuffer();

		int count = stripped.length() / 2;
        for (int i = 0; i < count; i++) {
	        int x = data.getCtAlphabet().indexOf(stripped.charAt(i * 2));
	        int y = data.getCtAlphabet().indexOf(stripped.charAt(i * 2 + 1));
	        pt.append(square[x][y]);
        }
        return pt.toString();
    }

    public String getDisplayname() {
        return "Polybios";
    }

    public String getName() {
        return "Polybios";
    }

    public String getDescription() {
        return "Polybios";
    }

    private String getNumber(char c) {
        for (int i = 0; i < square.length; i++) {
            for (int k = 0; k < square[i].length; k++) {
                if (square[i][k] == c) {
	                return "" + data.getCtAlphabet().charAt(i) + data.getCtAlphabet().charAt(k);
                }
            }
        }
        return String.valueOf(c);
    }

	public String getPtAlphabet() {
		if (ptAlphabet == null) {
			int n = getN();
			if (n == 5) {
				ptAlphabet = "abcdefghiklmnopqrstuvwxyz";
			} else if (n == 6) {
				ptAlphabet = "abcdefghijklmnopqrstuvwxyz0123456789";
			} else {
				throw new RuntimeException("Invalid n : " + n);
			}
		}
		return ptAlphabet;
	}

	public void setPtAlphabet(String ptAlphabet) {
		this.ptAlphabet = ptAlphabet;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public int getN() {
		return n;
	}

	public void setN(int n) {
		this.n = n;
	}

	private void initSquare() {
		int n = data.getN();
		square = new char[n][n];

		String key = data.getKey();
		String alphabet = data.getPtAlphabet();

		alphabet = Text.getAlphabet(alphabet, key, data.isReverse());

		for (int i = 0; i < n; i++) {
			for (int k = 0; k < n; k++) {
				int pos = i * n + k;
				if (pos < alphabet.length()) {
					square[i][k] = alphabet.charAt(pos);
				}
			}
		}
	}

	public String getCtAlphabet() {
		return ctAlphabet;
	}

	public void setCtAlphabet(String ctAlphabet) {
		this.ctAlphabet = ctAlphabet;
	}

	public String getEqualChars() {
		return equalChars;
	}

	public void setEqualChars(String equalChars) {
		this.equalChars = equalChars;
	}

	public boolean isReverse() {
		return reverse;
	}

	public void setReverse(boolean reverse) {
		this.reverse = reverse;
	}

	@Override
	public void setIData(Object data) {
		this.data = (IPolybiosData) data;
	}
}
