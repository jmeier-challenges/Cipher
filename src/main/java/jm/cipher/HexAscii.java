package jm.cipher;

import java.net.URLDecoder;

/**
 * User: T05365A
 * Date: 24.02.11
 * Time: 11:50
 */
public class HexAscii {

	public static String toAscii(final String hexString) {
		StringBuffer result = new StringBuffer();
		String hex = hexString;
		if (hexString.length() % 2 != 0) {
			hex += "0";
		}
		for (int i = 0; i < hex.length(); i+=2) {
			String hexValue = hex.substring(i, i+2);
			try {
				char c = (char) Integer.parseInt(hexValue, 16);
				result.append(c);
			} catch (NumberFormatException ex) {
				result.append("?");
			}
		}
		return result.toString();
	}

	public static String toHex(final String pt) {
		StringBuffer ct = new StringBuffer();
		for (int i = 0; i < pt.length(); i++) {
			char c = pt.charAt(i);
			ct.append(charToHex(c));
		}
		return ct.toString();
	}

	private static String charToHex(final char c) {
		String hex = Integer.toHexString(c);
		if (hex.length() == 1) {
			hex = "0" + hex;
		}
		return hex;
	}

    public static void main(String[] args) {
        String html = "a%3A2%3A%7Bs%3A11%3A%22autologinid%22%3Bs%3A0%3A%22%22%3Bs%3A6%3A%22userid%22%3Bs%3A5%3A%2219237%22%3B%7D";
        html = "a%3A2%3A%7Bs%3A11%3A%22autologinid%22%3Bs%3A0%3A%22%22%3Bs%3A6%3A%22userid%22%3Bs%3A5%3A%2219237%22%3B%7D";
        System.out.println(URLDecoder.decode(html));

    }
}

