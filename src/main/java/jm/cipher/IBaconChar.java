package jm.cipher;

/**
 * User: T05365A
 * Date: 07.03.11
 * Time: 15:40
 */
public interface IBaconChar {
	boolean isBaconA(char c);
	boolean isBaconB(char c);
	void setSwap(boolean swap);
}
