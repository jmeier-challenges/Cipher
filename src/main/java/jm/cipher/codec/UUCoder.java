package jm.cipher.codec;

import jm.cipher.AbstractCipher;

/**
 * User: T05365A
 * Date: 31.07.12
 * Time: 16:28
 *
 * http://darksleep.com/player/JavaAndUnsignedTypes.html
 */
public class UUCoder extends AbstractCipher {

    public String encode(byte[] pt) {
        StringBuffer ct = new StringBuffer();
        int i;
        int length = pt.length;
        for (i = 0; i <= length - 3; i += 3) {
            ct.append(encode(pt[i], pt[i + 1], pt[i + 2]));
        }
        if (length % 3 == 1) {
            ct.append(encode(pt[i], (byte) ' ', (byte) ' '));
        } else if (length % 3 == 2) {
            ct.append(encode(pt[i], pt[i + 1], (byte) ' '));
        }

        return ct.toString();
    }

    private String encode(byte p1, byte p2, byte p3) {
        int c1 = p1 & 0xff;
        int c2 = p2 & 0xff;
        int c3 = p3 & 0xff;

        int b1 = (c1 >> 2) + 32;
        int b2 = (((c1 & 0x03) << 4) | (c2 >> 4)) + 32;
        int b3 = (((c2 & 0x0f) << 2) | (c3 & 0xc0) >> 6) + 32;
        int b4 = (c3 & 0x3f) + 32;

        String ct = "" + (char) (b1) + (char) (b2) + (char) (b3) + (char) (b4);

        return ct;
    }

    private byte[] decode(byte b1, byte b2, byte b3, byte b4) {
        int c1 = (b1 & 0xff) - 32;
        int c2 = (b2 & 0xff) - 32;
        int c3 = (b3 & 0xff) - 32;
        int c4 = (b4 & 0xff) - 32;

        int p1 = (c1 << 2) | ((c2 & 0x30) >> 4);
        int p2 = ((c2 & 0x0f) << 4) | (c3 >> 2);
        int p3 = ((c3 &0x03) << 6) | c4 ;

        return new byte[]{(byte) p1, (byte) p2, (byte) p3};
    }

    private byte[] decode(String ct) {
        if (ct.length() % 3 == 0) {
            byte[] pb = new byte[ct.getBytes().length / 4 * 3];
            byte[] cb = ct.getBytes();

            int pos = 0;
            for (int i = 0; i < cb.length; i += 4) {
                byte[] p = decode(cb[i], cb[i + 1], cb[i + 2], cb[i + 3]);
                pb[pos++] = p[0];
                pb[pos++] = p[1];
                pb[pos++] = p[2];
            }
            return pb;
        } else {
            return "Length of UUEncoded String must be multiple of 4".getBytes();
        }
    }


    public static void main(String[] args) {
        UUCoder coder = new UUCoder();
        String message = "Catalog and Homolog!";
        String ct = coder.encode(message.getBytes());
        String pt = new String(coder.decode(ct));
        System.out.println(message);
        System.out.println(ct+"4");
        System.out.println(pt);
    }

    @Override
    public String encrypt(String pt) throws Exception {
        return encode(pt.getBytes());
    }

    @Override
    public String decrypt(String ct) throws Exception {
        return new String(decode(ct));
    }

    @Override
    public String getDisplayname() {
        return "UU";
    }

    @Override
    public String getName() {
        return "UU";
    }

    @Override
    public String getDescription() {
        return "UU";
    }
}
