package jm.cipher.codec;

import jm.cipher.AbstractCipher;

import java.math.BigInteger;

/**
 * User: T05365A
 * Date: 15.11.12
 * Time: 10:44
 */
public class GCCode extends AbstractCipher {
	BigInteger correctionFactor = BigInteger.valueOf(411120);

	@Override
	public String encrypt(String pt) throws Exception {
		BigInteger i = new BigInteger(pt);

		if (i.compareTo(BigInteger.valueOf(0xffff)) <= 0) {
			return "GC" + jm.math.Number.convert(pt.toUpperCase(), "0123456789", "0123456789ABCDEF");
		} else {
			return "GC" + jm.math.Number.convert(i.add(correctionFactor).toString(), "0123456789", "0123456789ABCDEFGHJKMNPQRTVWXYZ");
		}
	}

	private String getStripped(String pt) {
		if (pt.startsWith("GC")) {
			return pt.substring(2);
		} else {
			return pt;
		}
	}

	@Override
	public String decrypt(String ct) throws Exception {
		String stripped = getStripped(ct).toUpperCase();
		if (stripped.length() <= 4) {
			return jm.math.Number.convert(stripped, "0123456789ABCDEF", "0123456789");
		} else {
			String valueString = jm.math.Number.convert(stripped, "0123456789ABCDEFGHJKMNPQRTVWXYZ", "0123456789");
			BigInteger value = new BigInteger(valueString);
			return value.subtract(correctionFactor).toString();
		}
	}

	@Override
	public String getDisplayname() {
		return "GC Code";
	}

	@Override
	public String getName() {
		return "GC Code";
	}

	@Override
	public String getDescription() {
		return "GC Code";
	}
}
