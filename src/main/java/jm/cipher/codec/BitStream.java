package jm.cipher.codec;

import java.util.ArrayList;
import java.util.List;

/**
 * User: T05365A
 * Date: 01.08.12
 * Time: 14:52
 */
public class BitStream {
    private boolean readMode;
    private int bitPos = 0;
    private int bytePos = 0;
    public static int BYTE_LENGTH = 8;
    private int length = 0;

    private List<Byte> bytes = new ArrayList<Byte>();
    private int[] bitmask = {0x01, 0x03, 0x07, 0x0f, 0x1f, 0x3f, 0x7f, 0xff};

    public BitStream(byte[] someBytes) {
        readMode = true;
        if (someBytes != null) {
            for (byte someByte : someBytes) {
                bytes.add(someByte);
            }
        }
    }

    public BitStream() {
        readMode = false;
    }

    /**
     * Returns the read bits in result and returns the count of bits
     *
     * @param n
     * @param result
     * @return
     */
    public int read(int n, int[] result) {
        if (!readMode) {
            throw new RuntimeException("BitStream in write mode!");
        }
        if (isEmpty()) {
            return -1;
        }

        int count = 0;

        if ((bitPos + n) > BYTE_LENGTH) {
            int len1 = BYTE_LENGTH - bitPos;
            count = read(len1, result);
            if (isEmpty()) {
                return count;
            } else {
                int[] r2 = {0};
                int len2 = n - len1;
                count += read(len2, r2);
                result[0] = (result[0] << (len2)) | r2[0];
            }
        } else {
            int b = bytes.get(bytePos);
            int bitmask = getBitmask(n);
            b &= bitmask;
            b = b >> (BYTE_LENGTH - n - bitPos);
            bitPos += n;
            if (bitPos == BYTE_LENGTH) {
                bitPos = 0;
                bytePos++;
            }
            result[0] = b;
            count = n;
        }
        return count;
    }

    // TODO: ist nicht eindeutig!!!
    private int getBitmask(int n) {
        int mask = bitmask[n - 1];
        mask = mask << (BYTE_LENGTH - bitPos - n);
        return mask;
    }


    public void write(byte value, int bitcount) {
        if (readMode) {
            throw new RuntimeException("BitStream in read mode!");
        }
        if (bitcount + bitPos > BYTE_LENGTH) {
            int len1 = BYTE_LENGTH - bitPos;
            write((byte) (value >> (bitcount - len1)), len1);
            bitPos = 0;
            bytePos++;

            int len2 = bitcount - len1;
            write((byte) (value & bitmask[len2 - 1]), len2);

        } else {
            length += bitcount;
            byte b = getCurrentByte();
            int i = (b << bitcount) | value;
            bytes.set(bytePos, (byte) i);
            bitPos += bitcount;
        }
    }

    private byte getCurrentByte() {
        if (bytePos == bytes.size()) {
            bytes.add((byte) 0);
        }
        return bytes.get(bytePos);

    }

    public boolean isEmpty() {
        return bytePos == bytes.size() && bitPos == 0;
    }

    public String toBinaryString() {
        StringBuffer binaryString = new StringBuffer();
        for (int i = 0; i < bytes.size() - 1; i++) {
            binaryString.append(getBinaryString(bytes.get(i)));
        }
        byte lastByte = bytes.get(bytes.size() - 1);
        String lastString = getBinaryString(lastByte);
        for (int i = 0; i < bitPos; i++) {
            binaryString.append(lastString.charAt(BYTE_LENGTH - (bitPos - i) ));
        }

        return binaryString.toString();
    }

    private static String getBinaryString(byte b) {
        return getBinaryString(b, 8);
    }

    private static String getBinaryString(byte b, int length) {
        return String.format("%"+ length + "s", Integer.toBinaryString(b & 0xff)).replace(' ', '0');
    }

    public byte[] getBytes() {
        byte[] bites = new byte[bytes.size()];
        for (int i = 0; i < bytes.size(); i++) {
            bites[i] = bytes.get(i);
        }
        return bites;
    }

    public int getLength() {
        return length;
    }

    public static void main(String[] args) {
        byte[] bytes = {(byte) 0xfe, (byte) 0xAA};
        BitStream bs = new BitStream(bytes);
        int[] result = {0};
        int len = bs.read(5, result);
        System.out.println(getBinaryString((byte) result[0], len));
        System.out.println(len);

        len = bs.read(5, result);
        System.out.println(getBinaryString((byte) result[0], len));
        System.out.println(len);
    }

}
