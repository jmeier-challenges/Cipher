package jm.cipher.codec;

import jm.cipher.AbstractCipher;
import jm.cipher.data.INGramToAlphabetData;
import jm.tools.Text;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * User: T05365A
 * Date: 07.08.12
 * Time: 15:07
 */
public class NGramToAlphabet extends AbstractCipher
    implements INGramToAlphabetData
{
    private INGramToAlphabetData data;
    private String delimiters = " \n";
    private boolean useDelimiters = true;
    private int n;
    private String alphabet = Text.ALPHABET.toLowerCase();

    @Override
    public String encrypt(String pt) throws Exception {
        return "Not implemented";
    }

    @Override
    public String decrypt(String ct) throws Exception {

        if (data.useDelimiters()) {
            return decryptWords(ct);
        } else {
            return decryptNGram(ct);
        }
    }

    private String decryptNGram(String ct) {
        List<String> words = new ArrayList<String>();
        int i;
        int n = data.getN();
        for (i = 0; i < ct.length()/n; i++) {
            String part = ct.substring(i * n, i * n + n);
            words.add(part);
        }
        if (i*n != ct.length()) {
            words.add(ct.substring(i*n));
        }
        return decrypt(words);
    }

    private String decryptWords(String ct) {
        String[] words = ct.split("[" + data.getDelimiters() + "]");
        return decrypt(Arrays.asList(words));
    }

    private String decrypt(List<String> parts) {
        StringBuffer pt = new StringBuffer();
        List<String> words = new ArrayList<String>();
        for (String part : parts) {
            if (!words.contains(part)) {
                words.add(part);
            }

            int pos = words.indexOf(part);
            if (pos >= 0 && pos < alphabet.length()) {
                pt.append(alphabet.charAt(pos));
            } else {
                pt.append(part).append(" ");
            }
        }
        return pt.toString();
    }

    @Override
    public String getDisplayname() {
        return "N-Gram to alphabet";
    }

    @Override
    public String getName() {
        return "NGramToAlphabet";
    }

    @Override
    public String getDescription() {
        return "N-Gram to alphabet";
    }

    @Override
    public boolean canEncrypt() {
        return false;
    }

    public String getDelimiters() {
        return delimiters;
    }

    public void setDelimiters(String delimiters) {
        this.delimiters = delimiters;
    }

    public boolean useDelimiters() {
        return useDelimiters;
    }

    public void setUseDelimiters(boolean useDelimiters) {
        this.useDelimiters = useDelimiters;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    @Override
    public void setIData(Object data) {
        setData((INGramToAlphabetData) data);
    }

    private void setData(INGramToAlphabetData data) {
        this.data = data;
    }
}
