package jm.cipher.codec;

import jm.cipher.AbstractCipher;
import jm.tools.Text;

/**
 * User: T05365A
 * Date: 01.08.12
 * Time: 12:09
 */
public class Base32 extends AbstractCipher {
    private String alphabet = Text.UC_ALPHABET+"234567";

    public String encode(byte[] pt) {
        BitStream bs = new BitStream(pt);
        StringBuffer ct = new StringBuffer();
        int length;
        int[] result = {0};
        while ((length = bs.read(5, result)) != -1) {
            if (length == 5) {
                ct.append(alphabet.charAt(result[0]));
            } else {
                ct.append(alphabet.charAt(result[0] << 5 - length));
            }
        }

        // length must be dividable through eight!
        int mod = ct.length() % 8;
        for (length = 0; mod > 0 && length < 8 - mod; length++) {
            ct.append('=');
        }
        return ct.toString();
    }

    public byte[] decode(String ct) {
        if (!isValid(ct)) {
            throw new RuntimeException(ct + ": A Base32 encoded String must only have characters A-Z, 2-7 and =.");
        }
        BitStream outBs = new BitStream();

        for (byte b : ct.getBytes()) {
            if (b == '=') {
                break;
            } else {
                outBs.write((byte) alphabet.indexOf((char) b), 5);
            }
        }

	    // durch das padding auf 5 bits, kann es sein, dass ein paar überschüssige bits da sind
	    int len = (outBs.getLength() / 8);
	    byte[] bytes = outBs.getBytes();
	    byte[] ret = new byte[len];
	    for (int i = 0; i < len; i++) {
		    ret[i] = bytes[i];
	    }

        return ret;
    }

    private boolean isValid(String ct) {
        return ct.matches("[A-Z2-7=]+");
    }

    public static void main(String[] args) {
        Base32 base32 = new Base32();
        String text = "aaaa";

        String ct = base32.encode(text.getBytes());
        System.out.println(ct);
        ct = "MFQWCYI";
        System.out.println(new String(base32.decode(ct)));
    }

    @Override
    public String encrypt(String pt) throws Exception {
        return encode(pt.getBytes());
    }

    @Override
    public String decrypt(String ct) throws Exception {
        return new String(decode(ct));
    }

    @Override
    public String getDisplayname() {
        return "Base32";
    }

    @Override
    public String getName() {
        return "Base32";
    }

    @Override
    public String getDescription() {
        return "Base32";
    }

}
