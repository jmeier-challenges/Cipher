package jm.cipher.codec;

import jm.cipher.AbstractCipher;

/**
 * User: T05365A
 * Date: 06.08.12
 * Time: 11:35
 */
public class Base85 extends AbstractCipher {
    private long[] pow85 = {1, 85, 85*85, 85*85*85, 85*85*85*85};

    public String encode(byte[] pt) {
        StringBuffer ct = new StringBuffer();

        int i;
        for (i = 0; i < pt.length; i+=4) {
            long pl = getByteAsLong(pt, i) << 24;
            pl |= getByteAsLong(pt, i+1) << 16;
            pl |= getByteAsLong(pt, i+2) << 8;
            pl |= getByteAsLong(pt, i+3);

            if (pl == 0) {
                ct.append('z');
            } else {
                long r;
                int insertPos = ct.length();
                do {
                    r = pl % 85;
                    pl = pl / 85;
                    ct.insert(insertPos, (char) (r + 33));
                } while (pl > 85);
                if (pl > 0) {
                    ct.insert(insertPos, (char) (pl + 33));
                }
            }
        }

        int r = pt.length % 4;
        if (r == 0) {
            return ct.toString();
        } else {
            return ct.substring(0, ct.length() - (4 - r));
        }
    }

    private long getByteAsLong(byte[] pt, int i) {
        if (i < pt.length) {
            return pt[i];
        } else {
            return 0;
        }
    }


    public byte[] decode(String ct) {
        String stripped = getStripped(ct);
        int r = stripped.length() % 5;
        if (r == 0) {
            r = 5;
        }
        byte[] pb = new byte[stripped.length() *4 / 5];
        int pos = 0;
        for (int i = 0; i < stripped.length(); i += 5) {
            if (stripped.charAt(i) == 'z') {
                i++;
                pb[pos++] = 0; pb[pos++] = 0; pb[pos++] = 0; pb[pos++] = 0;
                continue;
            } else {
                long pl = getLong(stripped, i);
                putByte(pb, pos++, pl >> 24);
                putByte(pb, pos++, pl >> 16);
                putByte(pb, pos++, pl >> 8);
                putByte(pb, pos++, pl);
            }
        }

        return pb;
    }

    private void putByte(byte[] pb, int pos, long b) {
        if (pos < pb.length) {
            pb[pos] = (byte) (b & 0xff);
        }
    }

    private String getStripped(String ct) {
        return ct.replaceAll("[ \n]", "");
    }

    private long getLong(String ct, int pos) {
        long l = 0;
        int len = ct.getBytes().length;
        for (int i = 0; i < 5; i++) {
            int b;
            if (pos + i < len) {
                b = ct.getBytes()[i + pos];
            } else {
                b = 'u';
            }
            l = l + pow85[5-i-1]*(b-33);
        }
        return l;
    }

    @Override
    public String encrypt(String pt) throws Exception {
        return encode(pt.getBytes());
    }

    @Override
    public String decrypt(String ct) throws Exception {
        return new String(decode(ct));
    }

    @Override
    public String getDisplayname() {
        return "Base85";
    }

    @Override
    public String getName() {
        return "Base85";
    }

    @Override
    public String getDescription() {
        return "Base85";
    }

    public static void main(String[] args) {
        Base85 ascii85 = new Base85();
        String text = "Man is distinguished, not only by his reason, but by this singular passion from other animals, which is a lust of the mind, that by a perseverance of delight in the continued and indefatigable generation of knowledge, exceeds the short vehemence of any carnal pleasure.";
        String ct = ascii85.encode(text.getBytes());
        System.out.println(ct);
        System.out.println(new String(ascii85.decode(ct)));
        //System.out.println(ascii85.convertTo(100, 16));
    }
}
