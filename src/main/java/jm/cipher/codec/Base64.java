package jm.cipher.codec;

import jm.cipher.AbstractCipher;

/**
 * User: T05365A
 * Date: 03.08.12
 * Time: 09:19
 */
public class Base64 extends AbstractCipher {
    private java.util.Base64.Encoder encoder = java.util.Base64.getEncoder();
    private java.util.Base64.Decoder decoder = java.util.Base64.getDecoder();

    @Override
    public String encrypt(String pt) throws Exception {
        return encoder.encodeToString(pt.getBytes());
    }

    @Override
    public String decrypt(String ct) throws Exception {
        return new String(decoder.decode(ct));
    }

    @Override
    public String getDisplayname() {
        return "Base64";
    }

    @Override
    public String getName() {
        return "Base64";
    }

    @Override
    public String getDescription() {
        return "Base64";
    }
}
