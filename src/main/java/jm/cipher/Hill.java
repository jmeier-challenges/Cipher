package jm.cipher;

import jm.cipher.data.IHillData;
import jm.math.ModuloArithmetik;
import jm.tools.Text;

/**
 * User: T05365A
 * Date: 06.07.12
 * Time: 09:12
 *
 * Nur für eine 2x2 Matrix!!!
 */
public class Hill extends AbstractCipher implements IHillData {
    private String originalAlphabet = "fxcukpieszaqnhdbtgmyvrowlj"; // 5 23 2 20 10 15 8 4 18 25 0 16 13 7 3 1 19 6 12 24 21 17 14 22 11 9
    private String ptAlphabet = Text.LC_ALPHABET;
    private String key;
    private long[][] keyMatrix;

    boolean blockwise = true;
    private ModuloArithmetik modul = new ModuloArithmetik(ptAlphabet.length());

    private IHillData data;

    public Hill() {
        setIData(this);
    }

    public boolean isBlockwise() {
        return blockwise;
    }

    public void setBlockwise(boolean blockwise) {
        this.blockwise = blockwise;
    }

    public static void main(String[] args) throws Exception {
        Hill hill = new Hill();

        int[][] keyMatrix = new int[][]{
                {7, 11},
                {8, 11}
        };

	    String key = "gybnqkurp";
	    String pt = "dascachenindiesergegendistwirklichtollweilderanteildermysteriessehrhochistunddiesehervorragendgestaltetsindessindauchrechtkniffligeraetseldabeiderenloesungmirzurzeitnochverborgenistichmoechteaucheinenrelativeinfachenmysterybeisteuernderinmittelhessenlockerviereinhalbsternehaetteaberindererlangereckebestenfallsaufdreisternekommtdadielandschafthierdochrechtvielehuegelaufweisthabeichdencachekurzerhandauchsobenanntdercacheliegtbeinordneunundvierziggraddreiunddreissigpunktsechshundertzweiunddreissigundostelfgradnullsechspunktachthunderteinundachtzigerliegtinderausbuchtungeinesfelsensvielglueckbeidersuche";
	    hill.setKey(key);
	    System.out.println(hill.getKey(hill.getKeyMatrix()));
	    System.out.println(hill.encrypt(pt));

    }

	public String getKey(long[][] keyMatrix) {
		StringBuilder key = new StringBuilder();
		for (int i = 0; i < keyMatrix.length; i++) {
			for (int k = 0; k < keyMatrix[i].length; k++) {
				key.append(ptAlphabet.charAt((int) keyMatrix[k][i]));
			}
		}
		return key.toString();
	}

    public String crypt(String text, long[][] keyMatrix) throws Exception {
        long[][] textMatrix = getMatrix(getPadded(text, keyMatrix.length));

        long[][] matrix = modul.multiply(keyMatrix, textMatrix);

        StringBuilder builder = new StringBuilder(text.length());
        if (isBlockwise()) {
            for (int j = 0; j < matrix[0].length; j++) {
                for (int i = 0; i < matrix.length; i++) {
                    builder.append(data.getPtAlphabet().charAt((int) matrix[i][j]));
                }
            }
        } else {
            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix[i].length; j++) {
                    builder.append(data.getPtAlphabet().charAt((int) matrix[i][j]));
                }
            }
        }

        return builder.toString();
    }

    @Override
    public String encrypt(String pt) throws Exception {
        long [][] keyMatrix = getKeyMatrix();
        return crypt(pt, keyMatrix);
    }

    private long[][] getMatrix(String s) {
        int square = getKeyMatrix().length;
        String padded = getPadded(s, square);
        int length = padded.length() / square;
        long[][] matrix = new long[square][length];

        if (isBlockwise()) {
            int pos = 0;
            for (int j = 0; j < length; j++) {
                for (int i = 0; i < square; i++) {
                    matrix[i][j] = data.getPtAlphabet().indexOf(padded.charAt(pos++));
                }
            }

        } else {
            for (int i = 0; i < square; i++) {
                for (int j = 0; j < length; j++) {
                    matrix[i][j] = data.getPtAlphabet().indexOf(padded.charAt(i * length + j));
                }
            }
        }

        return matrix;
    }

    private String getPadded(String pt, int blockLength) {
        int remainder = pt.length() % blockLength;

        String pad = "";
        if (remainder > 0) {
            for (int i = 0; i < blockLength - remainder; i++) {
                pad += " ";
            }
        }

        return pt + pad;
    }

    @Override
    public String decrypt(String ct) throws Exception {
        long[][] decryptMatrix = modul.getInverseMatrix(getKeyMatrix());
        return crypt(ct, decryptMatrix);
    }

    @Override
    public String getDisplayname() {
        return "Hill";
    }

    @Override
    public String getName() {
        return "Hill";
    }

    @Override
    public String getDescription() {
        return "Hill";
    }

    public String getPtAlphabet() {
        return ptAlphabet;
    }

    public void setPtAlphabet(String ptAlphabet) {
        this.ptAlphabet = ptAlphabet;
        modul = new ModuloArithmetik(ptAlphabet.length());
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public long[][] getKeyMatrix() {
        if (keyMatrix == null) {
            int square = (int) Math.sqrt(data.getKey().length());
            if (square * square - data.getKey().length() != 0) {
                throw new RuntimeException("Keylength must be square");
            }

            keyMatrix = new long[square][square];

            for (int j = 0; j < square; j++) {
                for (int k = 0; k < square; k++) {
                    char c = data.getKey().charAt(j * square + k);
                    int i = data.getPtAlphabet().indexOf(c);
                    keyMatrix[k][j] = i;
                }
            }
        }
        return keyMatrix;
    }

    public void setKeyMatrix(long[][] matrix) {
        keyMatrix = matrix;
    }

    @Override
    public void setIData(Object data) {
        this.data = (IHillData) data;
    }
}
