package jm.cipher;

public class UpperLowerBaconChar extends BaconCharImpl {
	public boolean isBaconBImpl(final char c) {
		return Character.isLetter(c) && Character.isUpperCase(c);
	}

	public boolean isBaconAImpl(final char c) {
		return Character.isLetter(c) && Character.isLowerCase(c);
	}
}