package jm.cipher;

import jm.cipher.data.IPlayfairData;
import jm.tools.Text;

import java.io.IOException;

/*
http://www.quinapalus.com/playfair.html
http://www.dreamincode.net/forums/topic/190728-secret-code-iii-playfair-code/
http://practicalcryptography.com/ciphers/playfair-cipher/
*/
public class Playfair extends AbstractCipher {

	private final int SIZE = 5;
	private String passwd;
	private String ALPHABET = "ABCDEFGHIKLMNOPQRSTUVWXYZ";
	private char[][] cipherGrid = new char[SIZE][SIZE];
	private String bigrammtext = "";
    private IPlayfairData data;


	public void setPasswd(String pw) {

		passwd = pw.toUpperCase();

		for (int i = 0; i < passwd.length(); i++) {
			char c = passwd.charAt(i);
			if (gridContains(c)) {
				continue;
			}
			appendGrid(c);
		}

		for (int i = 0; i < ALPHABET.length(); i++) {
			char c = ALPHABET.charAt(i);
			if (gridContains(c)) {
				continue;
			}
			appendGrid(c);
		}
	}

	private void appendGrid(char c) {
		for (int y = 0; y < SIZE; y++) {
			for (int x = 0; x < SIZE; x++) {
				if (getCipherChar(x, y) == '*') {
					setCipherChar(x, y, c);
					return;
				}
			}
		}
	}

	private void createBigrammtext(String plaintext) {
		String bigramm = "";
		bigrammtext = "";
		String text = plaintext.toUpperCase();
		for (int i = 0; i < text.length(); i++) {
			char c = text.charAt(i);
			if (isValidChar(c)) {
				if (c == 'J') {
					c = 'I';
				}
				bigramm += c;
			}

			if (bigramm.length() == 2) {
				if (bigramm.charAt(0) == bigramm.charAt(1)) {
					bigramm = bigramm.substring(1);
					bigrammtext += (bigramm + "X");
				} else {
					bigrammtext += bigramm;
					bigramm = "";
				}
			}
		}
		bigrammtext += bigramm;

		if (bigramm.length() == 1) {
			bigrammtext += "X";
		}
	}

	private boolean isValidChar(char c) {
		return Character.isLetter(c);
	}

	public String encrypt(String pt) {
        initializeData();
		createBigrammtext(pt);
		StringBuffer ct = new StringBuffer();
		char[] bigramm = new char[2];
		for (int i = 0; i < bigrammtext.length(); i++) {
			char c = bigrammtext.charAt(i);
			bigramm[i % 2] = c;
			if (i % 2 == 1) {
				String cPair = encryptPair(bigramm);
				ct.append(cPair);
			}
		}
		return ct.toString();
	}

	private String encryptPair(char[] pPair) {
		String cPair;
		int x0 = getX(pPair[0]);
		int x1 = getX(pPair[1]);
		int y0 = getY(pPair[0]);
		int y1 = getY(pPair[1]);
		if (y0 == y1) {
			cPair = getRightChars(x0, x1, y0);
		} else if (x0 == x1) {
			cPair = getLowerChars(y0, y1, x0);
		} else {
			cPair = getDiagonalChars(x0, y0, x1, y1);
		}
		return cPair.toString();
	}

	private String decryptPair(char[] cPair) {
		String pPair;
		int x0 = getX(cPair[0]);
		int x1 = getX(cPair[1]);
		int y0 = getY(cPair[0]);
		int y1 = getY(cPair[1]);
		if (y0 == y1) {
			pPair = getLeftChars(x0, x1, y0);
		} else if (x0 == x1) {
			pPair = getUpperChars(y0, y1, x0);
		} else {
			pPair = getDiagonalChars(x0, y0, x1, y1);
		}
		return pPair.toString();
	}

	private String getDiagonalChars(final int x0, final int y0, final int x1, final int y1) {
		String pair = "";
		pair += getCipherChar(x1, y0);
		pair += getCipherChar(x0, y1);
		return pair;
	}

	private String getLowerChars(final int y0, final int y1, final int x) {
		String pair = "";
		pair += getCipherChar(x, (y0 + 1) % SIZE);
		pair += getCipherChar(x, (y1 + 1) % SIZE);
		return pair;
	}

	private String getUpperChars(final int y0, final int y1, final int x) {
		String pair = "";
		pair += getCipherChar(x, (y0 + 4) % SIZE);
		pair += getCipherChar(x, (y1 + 4) % SIZE);
		return pair;
	}

	private String getRightChars(final int x0, final int x1, final int y) {
		String pair = "";
		pair += getCipherChar((x0 + 1) % SIZE, y);
		pair += getCipherChar((x1 + 1) % SIZE, y);
		return pair;
	}

	private String getLeftChars(final int x0, final int x1, final int y) {
		String pair = "";
		pair += getCipherChar((x0 + 4) % SIZE, y);
		pair += getCipherChar((x1 + 4) % SIZE, y);
		return pair;
	}

	private int getX(final char c) {
		for (int y = 0; y < SIZE; y++) {
			for (int x = 0; x < SIZE; x++) {
				if (getCipherChar(x, y) == c) {
					return x;
				}
			}
		}
		return -1;
	}

	private int getY(final char c) {
		for (int y = 0; y < SIZE; y++) {
			for (int x = 0; x < SIZE; x++) {
				if (getCipherChar(x, y) == c) {
					return y;
				}
			}
		}
		return -1;
	}

	public String decrypt(String ct) {
        initializeData();
		StringBuffer pt = new StringBuffer();
		char[] bigramm = new char[2];
		for (int i = 0; i < ct.length(); i++) {
			char c = ct.charAt(i);
			bigramm[i % 2] = c;
			if (i % 2 == 1) {
				String pPair = decryptPair(bigramm);
				pt.append(pPair);
			}
		}
		return pt.toString();
	}

    public void initializeData() {
        if (data != null) {
            setPasswd(data.getKey());
        }
    }

    public String getDisplayname() {
        return "Playfair";
    }

    public String getName() {
        return "Playfair";
    }

    public String getDescription() {
        return "Playfair (Muss noch mal überprüft werden)";
    }

    public void setIData(Object data) {
        setData((IPlayfairData) data);
    }

    private void setData(IPlayfairData data) {
        this.data = data;
    }

    public String toString() {
        return super.toString();
        /*
		String s = "";
		for (int y = 0; y < SIZE; y++) {
			for (int x = 0; x < SIZE; x++) {
				s += getCipherChar(x, y);
			}
			s += "\n";
		}
		s += "\n";

		for (int i = 0; i < bigrammtext.length(); i++) {
			if (i > 0 && i % 2 == 0) {
				s += " ";
			}
			s += bigrammtext.charAt(i);
		}
		return s;
		*/
	}

	public void setCipherChar(final int x, final int y, final char c) {
		if (x < SIZE && x >= 0 && y < SIZE && y >= 0) {
			cipherGrid[y][x] = c;
		}
	}

	public char getCipherChar(final int x, final int y) {
		if (x == -1 || y == -1) {
			return '*';
		} else {
			char c = cipherGrid[y][x];
			if (c == 0) {
				return '*';
			} else {
				return c;
			}
		}
	}

	public void fillNotUsedChars(String chars) {
		int pos = 0;
		for (int y = 0; y < SIZE; y++) {
			for (int x = 0; x < SIZE; x++) {
				char c = getCipherChar(x, y);
				if (c == 0 || c == '*') {
					setCipherChar(x, y, chars.charAt(pos++));
				}
			}
		}
	}

	public String getNotUsedChars() {
		StringBuffer notUsedChars = new StringBuffer();
		for (int i = 0; i < ALPHABET.length(); i++) {
			char c = ALPHABET.charAt(i);
			if (!gridContains(c)) {
				notUsedChars.append(c);
			}
		}
		return notUsedChars.toString();
	}

	public String getUsedChars() {
		StringBuffer alphabet = new StringBuffer();
		for (int y = 0; y < SIZE; y++) {
			for (int x = 0; x < SIZE; x++) {
				alphabet.append(getCipherChar(x, y));
			}
		}
		return alphabet.toString();
	}

	private boolean gridContains(char chr) {
		for (char[] line : cipherGrid) {
			for (char c : line) {
				if (c == chr) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) throws IOException {
		//play();
		happySecurity();
		//playBlackSheep();
	}

	private static void happySecurity() throws IOException {
		final Playfair pf = new Playfair();
		final String ct = "IZOCCAYZQXNRTBADBBWOCZFSYQLBABPBQBHBCGQXEFLPPLEFMZPLVQPMIYLBOPEFELZKLZEILTLEBQCIALUEBQ";
		//final String key = "BDFHENACTQGISOMRKLPUVWXYZ";
		//pf.setPasswd(key);
		//final String ct = pf.encrypt("Dies ist ein wirklich geheimer Text");
		//System.out.println(Text.getKoinzidenzIndex("Dies ist ein wirklich geheimer Text"));

		/*
		pf.initializeHappyGrid();
		pf.printCipherGrid();
		System.out.println(ct);
		System.out.println(pf.decrypt(ct));
		String notUsedChars = pf.getNotUsedChars();
		System.out.println(notUsedChars);
		pf.fillNotUsedChars(notUsedChars);
		pf.printCipherGrid();
		System.out.println(pf.getUsedChars());
		pf.setPasswd(pf.getUsedChars());
		System.out.println(pf);
		*/
		final Text text = new Text();
		pf.initializeHappyGrid();
		String notUsedChars = pf.getNotUsedChars();
		System.out.println(pf.decrypt(ct));

		System.out.println(notUsedChars);
		/*
		final long max = Combinations.fac(BigInteger.valueOf(notUsedChars.length())).longValue(); // 14!
		final long onePercent = max/10L;
		System.out.println(max);
		System.out.println(onePercent);
		*/

		/*
		final Permutations perms = new Permutations();
		perms.permutate(notUsedChars, new PermutationVisitor<String>() {
			int score = 0;
			int count = 0;

			public void visitPermutation(final String permutation) {
				pf.initializeHappyGrid();
				pf.fillNotUsedChars(permutation);
				String pt = pf.decrypt(ct);
				int score = text.scoreDE(pt);
				//System.out.println(score + ": " + permutation + ": " + pt);
				if (score > this.score) {
					System.out.println(score + ": " + pt);
					this.score = score;
				}
				if (count++ % onePercent == 0) {
					System.out.println(count-1);
				}
			}

			public void visitPermutation(final List<String> prefix) {
				// TODO: not yet implemented
			}

		});
		*/
	}

	private void initializeHappyGrid() {
		cipherGrid = new char[][] {
				{'B', 'I', 'L', 'T', 'E'},
				{'R', 'A', 'C', 'W', 'Q'},
				{'G', 'Z', 'S', 'O', 'P'},
				{'M', 'N', 'F', 'K', 'U'},
				{'V', 'D', 'X', 'Y', 'H'}
		};
	}


	private void initializeGrid() {
		cipherGrid = new char[][] {
				{'D', 'E', 'A',	0, 'H'},
				{ 0,  'C', 'F',  0, 'I'},
				{'K',  0,  'M', 'N', 0 },
				{ 0,	0,	0,	0, 'U'},
				{'V',  0,  'X',  0,  0 }
		};
	}

	private void printCipherGrid() {
		for (int y = 0; y < SIZE; y++) {
			for (int x = 0; x < SIZE; x++) {
				System.out.print(getCipherChar(x, y) + ", ");
			}
			System.out.println();
		}
	}

	private static void play() {
		Playfair pf = new Playfair();
		//pf.setPasswd("death");
		//System.out.println(pf);

		//String ct = pf.encrypt("Laboulaye lady will lead to Cibola temples of gold");
		//String ct = pf.encrypt("Die Loesung, die du suchst, ist der Vorname des Erfindes dieses Codes");

		String ct = "HBCQLHUPSNHBAEPUQITUHGYGEAPXMUMTLAEAQTXMGOEAPTCHQTQGKHTQ";
		pf.initializeGrid();
		System.out.println(pf);
		System.out.println(ct);
		System.out.println(pf.decrypt(ct));
		//System.out.println(pf.decryptPair(new char[]{'F', 'B'}));
	}

	private static void playBlackSheep() {
		final String ct="ZBVFIYGHCZOXHLYVKECWSFSBYHTLIYUHCWDFQVDNZHETKXMOFQLCKELPYB";
		final Playfair pf = new Playfair();
		pf.initializeBlackSheepGrid();
		System.out.println(pf.decrypt(ct));

		final Text text = new Text();
		String notUsedChars = pf.getNotUsedChars();
		System.out.println(pf.decrypt(ct));
		System.out.println(notUsedChars);

		/*
		System.out.println(notUsedChars);
		final long max = Combinations.fac(BigInteger.valueOf(notUsedChars.length())).longValue(); // 14!
		final long onePercent = max/100L;
		System.out.println(max);
		System.out.println(onePercent);

		final Permutations perms = new Permutations();
		perms.permutate(notUsedChars, new PermutationVisitor<String>() {
			int score = 0;
			int count = 0;

			public void visitPermutation(final String permutation) {
				pf.initializeBlackSheepGrid();
				pf.fillNotUsedChars(permutation);
				String pt = pf.decrypt(ct);
				int score = text.scoreEN(pt);
				//System.out.println(score + ": " + permutation + ": " + pt);
				if (score > this.score) {
					System.out.println(score + ": " + pt);
					this.score = score;
				}
				if (count++ % onePercent == 0) {
					System.out.println(count-1);
				}
			}

			public void visitPermutation(final List<String> prefix) {
				// TODO: not yet implemented
			}

		});
		*/
	}

	private void initializeBlackSheepGrid() {
		cipherGrid = new char[][] {
				{'I', 'Y', 'X', 'C', 'T'},
				{'F', 'L', 'Q', 'A', 'P'},
				{'G', 'Z', 'U', 'O', 'M'},
				{'B', 'R', 'N', 'H', 'W'},
				{'E', 'K', 'S', 'V', 'D'}
		};
	}

}