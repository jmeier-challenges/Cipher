package jm.cipher;

import jm.cipher.data.IMorseData;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Morse extends AbstractCipher {
    private IMorseData data;

    public static String SPACE = " ";
    public static String DIT = ".";
    public static String DAH = "-";

	public static String C_A = DIT+DAH;
	public static String C_B = DAH+DIT+DIT+DIT;
	public static String C_C = DAH+DIT+DAH+DIT;
	public static String C_D = DAH+DIT+DIT;
	public static String C_E = DIT;
	public static String C_F = DIT+DIT+DAH+DIT;
	public static String C_G = DAH+DAH+DIT;
	public static String C_H = DIT+DIT+DIT+DIT;
	public static String C_I = DIT+DIT;
	public static String C_J = DIT+DAH+DAH+DAH;
	public static String C_K = DAH+DIT+DAH;
	public static String C_L = DIT+DAH+DIT+DIT;
	public static String C_M = DAH+DAH;
	public static String C_N = DAH+DIT;
	public static String C_O = DAH+DAH+DAH;
	public static String C_P = DIT+DAH+DAH+DIT;
	public static String C_Q = DAH+DAH+DIT+DAH;
	public static String C_R = DIT+DAH+DIT;
	public static String C_S = DIT+DIT+DIT;
	public static String C_T = DAH;
	public static String C_U = DIT+DIT+DAH;
	public static String C_V = DIT+DIT+DIT+DAH;
	public static String C_W = DIT+DAH+DAH;
	public static String C_X = DAH+DIT+DIT+DAH;
	public static String C_Y = DAH+DIT+DAH+DAH;
	public static String C_Z = DAH+DAH+DIT+DIT;
	public static String C_0 = DAH+DAH+DAH+DAH+DAH;
	public static String C_1 = DIT+DAH+DAH+DAH+DAH;
	public static String C_2 = DIT+DIT+DAH+DAH+DAH;
	public static String C_3 = DIT+DIT+DIT+DAH+DAH;
	public static String C_4 = DIT+DIT+DIT+DIT+DAH;
	public static String C_5 = DIT+DIT+DIT+DIT+DIT;
	public static String C_6 = DAH+DIT+DIT+DIT+DIT;
	public static String C_7 = DAH+DAH+DIT+DIT+DIT;
	public static String C_8 = DAH+DAH+DAH+DIT+DIT;
	public static String C_9 = DAH+DAH+DAH+DAH+DIT;

	private Map<String, String> char2morse = new HashMap<String,String>();
	private Map<String, String> morse2char = new HashMap<String,String>();

	private void initTranslationMaps() {
		char2morse.put("A", C_A);
		char2morse.put("B", C_B);
		char2morse.put("C", C_C);
		char2morse.put("D", C_D);
		char2morse.put("E", C_E);
		char2morse.put("F", C_F);
		char2morse.put("G", C_G);
		char2morse.put("H", C_H);
		char2morse.put("I", C_I);
		char2morse.put("J", C_J);
		char2morse.put("K", C_K);
		char2morse.put("L", C_L);
		char2morse.put("M", C_M);
		char2morse.put("N", C_N);
		char2morse.put("O", C_O);
		char2morse.put("P", C_P);
		char2morse.put("Q", C_Q);
		char2morse.put("R", C_R);
		char2morse.put("S", C_S);
		char2morse.put("T", C_T);
		char2morse.put("U", C_U);
		char2morse.put("V", C_V);
		char2morse.put("W", C_W);
		char2morse.put("X", C_X);
		char2morse.put("Y", C_Y);
		char2morse.put("Z", C_Z);
		char2morse.put("0", C_0);
		char2morse.put("1", C_1);
		char2morse.put("2", C_2);
		char2morse.put("3", C_3);
		char2morse.put("4", C_4);
		char2morse.put("5", C_5);
		char2morse.put("6", C_6);
		char2morse.put("7", C_7);
		char2morse.put("8", C_8);
		char2morse.put("9", C_9);
		char2morse.put(" ", SPACE);

		morse2char.put(C_A, "A");
		morse2char.put(C_B, "B");
		morse2char.put(C_C, "C");
		morse2char.put(C_D, "D");
		morse2char.put(C_E, "E");
		morse2char.put(C_F, "F");
		morse2char.put(C_G, "G");
		morse2char.put(C_H, "H");
		morse2char.put(C_I, "I");
		morse2char.put(C_J, "J");
		morse2char.put(C_K, "K");
		morse2char.put(C_L, "L");
		morse2char.put(C_M, "M");
		morse2char.put(C_N, "N");
		morse2char.put(C_O, "O");
		morse2char.put(C_P, "P");
		morse2char.put(C_Q, "Q");
		morse2char.put(C_R, "R");
		morse2char.put(C_S, "S");
		morse2char.put(C_T, "T");
		morse2char.put(C_U, "U");
		morse2char.put(C_V, "V");
		morse2char.put(C_W, "W");
		morse2char.put(C_X, "X");
		morse2char.put(C_Y, "Y");
		morse2char.put(C_Z, "Z");
		morse2char.put(C_0, "0");
		morse2char.put(C_1, "1");
		morse2char.put(C_2, "2");
		morse2char.put(C_3, "3");
		morse2char.put(C_4, "4");
		morse2char.put(C_5, "5");
		morse2char.put(C_6, "6");
		morse2char.put(C_7, "7");
		morse2char.put(C_8, "8");
		morse2char.put(C_9, "9");
		morse2char.put(" ", SPACE);
	}

	private static FileWriter writer = null;
	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
        Morse morse = new Morse(new IMorseData() {
            public String getDit() {
                return ".";
            }

            public String getDah() {
                return "-";
            }

            public String getSpace() {
                return " ";
            }
        });
		System.out.println(morse.encrypt("SOS"));
        System.out.println(morse.decrypt(".-- .-- --. --. -.-. ..--- .-.. - .-- -.."));
	}

	public String encrypt(final String message) {
        initializeData();
		StringBuffer code = new StringBuffer();
		for (int i = 0; i < message.length(); i++) {
			String c = message.substring(i, i+1).toUpperCase();
            if (char2morse.containsKey(c)) {
                code.append(char2morse.get(c));
                code.append(SPACE);
            } else if (!SPACE.equals(c)) {
                code.append(c);
            }
		}
		return code.toString();
	}

	public String decrypt(final String code) {
        initializeData();
		StringBuffer message = new StringBuffer();
		String cMorse = "";
		for (int i = 0; i < code.length(); i++) {
			char c = code.charAt(i);
			if (c == DIT.charAt(0) || c == DAH.charAt(0)) {
				cMorse = cMorse + c;
			} else {
				if (cMorse.length() != 0) {
					message.append(morse2char.get(cMorse));
				} else {
					message.append(c);
				}
				cMorse = "";
			}
		}
        if (cMorse.length() != 0) {
            message.append(morse2char.get(cMorse));
        }
		return message.toString();
	}

    public void initializeData() {
        if (data != null) {
            setDitDitDah(data.getDit() + data.getDah() + data.getSpace());
        }
    }

    public String getDisplayname() {
        return "Morsecode";
    }

    public String getName() {
        return "Morse";
    }

    public String getDescription() {
        return "Morsecoder verschlüsseln und entschlüsseln";
    }

    public void decodeBruteforce(final String code, final String message) throws IOException {
		/*
		if (!isValidKombination(message)) {
			return;
		}
		*/
		if (code.length() == 0) {
			System.out.println(message);
			//writer.write(message + '\n');
			return;
		}
		String cMorse = "";
		for (int i = 0; i < code.length(); i++) {
			char c = code.charAt(i);
			if (c == DIT.charAt(0) || c == DAH.charAt(0)) {
				cMorse = cMorse + c;
			}
			if (morse2char.containsKey(cMorse)) {
				decodeBruteforce(code.substring(i+1), message + morse2char.get(cMorse));
			} else {
				break;
			}
		}
	}

	private boolean isValidKombination(String message) {
		int length = message.length();
		if (length < 3) {
			return true;
		}
		char lastChar = message.charAt(length-2);
		if (message.charAt(length-1) == lastChar && message.charAt(length-2) == lastChar) {
			return false;
		}
		return true;
	}

    public void setDitDitDah(String ditDitDah) {
        if (ditDitDah.length() != 3) {
            throw new RuntimeException("DitDitDah must have the size of 3");
        }
        DIT = ditDitDah.substring(0, 1);
        DAH = ditDitDah.substring(1, 2);
        SPACE = ditDitDah.substring(2, 3);

        C_A = DIT + DAH;
        C_B = DAH + DIT + DIT + DIT;
        C_C = DAH + DIT + DAH + DIT;
        C_D = DAH + DIT + DIT;
        C_E = DIT;
        C_F = DIT + DIT + DAH + DIT;
        C_G = DAH + DAH + DIT;
        C_H = DIT + DIT + DIT + DIT;
        C_I = DIT + DIT;
        C_J = DIT + DAH + DAH + DAH;
        C_K = DAH + DIT + DAH;
        C_L = DIT + DAH + DIT + DIT;
        C_M = DAH + DAH;
        C_N = DAH + DIT;
        C_O = DAH + DAH + DAH;
        C_P = DIT + DAH + DAH + DIT;
        C_Q = DAH + DAH + DIT + DAH;
        C_R = DIT + DAH + DIT;
        C_S = DIT + DIT + DIT;
        C_T = DAH;
        C_U = DIT + DIT + DAH;
        C_V = DIT + DIT + DIT + DAH;
        C_W = DIT + DAH + DAH;
        C_X = DAH + DIT + DIT + DAH;
        C_Y = DAH + DIT + DAH + DAH;
        C_Z = DAH + DAH + DIT + DIT;
        C_0 = DAH + DAH + DAH + DAH + DAH;
        C_1 = DIT + DAH + DAH + DAH + DAH;
        C_2 = DIT + DIT + DAH + DAH + DAH;
        C_3 = DIT + DIT + DIT + DAH + DAH;
        C_4 = DIT + DIT + DIT + DIT + DAH;
        C_5 = DIT + DIT + DIT + DIT + DIT;
        C_6 = DAH + DIT + DIT + DIT + DIT;
        C_7 = DAH + DAH + DIT + DIT + DIT;
        C_8 = DAH + DAH + DAH + DIT + DIT;
        C_9 = DAH + DAH + DAH + DAH + DIT;

        initTranslationMaps();
    }

    public Morse() {
        setDitDitDah(".- ");
    }

    public Morse(IMorseData morseData) {
        setData(morseData);
    }

    public void setData(IMorseData morseData) {
        data = morseData;
    }

    public void setIData(Object morseData) {
        setData((IMorseData) morseData);
    }


    /**
     *  dit and dah must not be the same
     * @param ct cipher
     * @param dit 00, 01, 10 or 11
     * @param dah 00, 01, 10 or 11
     */
    public static String twoBitsToMorse(List<Integer> ct, String dit, String dah) {
        StringBuilder result = new StringBuilder();
        for (Integer ci : ct) {
            String binaryString = getBinaryString(ci);
            for (int i = 0; i < binaryString.length(); i+=2) {
                String code = binaryString.substring(i, i+2);
                if (dit.equals(code)) {
                    result.append(".");
                } else if (dah.equals(code)) {
                    result.append("-");
                } else {
                    result.append(" ");
                }
            }
        }
        return result.toString();
    }

    /**
    00 zwischen Buchstaben
    0 zwischen Dit und Dah
    11 Dah (-)
    1 Dit (.)
    */
    public static String onesToMorse(List<Integer> ct) {
        return onesToMorse(ct, false);

    }
    public static String onesToMorse(List<Integer> ct, boolean skipLeadingZeros) {

        StringBuilder result = new StringBuilder();

        // skip leading zeros
        StringBuilder binaryString;
        int i;
        if (skipLeadingZeros) {
            i = 1;
            binaryString = new StringBuilder(Integer.toBinaryString(ct.get(0)));
        } else {
            i = 0;
            binaryString = new StringBuilder();
        }

        for (; i < ct.size(); i++) {
            binaryString.append(getBinaryString(ct.get(i)));
        }

        char last = ' ';
        for (char c : binaryString.toString().toCharArray()) {

            if (isDah(c, last)) {
                result.append(DAH);
                last = ' ';
            } else if (isDit(c, last)) {
                result.append(DIT);
                last = c;
            } else if (isSpace(c, last)) {
                result.append(' ');
                last = ' ';
            } else {
                last = c;
            }
        }

        if ('1' == last) {
            result.append(DIT);
        }
        return result.toString();
    }

    public static boolean isDah(char c, char last) {
        return '1' == c && '1' == last;
    }

    public static boolean isDit(char c, char last) {
        return '0' == c && '1' == last;
    }

    public static boolean isSpace(char c, char last) {
        return '0' == c && '0' == last;
    }


    public static String getBinaryString(Integer ci) {
        StringBuilder binaryString = new StringBuilder(Integer.toBinaryString(ci));

        if (ci < 256) {
            for (int i = binaryString.length(); i < 8; i++) {
                binaryString.insert(0, "0");
            }
        } else if (ci < 4096) {
            for (int i = binaryString.length(); i < 16; i++) {
                binaryString.insert(0, "0");
            }
        }

        return binaryString.toString();
    }

}
