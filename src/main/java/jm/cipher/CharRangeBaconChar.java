package jm.cipher;

public class CharRangeBaconChar extends BaconCharImpl {
	private String aChars;
	private String bChars;

	public CharRangeBaconChar() {
		setAChars("abcdefghijkl");
		setBChars("mnopqrstuvwxyz");
	}

	public boolean isBaconAImpl(final char c) {
		if (Character.isLetter(c)) {
			char ec = Character.toLowerCase(c);
			return aChars.contains(String.valueOf(ec));
		} else {
			return false;
		}
	}

	public boolean isBaconBImpl(final char c) {
		if (Character.isLetter(c)) {
			char ec = Character.toLowerCase(c);
			return bChars.contains(String.valueOf(ec));
		} else {
			return false;
		}
	}

	public void setAChars(final String aChars) {
		this.aChars = aChars;
	}

	public String getAChars() {
		return aChars;
	}

	public void setBChars(final String bChars) {
		this.bChars = bChars;
	}

	public String getBChars() {
		return bChars;
	}
}
