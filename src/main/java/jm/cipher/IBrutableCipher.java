package jm.cipher;

import jm.tools.Language;
import jm.tools.Text;
import jm.util.BestN;

/**
 * User: T05365A
 * Date: 24.08.12
 * Time: 16:15
 */
public interface IBrutableCipher {
	BestN<Object> brute(String ct, Language language) throws Exception;
	String getName();
    void setCtAlphabet(String alphabet);
    void setKnownPlaintext(String knownPt);
	void setLanguage(Language language);
}