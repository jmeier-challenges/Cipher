package jm.cipher;

import jm.cipher.data.IAsciiNumberData;
import jm.math.Number;

/**
 * User: T05365A
 * Date: 30.07.12
 * Time: 14:08
 */
public class AsciiNumber extends AbstractCipher {
    private String toDigits = "0123456789";
    private String fromDigits = "0123456789";
    private IAsciiNumberData data;

    public String decrypt(final String ct) {
        StringBuffer pt = new StringBuffer();
        String[] numbers = ct.split(" ");
        for (String number : numbers) {
            String n = jm.math.Number.convert(number, fromDigits, toDigits);
            try {
                int c = Integer.parseInt(n);
                pt.append((char) c);
            } catch (NumberFormatException ex) {
                pt.append('?');
                System.err.println(ex);
            }
        }
        return pt.toString();
    }

    public void initializeData() {
        if (data != null) {
            fromDigits = data.getFromDigits();
        }
    }

    public String getDisplayname() {
        return "Ascii to Number";
    }

    public String getName() {
        return "AsciiNumber";
    }

    public String getDescription() {
        return "Ascii to Number";
    }

    public void setIData(Object data) {
        setData((IAsciiNumberData) data);
    }

    private void setData(IAsciiNumberData data) {
        this.data = data;
    }

    public String encrypt(final String pt) {
        initializeData();
        StringBuffer ct = new StringBuffer();
        for (int i = 0; i < pt.length(); i++) {
            char c = pt.charAt(i);
            String n = Integer.toString(c);
            String number = Number.convert(n, toDigits, fromDigits);
            ct.append(number).append(" ");
        }
        return ct.toString();
    }

    public String getFromDigits() {
        return fromDigits;
    }

    public void setFromDigits(String fromDigits) {
        this.fromDigits = fromDigits;
    }
}
