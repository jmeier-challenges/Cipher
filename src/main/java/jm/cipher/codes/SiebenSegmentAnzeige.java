package jm.cipher.codes;

import jm.cipher.AbstractCipher;
import jm.tools.Text;

import java.util.Arrays;
import java.util.List;

/**
 * User: T05365A
 * Date: 25.09.12
 * Time: 15:57
 */
public class SiebenSegmentAnzeige extends AbstractCipher {
	private String alphabet = "0123456789ABCDEF";
	private String[] codes = {
		"1111110", // 0
		"0110000", // 1
		"1101101", // 2
		"1111001", // 3
		"0110011", // 4
		"1011011", // 5
		"1011111", // 6
		"1110000", // 7
		"1111111", // 8
		"1111011", // 9
		"1110111", // A
		"0011111", // b
		"1001110", // C
		"0111101", // d
		"1001111", // E
		"1000111"  // F
	};

	@Override
	public String encrypt(String pt) throws Exception {
		StringBuilder ct = new StringBuilder();

		String[] lines = pt.split("\n");
		for (String line : lines) {
			for (char c : line.toCharArray()) {
				if (alphabet.indexOf(c) >= 0) {
					ct.append(codes[alphabet.indexOf(c)]).append(" ");
				} else {
					ct.append(c);
				}
			}
		}

		return ct.toString();
	}

	@Override
	public String decrypt(String ct) throws Exception {
		String[] lines = ct.split("\n");
		StringBuilder pt = new StringBuilder();
		List<String> codeList = Arrays.asList(codes);
		for (String line : lines) {
			String[] words = line.split(" ");
			if (words.length > 0) {
				for (String word : words) {
					if (codeList.contains(word)) {
						pt.append(alphabet.charAt(codeList.indexOf(word)));
					} else {
						pt.append(word);
					}
				}
			}
			pt.append("\n");
		}

		return pt.toString();
	}

	@Override
	public String getDisplayname() {
		return "Sieben-Segment-Anzeige";
	}

	@Override
	public String getName() {
		return "SiebenSegmentAnzeige";
	}

	@Override
	public String getDescription() {
		return "Sieben-Segment-Anzeige wie im Wikipediaartikel. Es können nur 0-9 und A-F verschlüsselt werden";
	}
}
