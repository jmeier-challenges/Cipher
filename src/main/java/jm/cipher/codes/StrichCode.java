package jm.cipher.codes;

import jm.cipher.AbstractCipher;

import java.util.*;

/**
 * User: T05365A
 * Date: 28.09.12
 * Time: 12:21
 *
 * Zielcode der Post
 * http://de.wikipedia.org/wiki/Zielcode
 */
public class StrichCode extends AbstractCipher {

	private String alphabet = "0123456789";
	//01247
	private String[] code5 = {
		"11100",
		"00111",
		"01011",
		"10011",
		"01101",
		"10101",
		"11001",
		"01110",
		"10110",
		"11010"
	};

	//8421
	private String[] code4 = {
		"1111",
		"1110",
		"1101",
		"1100",
		"1011",
		"1010",
		"1001",
		"0101",
		"0111",
		"0110",
	};

	public enum CODE {CODE5, CODE4,};

	private CODE code = CODE.CODE5;

	@Override
	public String encrypt(String pt) throws Exception {
		StringBuilder ct = new StringBuilder();
		String[] lines = pt.split("\n");
		List<String> codes = getCodes();
		for (String line : lines) {
			for (char c : line.toCharArray()) {
				int index = alphabet.indexOf(c);
				if (index < 0) {
					ct.append(c);
				} else {
					ct.append(codes.get(index));
					ct.append(" ");
				}
				ct.append("\n");
			}
		}
		return ct.toString();
	}

	@Override
	public String decrypt(String ct) throws Exception {
		StringBuilder pt = new StringBuilder();
		String[] lines = ct.split("\n");
		List<String> codes = getCodes();
		for (String line : lines) {
			List<String> words = split(line, alphabet);
			for (String word : words) {
				int pos = codes.indexOf(word);
				if (pos < 0) {
					pt.append(" ").append(word);
				} else {
					pt.append(codes.indexOf(word));
				}
			}
			pt.append("\n");
		}

		return pt.toString();
	}

	public static void main(String[] args) {
		StrichCode code = new StrichCode();
		System.out.println(code.split("N 01101 11010 01011 10101.00111 10011 10110",code.alphabet));

	}
	private List<String> split(String line, String alphabet) {
		List<String> parts = new ArrayList<String>();
		StringBuilder partAlphabet = new StringBuilder();
		StringBuilder partNotAlphabet = new StringBuilder();

		for (char c : line.toCharArray()) {
			int index = alphabet.indexOf(c);
			if (c == ' ') {
				if (partAlphabet.length() > 0) {
					parts.add(partAlphabet.toString());
					partAlphabet = new StringBuilder();
				}
				if (partNotAlphabet.length() > 0) {
					parts.add(partNotAlphabet.toString());
					partNotAlphabet = new StringBuilder();
				}
				continue;
			}

			if (index < 0) {
				// not in alphabet
				if (partAlphabet.length() > 0) {
					parts.add(partAlphabet.toString());
					partAlphabet = new StringBuilder();
				}
				partNotAlphabet.append(c);
			} else {
				if (partNotAlphabet.length() > 0) {
					parts.add(partNotAlphabet.toString());
					partNotAlphabet = new StringBuilder();
				}
				partAlphabet.append(c);
			}
		}
		if (partAlphabet.length() > 0) {
			parts.add(partAlphabet.toString());
		}
		if (partNotAlphabet.length() > 0) {
			parts.add(partNotAlphabet.toString());
		}
		return parts;
	}

	private List<String> getCodes() {
		if (getCode() == CODE.CODE4) {
			return Arrays.asList(code4);
		} else if (getCode() == CODE.CODE5) {
			return Arrays.asList(code5);
		} else {
			throw new RuntimeException("Unknown Code");
		}
	}

	@Override
	public String getDisplayname() {
		return "Zielcode";
	}

	@Override
	public String getName() {
		return "ZielCode";
	}

	@Override
	public String getDescription() {
		return "Zielcode der Post.";
	}

	public CODE getCode() {
		return code;
	}

	public void setCode(CODE code) {
		this.code = code;
	}

	public StrichCode() {
		setCode(CODE.CODE5);
	}
}
