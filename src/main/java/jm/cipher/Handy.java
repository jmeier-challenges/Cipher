package jm.cipher;

import jm.cipher.data.IHandyData;
import jm.cipher.dataimpl.HandyData;
import jm.tools.WordIterator;

import java.util.HashMap;
import java.util.Map;

/**
 * User: T05365A
 * Date: 10.07.12
 * Time: 12:01
 */
public class Handy extends AbstractCipher implements IHandyData {
    private Map<Character, String> encode = new HashMap<Character, String>();
    private Map<String, Character> decode = new HashMap<String, Character>();
	private String ctAlphabet = "0123456789";

	private IHandyData data;


    public Handy() {
	    data = new HandyData();
        initEncode();
        initDecode();
    }
    private void initEncode() {
        encode.put('a', "2");
        encode.put('b', "22");
        encode.put('c', "222");
        encode.put('d', "3");
        encode.put('e', "33");
        encode.put('f', "333");
        encode.put('g', "4");
        encode.put('h', "44");
        encode.put('i', "444");
        encode.put('j', "5");
        encode.put('k', "55");
        encode.put('l', "555");
        encode.put('m', "6");
        encode.put('n', "66");
        encode.put('o', "666");
        encode.put('p', "7");
        encode.put('q', "77");
        encode.put('r', "777");
        encode.put('s', "7777");
        encode.put('t', "8");
        encode.put('u', "88");
        encode.put('v', "888");
        encode.put('w', "9");
        encode.put('x', "99");
        encode.put('y', "999");
        encode.put('z', "9999");

        encode.put(' ', "0");
        encode.put('.', "1");
        encode.put('*', "*");
        encode.put('#', "#");
    }

    private void initDecode() {
	    decode = new HashMap<String, Character>();
        decode.put("2", 'a');
        decode.put("22", 'b');
        decode.put("222", 'c');
        decode.put("3", 'd');
        decode.put("33", 'e');
        decode.put("333", 'f');
        decode.put("4", 'g');
        decode.put("44", 'h');
        decode.put("444", 'i');
        decode.put("5", 'j');
        decode.put("55", 'k');
        decode.put("555", 'l');
        decode.put("6", 'm');
        decode.put("66", 'n');
        decode.put("666", 'o');
        decode.put("7", 'p');
        decode.put("77", 'q');
        decode.put("777", 'r');
        decode.put("7777", 's');
        decode.put("8", 't');
        decode.put("88", 'u');
        decode.put("888", 'v');
        decode.put("9", 'w');
        decode.put("99", 'x');
        decode.put("999", 'y');
        decode.put("9999", 'z');

        decode.put("0", ' ');
        decode.put("1", '.');
        decode.put("*", '*');
        decode.put("#", '#');
    }

	void initNumberDecode() {
		decode = new HashMap<String, Character>();
		decode.put("21", 'a');
		decode.put("22", 'b');
		decode.put("23", 'c');
		decode.put("31", 'd');
		decode.put("32", 'e');
		decode.put("33", 'f');
		decode.put("41", 'g');
		decode.put("42", 'h');
		decode.put("43", 'i');
		decode.put("51", 'j');
		decode.put("52", 'k');
		decode.put("53", 'l');
		decode.put("61", 'm');
		decode.put("62", 'n');
		decode.put("63", 'o');
		decode.put("71", 'p');
		decode.put("72", 'q');
		decode.put("73", 'r');
		decode.put("74", 's');
		decode.put("81", 't');
		decode.put("82", 'u');
		decode.put("83", 'v');
		decode.put("91", 'w');
		decode.put("92", 'x');
		decode.put("93", 'y');
		decode.put("94", 'z');
	}

    public String encrypt(String pt) throws Exception {
	    initialize();
        StringBuilder ct = new StringBuilder();
	    String[] lines = pt.split("\n");
	    for (String line : lines) {
		    for (char c : line.toCharArray()) {
			    char cl = Character.toLowerCase(c);
			    if (encode.containsKey(cl)) {
				    String cc = encode.get(Character.toLowerCase(c));
				    ct.append(cc).append(" ");
			    } else {
				    ct.append(c).append(" ");
			    }
		    }
		    ct.append("\n");
	    }
        return ct.toString();
    }

    public String decrypt(String ct) throws Exception {
	    initialize();
        StringBuilder pt = new StringBuilder();
	    WordIterator wordIterator = new WordIterator();
	    wordIterator.setAlphabet(ctAlphabet);
	    String[] lines = ct.split("\n");
	    for (String line : lines) {
		    wordIterator.setText(line);
		    while (wordIterator.hasNext()) {
			    String word = wordIterator.next();
			    if (decode.containsKey(word)) {
				    pt.append(decode.get(word));
			    } else {
				    pt.append(word);
			    }
		    }
		    pt.append("\n");
	    }

        return pt.toString();
    }

	private void initialize() {
		switch (data.getType()) {
			case NUMBER:
				initNumberDecode();
				break;
			case REPEATED_NUMBER:
				initDecode();
				initEncode();
				break;
			default:
				throw new RuntimeException("Unhandled HANDY_TYPE " + data.getType());
		}
	}

	public String getDisplayname() {
        return "Handy";
    }

    public String getName() {
        return "Handy";
    }

    public String getDescription() {
        return "Handyverschlüsselung: 2 = a, 22 = b, 222 = c usw.";
    }

	@Override
	public Memento getMemento() {
		HandyData memento = new HandyData();
		memento.setType(getType());
		return memento;
	}

	@Override
	public void setMemento(Memento memento) {
		data.setType(((HandyData)memento).getType());
	}

	@Override
	public void setIData(Object data) {
		this.data = (IHandyData) data;
	}

	public static void main(String[] args) throws Exception {
        Handy cipher = new Handy();

		String ct = "83 43 32 73; 62 32 82 62; 32 43 62 74; 21 23 42 81; 62 32 82 62; 74 32 23 42 74; 31 73 32 43";
		cipher.setType(HANDY_TYPE.NUMBER);
		System.out.println(cipher.decrypt(ct));

    }

	@Override
	public HANDY_TYPE getType() {
		return data.getType();
	}

	@Override
	public void setType(HANDY_TYPE type) {
		switch (type) {
			case NUMBER:
				initNumberDecode();
				break;
			case REPEATED_NUMBER:
				initDecode();
				break;
			default:
				throw new RuntimeException("Unhandled HANDY_TYPE" + type.toString());
		}
		data.setType(type);
	}
}
