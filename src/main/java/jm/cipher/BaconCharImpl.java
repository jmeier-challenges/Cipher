package jm.cipher;

public abstract class BaconCharImpl implements IBaconChar {
	private boolean swapped = false;
	public void setSwap(boolean swap) {
		swapped = swap;
	}

	public boolean isBaconA(final char c) {
		if (swapped) {
			return isBaconBImpl(c);
		} else {
			return isBaconAImpl(c);
		}
	}

	public boolean isBaconB(final char c) {
		if (swapped) {
			return isBaconAImpl(c);
		} else {
			return isBaconBImpl(c);
		}
	}

	protected abstract boolean isBaconAImpl(final char c);
	protected abstract boolean isBaconBImpl(final char c);
}
