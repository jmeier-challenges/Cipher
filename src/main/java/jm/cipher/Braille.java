package jm.cipher;

import jm.cipher.data.IBrailleData;
import jm.tools.Text;

import java.util.HashMap;
import java.util.Map;

/**
 * User: T05365A
 * Date: 10.10.12
 * Time: 10:49
 *
 *  https://de.wikipedia.org/wiki/Brailleschrift
 *  http://www.fakoo.de/download/pdf/Das_System_der_deutschen_Blindenschrift_2005.pdf
 */
public class Braille extends AbstractCipher {
	Map<String, String> codeToAlphabet;
	Map<String, String> alphabetToCode;
	Map<String, String> codeToNumber;
	private String ptAlphabet;
	private String ctAlphabet;

    private IBrailleData data;

	private String numberFollows = "001111";

	void initializeNumbers() {
		codeToNumber = new HashMap<String, String>();
		codeToNumber.put("100000", "1");
		codeToNumber.put("110000", "2");
		codeToNumber.put("100100", "3");
		codeToNumber.put("100110", "4");
		codeToNumber.put("100010", "5");
		codeToNumber.put("110100", "6");
		codeToNumber.put("110110", "7");
		codeToNumber.put("110010", "8");
		codeToNumber.put("010100", "9");
		codeToNumber.put("010110", "0");
	}

	void initialize() {
		codeToAlphabet = new HashMap<String,String>();
		codeToAlphabet.put("100000", "a");
		codeToAlphabet.put("110000", "b");
		codeToAlphabet.put("100100", "c");
		codeToAlphabet.put("100110", "d");
		codeToAlphabet.put("100010", "e");
		codeToAlphabet.put("110100", "f");
		codeToAlphabet.put("110110", "g");
		codeToAlphabet.put("110010", "h");
		codeToAlphabet.put("010100", "i");
		codeToAlphabet.put("010110", "j");
		codeToAlphabet.put("101000", "k");
		codeToAlphabet.put("111000", "l");
		codeToAlphabet.put("101100", "m");
		codeToAlphabet.put("101110", "n");
		codeToAlphabet.put("101010", "o");
		codeToAlphabet.put("111100", "p");
		codeToAlphabet.put("111110", "q");
		codeToAlphabet.put("111010", "r");
		codeToAlphabet.put("011100", "s");
		codeToAlphabet.put("011110", "t");
		codeToAlphabet.put("101001", "u");
		codeToAlphabet.put("111001", "v");
		codeToAlphabet.put("010111", "w");
		codeToAlphabet.put("101101", "x");
		codeToAlphabet.put("101111", "y");
		codeToAlphabet.put("101011", "z");

		codeToAlphabet.put("001110", "ä");
		codeToAlphabet.put("010101", "ö");
		codeToAlphabet.put("110011", "ü");

		codeToAlphabet.put("010000", ",");
		codeToAlphabet.put("011000", ";");
		codeToAlphabet.put("010010", ":");
		codeToAlphabet.put("001000", ".");
		codeToAlphabet.put("010001", "?");
		codeToAlphabet.put("011010", "!");
		codeToAlphabet.put("011001", "\"");
		codeToAlphabet.put("001011", "\"");
		codeToAlphabet.put("011011", "("); // klammern
		codeToAlphabet.put("001001", "-");
		codeToAlphabet.put("001101", "§");
		codeToAlphabet.put("000001", "'");


		alphabetToCode = new HashMap<String,String>();
		alphabetToCode.put("a", "100000");
		alphabetToCode.put("b", "110000");
		alphabetToCode.put("c", "100100");
		alphabetToCode.put("d", "100110");
		alphabetToCode.put("e", "100010");
		alphabetToCode.put("f", "110100");
		alphabetToCode.put("g", "110110");
		alphabetToCode.put("h", "110010");
		alphabetToCode.put("i", "010100");
		alphabetToCode.put("j", "010110");
		alphabetToCode.put("k", "101000");
		alphabetToCode.put("l", "111000");
		alphabetToCode.put("m", "101100");
		alphabetToCode.put("n", "101110");
		alphabetToCode.put("o", "101010");
		alphabetToCode.put("p", "111100");
		alphabetToCode.put("q", "111110");
		alphabetToCode.put("r", "111010");
		alphabetToCode.put("s", "011100");
		alphabetToCode.put("t", "011110");
		alphabetToCode.put("u", "101001");
		alphabetToCode.put("v", "111001");
		alphabetToCode.put("w", "010111");
		alphabetToCode.put("x", "101101");
		alphabetToCode.put("y", "101111");
		alphabetToCode.put("z", "101011");

		alphabetToCode.put("001110", "ä");
		alphabetToCode.put("010101", "ö");
		alphabetToCode.put("110011", "ü");

		alphabetToCode.put(",", "010000");
		alphabetToCode.put(";", "011000");
		alphabetToCode.put(":", "010010");
		alphabetToCode.put(".", "001000");
		alphabetToCode.put("?", "010001");
		alphabetToCode.put("!", "011010");
		alphabetToCode.put("\"", "01100");
		alphabetToCode.put("\"", "001011");
		alphabetToCode.put("(", "011011"); // klammern
		alphabetToCode.put("-", "001001");
		alphabetToCode.put("§", "001101");
		alphabetToCode.put("'", "000001");

	}
	private void initKurzschrift() {
		// kurzschrift
		codeToAlphabet.put("001100", "äu");
		codeToAlphabet.put("011000", "be");
		codeToAlphabet.put("100111", "ch");
		codeToAlphabet.put("000101", "ck");
		codeToAlphabet.put("011011", "eh");
		codeToAlphabet.put("100101", "ei");
		codeToAlphabet.put("110101", "ein");
		codeToAlphabet.put("101111", "el");
		codeToAlphabet.put("111011", "em");
		codeToAlphabet.put("100100", "en");
		codeToAlphabet.put("110111", "er");
		codeToAlphabet.put("111111", "es");
		codeToAlphabet.put("110001", "eu");
		codeToAlphabet.put("111101", "ge");
		codeToAlphabet.put("001111", "ich");
		codeToAlphabet.put("001101", "ie");
		codeToAlphabet.put("000110", "ig");
		codeToAlphabet.put("001010", "in");
		codeToAlphabet.put("000111", "lich");
		codeToAlphabet.put("111110", "ll");
		codeToAlphabet.put("101101", "mm");
		codeToAlphabet.put("010001", "or");
		codeToAlphabet.put("100011", "sch");
		codeToAlphabet.put("011101", "ss");
		codeToAlphabet.put("011111", "st");
		codeToAlphabet.put("011001", "te");
		codeToAlphabet.put("010011", "un");

	}

	public Braille() {
        setIData(new BrailleData());
		initialize();
		initializeNumbers();
		setCtAlphabet("01");
		setPtAlphabet(Text.LC_ALPHABET);
	}

	@Override
	public String encrypt(String pt) throws Exception {
		StringBuilder builder = new StringBuilder();
		for (char c : pt.toCharArray()) {
            if (alphabetToCode.containsKey(String.valueOf(c))) {
                builder.append(alphabetToCode.get(String.valueOf(c))).append(" ");
            }
        }
        if (isBinary()) {
            return builder.toString();
        } else {
            return toString(builder.toString());
        }
    }

	@Override
	public String decrypt(String ct) throws Exception {
        if (!isBinary()) {
            ct = toBinary(ct);
        }
        StringBuilder pt = new StringBuilder();
        String[] lines = ct.split("\n");
        boolean nowComeNumbers = false;
        for (String line : lines) {
            String[] words = line.split(" ");
            for (String word : words) {
                if (nowComeNumbers) {
                    pt.append(codeWord(word, codeToNumber));
                } else {
                    if (numberFollows.equals(word)) {
                        nowComeNumbers = true;
                    } else {
                        pt.append(codeWord(word, codeToAlphabet));
                    }
                }
            }
            nowComeNumbers = false;
            pt.append("\n");
        }
        return pt.toString();
    }

	private String codeWord(String word, Map<String, String> alphabet) {
		if (alphabet.containsKey(word)) {
			return alphabet.get(word);
		} else {
			return " " +word + " ";
		}
	}

    @Override
    public void setIData(Object data) {
        this.data = (IBrailleData) data;
    }

    @Override
	public String getDisplayname() {
		return "Braille";
	}

	@Override
	public String getName() {
		return "Braille";
	}

	@Override
	public String getDescription() {
		return "Braille";
	}

	public String toString(String binaryBraille) {
		binaryBraille = Text.getStripped(binaryBraille, getCtAlphabet());
		int length = binaryBraille.length() / 6;
		StringBuilder builder = new StringBuilder(binaryBraille.length());
		for (int i = 0; i < length; i++) {
			builder.append(binaryBraille.charAt(i * 6));
			builder.append(binaryBraille.charAt(i * 6 + 3));
			builder.append(" ");
		}
		builder.append("\n");
		for (int i = 0; i < length; i++) {
			builder.append(binaryBraille.charAt(i * 6 + 1));
			builder.append(binaryBraille.charAt(i * 6 + 4));
			builder.append(" ");
		}
		builder.append("\n");
		for (int i = 0; i < length; i++) {
			builder.append(binaryBraille.charAt(i * 6 + 2));
			builder.append(binaryBraille.charAt(i * 6 + 5));
			builder.append(" ");
		}
		return builder.toString();
	}

    public String toBinary(String braille) {
        StringBuffer buffer = new StringBuffer();
        String[] lines = braille.split("\n");
        int maxLines = lines.length/3;

        for (int i = 0; i < maxLines; i++) {
            String[] words1 = lines[i].split(" ");
            String[] words2 = lines[i+1].split(" ");
            String[] words3 = lines[i+2].split(" ");
            int maxWords = getMax(words1.length, words2.length, words3.length);
            for (int k = 0; k < maxWords; k++) {
                String word1 = words1[k];
                String word2 = words2[k];
                String word3 = words3[k];

                buffer.append(word1.charAt(0));
                buffer.append(word2.charAt(0));
                buffer.append(word3.charAt(0));
                buffer.append(word1.charAt(1));
                buffer.append(word2.charAt(1));
                buffer.append(word3.charAt(1));
                buffer.append(" ");
            }
        }

        return buffer.toString();
    }

    private int getMax(int length, int length1, int length2) {
        int min = length;
        if (length1 < min) {
            min = length1;
        }
        if (length2 < min) {
            min = length2;
        }
        return min;
    }

    public static void main(String[] args) throws Exception {
        Braille braille = new Braille();
        braille.setBinary(false);
        String ct = braille.encrypt("wolfgang ist toll");
        System.out.println(ct);
        System.out.println();
        String pt = braille.decrypt(ct);
        System.out.println(pt);

		//fuehlen();
		//guenther();
	}

	private static void fuehlen() throws Exception {
		Braille cipher = new Braille();
		String pt = "qcojuxoopxjlqxz";
		String ct = cipher.encrypt(pt);
		System.out.println(ct);
		System.out.println();
		System.out.println(" 4  9  2  5  x  y  z  0  1  1  0  1  a  b  c");
		System.out.println(cipher.toString(ct));
	}

	private static void guenther() throws Exception {
		Braille cipher = new Braille();
		String pt = "oslgbzfbdhalffh";
		String ct = cipher.encrypt(pt);
		System.out.println(ct);
		System.out.println();
		System.out.println(" 4  9  2  0  x  y  z  0  1  1  0  5  a  b  c");
		System.out.println(cipher.toString(ct));
	}

	public String getPtAlphabet() {
		return ptAlphabet;
	}

	public void setPtAlphabet(String ptAlphabet) {
		this.ptAlphabet = ptAlphabet;
	}

	public String getCtAlphabet() {
		return ctAlphabet;
	}

	public void setCtAlphabet(String ctAlphabet) {
		this.ctAlphabet = ctAlphabet;
	}

    public boolean isBinary() {
        return data.isBinary();
    }

    public void setBinary(boolean aBinary) {
        data.setBinary(aBinary);
    }

    class BrailleData implements IBrailleData {
        private boolean binary = true;

        @Override
        public boolean isBinary() {
            return binary;
        }

        @Override
        public void setBinary(boolean aBinary) {
            binary = aBinary;
        }
    }

}
