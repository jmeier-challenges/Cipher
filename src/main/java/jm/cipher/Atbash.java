package jm.cipher;

import jm.cipher.data.IAtbashData;
import jm.tools.Text;

/**
 * User: johann
 * Date: 25.04.11
 * Time: 17:50
 */
public class Atbash extends AbstractCipher implements IAtbashData {
    public final static String[] CommonAlphabets = {Text.LC_ALPHABET, Text.UC_ALPHABET};
    private IAtbashData data;

	public Atbash() {
        setIData(new AtbashData());
        setAlphabet(Text.LC_ALPHABET);
    }

    @Override
    public void setAlphabet(String alphabet) {
        data.setAlphabet(alphabet);
    }

    @Override
    public String getAlphabet() {
        return data.getAlphabet();
    }

    public String encrypt(String pt) {
        StringBuilder ct = new StringBuilder();
        for (int i = 0; i < pt.length(); i++) {
            char pc = pt.charAt(i);
            ct.append(cipher(pc));
        }
        return  ct.toString();
    }

    public String decrypt(String ct) {
        return encrypt(ct);
    }

    public String getDisplayname() {
        return "Atbashcode";
    }

	public String getName() {
        return "Atbash";
    }

    public String getDescription() {
        return "Atbashverschlüssellung: A-Z wird zu Z-A";
    }

    private char cipher(char c) {
	    String alphabet = getAlphabet();
        int pos = alphabet.indexOf(c);
        if (pos < 0) {
            return c;
        } else {
            return alphabet.charAt(alphabet.length() - pos - 1);
        }
    }

    public static void main(String[] args) {
        Atbash atbash = new Atbash();
        String pt = "abcde";
        String ct = atbash.encrypt(pt);
        System.out.println(pt);
        System.out.println(ct);
        System.out.println(atbash.decrypt(ct));
    }

    @Override
    public void setIData(Object data) {
        this.data = (IAtbashData) data;
    }

    @Override
    public void setMemento(Memento memento) {
        setAlphabet(((AtbashData) memento).getAlphabet());
    }

    @Override
    public Memento getMemento() {
        AtbashData memento = new AtbashData();
        memento.setAlphabet(getAlphabet());
        return memento;
    }

    class AtbashData extends Memento implements IAtbashData {
        private String alphabet;

        @Override
        public String getAlphabet() {
            return alphabet;
        }

        @Override
        public void setAlphabet(String alphabet) {
            this.alphabet = alphabet;
        }
    }
}
