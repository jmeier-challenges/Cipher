package jm.cipher.brutable;

import jm.cipher.Caesar;
import jm.tools.Language;
import jm.tools.Text;
import jm.util.BestN;

public class CaesarBrute extends AbstractBrutableCipher {
    private Caesar caesar = new Caesar();

	public static void main(String[] args) throws Exception {
		CaesarBrute brute = new CaesarBrute();
        brute.setLanguage(Language.DE);
		String ct = "jbystnat vfg zny jvrqre vz ynaqr";
		System.out.println(brute.brute(ct, Language.DE));
	}

	@Override
	public BestN<Object> brute(String ct, Language language) throws Exception {
		BestN<Object> bestN = new BestN<Object>();
        caesar.setAlphabet(getCtAlphabet());
		caesar.setPreserveCase(true);

		for (int shift = 1; shift < getCtAlphabet().length(); shift++) {
			caesar.setShift(shift);
			String pt = caesar.decrypt(ct);
			int fitness = Text.calculateFitness(pt, getLanguage());
			bestN.put(fitness, new Information(pt, Integer.toString(shift)));
		}

		return bestN;
	}

	@Override
	public String getName() {
        return "Caesar";
    }

	class Information {
		private String pt;
		private String key;

		Information(String pt, String key) {
			this.pt = pt;
			this.key = key;
		}

		@Override
		public String toString() {
			return pt + ": Key = " + key;
		}
	}
}