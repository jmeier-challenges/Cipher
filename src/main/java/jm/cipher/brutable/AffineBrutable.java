package jm.cipher.brutable;

import jm.cipher.Affine;
import jm.tools.Language;
import jm.tools.Text;
import jm.util.BestN;

import java.util.List;

/**
 * User: johann
 * Date: 09.09.12
 * Time: 20:25
 */
public class AffineBrutable extends AbstractBrutableCipher {
    private Affine cipher = new Affine();
    private String knownPlaintext;

	public static void main(String[] args) throws Exception {
		AffineBrutable cipher = new AffineBrutable();
		cipher.setCtAlphabet("abcdefghijklmnopqrstuvwxyz ,.!+-");
		System.out.println(cipher.brute("jriwzhozv-!avt-ovariit vlhoo+", Language.DE));
	}

    @Override
    public BestN<Object> brute(String ct, Language language) throws Exception {
        BestN<Object> bestN = new BestN<Object>();
        cipher.setPtAlphabet(getCtAlphabet());

        List<Integer> validAs = cipher.getValidAs();

        cipher.setCbc(false);
        for (int a : validAs) {
            cipher.setA(a);
            for (int b = 0; b < cipher.getPtAlphabet().length(); b++) {
                cipher.setB(b);
                String pt = cipher.decrypt(ct);
                if (knownPlaintext != null) {
                    if (pt.contains(knownPlaintext)) {
                        int fitness = 10000;
                        bestN.put(fitness, pt);
                    }
                }
                int fitness = Text.calculateFitness(pt, language);
                bestN.put(fitness, pt);
            }
        }

        for (int iv = 0; iv < 32; iv++) {
            cipher.setIv(iv);
            for (int a : validAs) {
                cipher.setA(a);
                for (int b = 0; b < cipher.getPtAlphabet().length(); b++) {
                    cipher.setB(b);
                    String pt = cipher.decrypt(ct);
                    if (knownPlaintext != null && !knownPlaintext.isEmpty()) {
                        if (pt.contains(knownPlaintext)) {
                            int fitness = 10000;
                            bestN.put(fitness, pt);
                        }
                    }
                    int fitness = Text.calculateFitness(pt, language);
                    bestN.put(fitness, pt);
                }
            }
        }

        return bestN;
    }


    @Override
    public void setKnownPlaintext(String knownPlaintext) {
        this.knownPlaintext = knownPlaintext;
    }

    @Override
    public void setCtAlphabet(String alphabet) {
        cipher.setPtAlphabet(alphabet);
    }

    @Override
    public String getName() {
        return cipher.getName();
    }

    @Override
    public String toString() {
        return getName();
    }
}
