package jm.cipher.brutable;

import jm.cipher.transposition.Railfence;
import jm.tools.Language;
import jm.tools.Text;
import jm.util.BestN;

/**
 * User: T05365A
 * Date: 11.10.12
 * Time: 10:51
 */
public class RailfenceBruter extends AbstractBrutableCipher {
	Railfence railfence;

	public static void main(String[] args) throws Exception {
		RailfenceBruter bruter = new RailfenceBruter();


		String ct = "dnetlhseedheswloteateftaafcl";
		String trimmedCt = Text.getStripped(ct);

		BestN<Object> bestN = bruter.brute(trimmedCt, Language.DE);
		System.out.println(bestN);
	}

	public RailfenceBruter() {
		railfence = new Railfence();
	}

	@Override
	public BestN<Object> brute(String ct, Language language) throws Exception {
		BestN<Object> bestN = new BestN<Object>();

		for (int railCount = 2; railCount < 10; railCount++) {
			for (int startRail = 0; startRail < railCount; startRail++) {
				railfence.setRailCount(railCount);
				railfence.setStartRail(startRail);
				String pt = railfence.decrypt(ct);
				int fitness = Text.calculateFitness(pt, language);
				bestN.put(fitness, pt);
			}
		}

		return bestN;
	}

	@Override
	public String getName() {
		return "Rail Fence";
	}
}
