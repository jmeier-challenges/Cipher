package jm.cipher.brutable;

class Information {
	public String text;
	public String key;

	Information(String text, String key) {
		this.text = text;
		this.key = key;
	}

	@Override
	public String toString() {
		return  text + ", key=" + key;
	}
}