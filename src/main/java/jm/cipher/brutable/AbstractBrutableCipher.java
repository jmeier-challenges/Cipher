package jm.cipher.brutable;

import jm.cipher.IBrutableCipher;
import jm.tools.Language;
import jm.tools.Text;

/**
 * User: T05365A
 * Date: 11.10.12
 * Time: 09:21
 */
public abstract class AbstractBrutableCipher implements IBrutableCipher {
	private String ctAlphabet = Text.LC_ALPHABET;
	private String knownPlaintext;
	private Language language;

	@Override
	public void setCtAlphabet(String alphabet) {
		ctAlphabet = alphabet;
	}

	public String getCtAlphabet() {
		return ctAlphabet;
	}

	@Override
	public void setKnownPlaintext(String knownPt) {
		knownPlaintext = knownPt;
	}

	public String getKnownPlaintext() {
		return knownPlaintext;
	}

	@Override
	public void setLanguage(Language language) {
		this.language = language;
	}

	public Language getLanguage() {
		return language;
	}

	@Override
	public String toString() {
		return getName();
	}
}
