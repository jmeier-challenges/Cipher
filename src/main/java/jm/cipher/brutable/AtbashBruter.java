package jm.cipher.brutable;

import jm.cipher.Atbash;
import jm.tools.Language;
import jm.tools.Text;
import jm.util.BestN;

/**
 * User: T05365A
 * Date: 11.10.12
 * Time: 11:32
 */
public class AtbashBruter extends AbstractBrutableCipher {
	Atbash atbash = new Atbash();

	@Override
	public BestN<Object> brute(String ct, Language language) throws Exception {
		BestN<Object> bestN = new BestN<Object>();

		String pt = atbash.decrypt(ct);
		int fitness = Text.calculateFitness(pt, language);
		bestN.put(fitness, pt);

		return bestN;
	}


	@Override
	public String getName() {
		return "Atbash";
	}
}
