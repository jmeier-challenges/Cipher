package jm.cipher.brutable;

import jm.cipher.Trithemius;
import jm.tools.Language;
import jm.tools.Text;
import jm.util.BestN;

/**
 * User: T05365A
 * Date: 11.10.12
 * Time: 10:57
 */
public class TrithemiusBruter extends AbstractBrutableCipher {
	Trithemius trithemius = new Trithemius();

	public static void main(String[] args) throws Exception {
		Trithemius trithemius = new Trithemius();
		String pt = "wolfgang ist ein besonders toller mann";
		String ct = trithemius.decrypt(pt);

		TrithemiusBruter bruter = new TrithemiusBruter();
		System.out.println(bruter.brute(ct, Language.DE));
	}

	@Override
	public BestN<Object> brute(String ct, Language language) throws Exception {
		BestN<Object> bestN = new BestN<Object>();

		String pt = trithemius.decrypt(ct);
		int fitness = Text.calculateFitness(pt, language);
		bestN.put(fitness, pt);

		pt = trithemius.encrypt(ct);
		fitness = Text.calculateFitness(pt, language);

		bestN.put(fitness, pt);

		return bestN;
	}

	@Override
	public String getName() {
		return "Trithemius";
	}
}
