package jm.cipher.brutable;

import jm.cipher.transposition.ColumnarTransposition;
import jm.tools.Language;
import jm.tools.Permutation;
import jm.tools.Text;
import jm.util.BestN;

import java.util.Arrays;

/**
 * User: T05365A
 * Date: 11.10.12
 * Time: 09:34
 */
public class ColumnarTranspositionBruter extends AbstractBrutableCipher {

	private ColumnarTransposition columnarTransposition;
	private int maxKeyLength = 8;

	public static void main(String[] args) throws Exception {
		String pt = "wolfgang ist ein toller kerl";
		ColumnarTransposition columnarTransposition = new ColumnarTransposition();
		columnarTransposition.setKey("wolfgang");
		System.out.println(Text.calculateFitness(pt, Language.DE));

		String ct = columnarTransposition.encrypt(pt);
		ColumnarTranspositionBruter bruter = new ColumnarTranspositionBruter();
		System.out.println(bruter.brute(ct, Language.DE));
	}

	public ColumnarTranspositionBruter() {
		columnarTransposition = new ColumnarTransposition();
	}

	@Override
	public BestN<Object> brute(String ct, Language language) throws Exception {
		BestN<Object> bestN = new BestN<Object>();
		bestN.setMax(80);

		bestN.add(brute(ct, language, false));
		bestN.add(brute(ct, language, true));

		return bestN;
	}

	public BestN<Object> brute(String ct, Language language, boolean blockwise) throws Exception {
		BestN<Object> bestN = new BestN<Object>();
		bestN.setMax(40);

		columnarTransposition.setBlockwise(blockwise);
		for (int keyLength = 8; keyLength <= maxKeyLength; keyLength++) {
			int[] permutation = getStartPermutation(keyLength);
			while (Permutation.hasNextPermutation(permutation)) {
				columnarTransposition.setPermutation(permutation);
				columnarTransposition.setKeyLength(permutation.length);
				String pt = columnarTransposition.decrypt(ct);
				int fitness = Text.calculateFitness(pt, language);
				bestN.put(fitness, new Information(pt, Arrays.toString(permutation)));
				if (isKey(permutation)) {
					System.out.println(Arrays.toString(permutation));
				}
				Permutation.nextPermutation(permutation);
			}
		}

		return bestN;
	}

	private boolean isKey(int[] key) {
		int[] permutation = {5, 3, 4, 7, 2, 6, 1, 0};
		if (key.length != permutation.length) {
			return false;
		}
		for (int i = 0; i < key.length; i++) {
			if (permutation[i] != key[i]) {
				return false;
			}
		}
		return true;
	}


	private int[] getStartPermutation(int keyLength) {
		int[] permutation = new int[keyLength];
		for (int i = 0; i < keyLength; i++) {
			permutation[i] = i;
		}
		return  permutation;
	}

	@Override
	public String getName() {
		return "Columnar Transposition";
	}

	class Information {
		String pt;
		String key;

		Information(String pt, String key) {
			this.pt = pt;
			this.key = key;
		}

		@Override
		public String toString() {
			return pt + " Key = " + key;
		}
	}
}
