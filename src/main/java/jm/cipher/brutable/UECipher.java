package jm.cipher.brutable;

import jm.cipher.codec.Base32;
import jm.tools.Language;
import jm.tools.Text;
import jm.util.BestN;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 * User: T05365A
 * Date: 27.08.12
 * Time: 10:26
 */
public class UECipher extends AbstractBrutableCipher {
	/** Seed string. */
	private static final String SEED =  "Menin aeide, thea, Peleiadeo Achileos " +
		"Oulomenen, he myri' Achaiois alge' etheke, " +
		"pollas d' iphthimous psychas Hadi proiapsen " +
		"heroon, autous de heloria teuche kynessin " +
		"oionoisi te pasi, Dios, eteleieto boule, " +
		"ex hou de ta prota diasteten erisante " +
		"atreides te anax andron kai dios Achilleus";


	public static void main(String[] args) throws Exception {
		UECipher cipher = new UECipher();
		//String cleanCt = clean("cipher://ZXBDMLAUEKXKM");
		String cleanCt = clean("ZVGF7I74OPWCY");

		System.out.println(cipher.brute(cleanCt, Language.DE));
	}

	private static String clean(String ct) {
		String pre = "cipher://";
		int mod = 8;
		String s = ct;
		if (ct.startsWith(pre)) {
			s = ct.substring(pre.length());
		}
		int length = s.length();
		int max = length % mod;
		if (max == 0) {
			return s;
		}

		for (int i = 0; i < mod - max; i++) {
			s = s + "=";
		}

		return s;
	}

	@Override
	public BestN<Object> brute(String ct, Language language) throws Exception {
		BestN<Object> bestN = new BestN<Object>();
		Base32 base32 = new Base32();

		byte[] work = base32.decode(clean(ct));

		Cipher cipher = Cipher.getInstance ("DESede");

		for (int offset = 0; offset < SEED.length() - 24; offset++) {
			String seed = getSeed(offset);
			// Uses triple DES :  password is 24 bytes long
			SecretKey key = new SecretKeySpec(seed.getBytes(), "DESede");

			// initialize the cipher
			cipher.init (Cipher.DECRYPT_MODE, key);

			// decipher the string
			try {
				byte[] deciphered = cipher.doFinal(work);
				String pt = new String(deciphered);
				int fittnes = Text.calculateFitness(pt, language);
				bestN.put(fittnes, pt);
			} catch (BadPaddingException ex) {
				// ignore
			} catch (ArrayIndexOutOfBoundsException ex) {
				// ignore
			}

		}
		return bestN;
	}

	@Override
	public String getName() {
		return "UE";
	}

    public static String getSeed(int offset) {
		return SEED.substring(offset, offset + 24);
	}
}
