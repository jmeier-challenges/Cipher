package jm.cipher.brutable;

import jm.cipher.Hill;
import jm.math.ModuloArithmetik;
import jm.tools.Language;
import jm.tools.Text;
import jm.util.BestN;

/**
 * User: johann
 * Date: 24.09.12
 * Time: 22:34
 */
public class HillBruter extends AbstractBrutableCipher {

	public static void main(String[] args) throws Exception {
		String plaintext = "dascachenindiesergegendistwirklichtollweilderanteildermysteriesseh";
		plaintext = "ierziggra";
		plaintext = "vierziggr";
		String ct = "vyskwwpwxkuhweogduquptfixpievmjunpbuqbqmvdnqpgobmqgrhlkwxphlwemebfofqehoxpxrcfweogpwmdznpgiouhioxprrvotmrznqmerzvygacjycyffphpafsahlsstmtxvyzalthlptxiswxrsyjrfeevmqqjqeubhlfgduptucngnpeqycyfcggapwrzpthyzkngjerzjonpptkwxphlxvmqxposhluhhlrzqcextxpwmeptxiuwhlbwhlmqmzrrxyvoctbfssexcgzazquhhlhlzkbohlyckuzaxpptjoqbwonhadmqxphlfsqaiukdhhwezkuhskxekphohltwnphynpatwercjqqutxmmrwmqxpxezaaielptoqnpimpjlkofnnvyganuftptnnobnqlcwwpwjuquohmqlywnfsxrxrsvweevsavbhhadmqxrcfhyucgyzfxrtdognpvnxrnqqhbamqxrcfhyucgyciuhkyvocnvbhhzmqbognpfxxrtdwwyfjquhhlvorzxrvynpslsahljuqunguhhlmmhzgayfxriorzswbgpapttlwenylwycdnmqnqfwgapw";
		ct = "oonaegqlirjbsqgbceaaurchpssbkqckrxirzijkvpujmdlzkvpujmqqcmdcsqgorhfeldkkbkovvksqgnqefuvlefknhuctriadmbowhpmziygnwrahpmmjvsjwfmvqbmdpqxnqhpmlsgpznuahyviwqgotpfhjhtobfoybfarfmdaahwszyuzkazinzkahahpydtnvzlmkofyydtsyjqliqqcmdcdkqmxfaqetrkpkieqtzwjdclgiyhyvoqgpwjnuatzjhlvtyinizscpzwjpgsydgujmxjbdymnuaoqgcezknuqvoogjlnlckvtyinizossptnrculmkjqbzeubxoyzrgsrfodpptyyfyfagwulmkbykukqxwlkqfhdnzaikvviyonsplajnwrahpyxrlsedlmujmaegcdemuizuldoklbiynjllnyyfpixmjjvvxckvllnhwzsgqkxhvaomgzvyeqhmujmvibisuvvkckviygkjloexwfqglinjrpljskkejwvaoywxntqllnbprydtllnkvvbezmdpqsvbmepmziufbiqppttkgydtubzhguhmoyyfabcegmxmclwuiwdbuc";

		HillBruter bruter = new HillBruter();
		bruter.setKnownPlaintext(plaintext);
		System.out.println(bruter.brute(ct, Language.DE));
	}

	public HillBruter() {
		setCtAlphabet(Text.LC_ALPHABET);
	}

	private long[][] getMatrix(String plaintext, int n) {
		long[][] matrix = new long[n][n];
		for (int row = 0; row < n; row++) {
			for (int col = 0; col < n; col++) {
				int pos = col * n + row;
				char c = plaintext.charAt(pos);
				matrix[row][col] = getCtAlphabet().indexOf(c);
			}
		}

		return matrix;
	}

	@Override
    public BestN<Object> brute(String ct, Language language) throws Exception {
	    if (getKnownPlaintext().isEmpty()) {
		    return doBrute(ct, language);
	    } else {
		    return doKnownPlaintext(ct, language);
	    }
    }

	private BestN<Object> doKnownPlaintext(String ct, Language language) {
		Hill cipher = new Hill();
		cipher.setPtAlphabet(getCtAlphabet());
		BestN<Object> bestN =new BestN<Object>();
		ModuloArithmetik arithmetik = new ModuloArithmetik(26);
		int maxN = 5;
		String plaintext = getKnownPlaintext();
		for (int n = 2; n <= maxN; n++) {
			for (int i = 0; i <= plaintext.length() - n * n; i++) {
				long[][] ptMatrix = getMatrix(plaintext.substring(i), n);

				try {
					long[][] inversPtMatrix = arithmetik.getInverseMatrix(ptMatrix);
					for (int k = 0; k <= ct.length() - n * n; k++) {
						long[][] ctMatrix = getMatrix(ct.substring(k), n);
						long[][] keyMatrix = arithmetik.multiply(ctMatrix, inversPtMatrix);
						cipher.setKeyMatrix(keyMatrix);
						try {
							String pt = cipher.decrypt(ct);
							int fitness = Text.calculateFitness(pt, language);
							bestN.put(fitness, new Information(pt, getKey(keyMatrix)));
						} catch (Exception e) {
							// ignore
						}
					}
				} catch (RuntimeException ex) {
					// ignore
				} catch (Exception e) {
					// ignore
				}
			}
		}
		return bestN;
	}

	private BestN<Object> doBrute(String ct, Language language) {
		Hill cipher = new Hill();
		String ctAlphabet = getCtAlphabet();
		cipher.setPtAlphabet(ctAlphabet);
		int size = 2;
		long[][] keyMatrix = new long[size][size];
		BestN<Object> bestN = new BestN<Object>();
		for (int a = 0; a < ctAlphabet.length(); a++) {
			for (int b = 0; b < ctAlphabet.length(); b++) {
				for (int c = 0; c < ctAlphabet.length(); c++) {
					for (int d = 0; d < ctAlphabet.length(); d++) {
						keyMatrix[0][0] = a;
						keyMatrix[0][1] = b;
						keyMatrix[1][0] = c;
						keyMatrix[1][1] = d;
						cipher.setKeyMatrix(keyMatrix);
						try {
							String pt = cipher.decrypt(ct);
							int fitness = Text.calculateFitness(pt, language);
							bestN.put(fitness, new Information(pt, getKey(keyMatrix)));
						} catch (RuntimeException ex) {
							// ignore
						} catch (Exception e) {
							// ignore
						}
					}
				}
			}
		}
		return bestN;
	}

	public String getKey(long[][] keyMatrix) {
        StringBuilder key = new StringBuilder();
        for (int i = 0; i < keyMatrix.length; i++) {
            for (int k = 0; k < keyMatrix[i].length; k++) {
                key.append(getCtAlphabet().charAt((int) keyMatrix[k][i]));
            }
        }
        return key.toString();
    }

    @Override
    public String getName() {
        return "Hill";
    }
}



