package jm.cipher.brutable;

import jm.cipher.Vigenere;
import jm.math.BaseArithmetik;
import jm.math.ModuloArithmetik;
import jm.tools.Language;
import jm.tools.Text;
import jm.util.BestN;

/**
 * User: T05365A
 * Date: 11.10.12
 * Time: 11:04
 */
public class VigenereBruter extends AbstractBrutableCipher {
	Vigenere vigenere = new Vigenere();
	private int maxN = 5;


	public static void main(String[] args) throws Exception {
		VigenereBruter bruter = new VigenereBruter();
        String ct = "wplggbnh itt fio tplmes mbno uod iau ejnf tplme grbu. Iir oane jsu Beutjnb";
		bruter.setCtAlphabet(Text.LC_ALPHABET);
		BestN<Object> bestN = bruter.brute(ct, Language.DE);
		System.out.println(bestN);
	}

	@Override
	public BestN<Object> brute(String ct, Language language) throws Exception {
		BestN<Object> bestN = new BestN<Object>();
        bestN.setMax(200);
		String knownPlaintext = getKnownPlaintext();
		vigenere.setAlphabet(getCtAlphabet());
		if (Text.isEmpty(knownPlaintext)) {
			bestN.add(doBrute(ct, language));
		} else {
			bestN.add(doPlaintext(ct, language));
		}
		return bestN;
	}

	private BestN<Object> doPlaintext(String ct, Language language) {
		String knownPlaintext = getKnownPlaintext();
		int maxKeyLength = 20;
		BestN<Object> bestN = new BestN<Object>();
        bestN.setMax(200);

		for (int keyLength = 1; keyLength < maxKeyLength; keyLength++) {
			for (int offset = 0; offset < (ct.length() - knownPlaintext.length() + 1); offset++) {
				String key = getGuessedKey(ct, offset, keyLength);
				vigenere.setKey(key);
				String pt = vigenere.decrypt(ct);
				int fitness = Text.calculateFitness(pt, language);
				bestN.put(fitness, new Information(pt, key));
			}
		}
		return bestN;
	}

	private String getGuessedKey(String ct, int offset, int keylength) {
		String knownPlaintext = getKnownPlaintext();
		StringBuilder builder = new StringBuilder(knownPlaintext.length());
		String ctAlphabet = getCtAlphabet();
		int length = ctAlphabet.length();
		ModuloArithmetik moduloArithmetik = new ModuloArithmetik(ctAlphabet.length());

		for (int i = 0; i < knownPlaintext.length(); i++) {
			char pc = knownPlaintext.charAt(i);
			char cc = ct.charAt(i + offset);
			long diff = moduloArithmetik.getModulo(cc - pc + length);

			builder.append(ctAlphabet.charAt((int) diff));
		}

		char[] key = new char[keylength];
		for (int i = 0; i < key.length; i++) {
			key[i] = ctAlphabet.charAt(0);
		}
		int keyrest = offset % keylength;
		for (int i = 0; i < builder.length(); i++) {
			key[(i + keyrest) % keylength] = builder.charAt(i);
		}

		return new String(key);
	}

	public BestN<Object> doBrute(String ct, Language language) throws Exception {
		vigenere.setAlphabet(getCtAlphabet());
		BestN<Object> bestN = new BestN<Object>();
        bestN.setMax(200);
		for (int n = 1; n <= maxN; n++) {
			bestN.add(brute(ct, language, n));
		}
		return bestN;
	}

	public BestN<Object> brute(String ct, Language language, int keyLength) throws Exception {
		BestN<Object> bestN = new BestN<Object>();
        bestN.setMax(200);
		int length = getCtAlphabet().length();
		BaseArithmetik baseArithmetik = new BaseArithmetik(length);
		int[] key = new int[keyLength];
		long maxN = 1;

		for (int i = 0; i < keyLength; i++) {
			maxN *= length;
		}

		for (long i = 0; i < maxN; i++) {
			baseArithmetik.increase(key);
			String pt = vigenere.decrypt(ct, key);
			int fitness = Text.calculateFitness(pt, language);
			bestN.put(fitness, new Information(pt, toString(key)));
		}
		return bestN;
	}

	@Override
	public String getName() {
		return "Vigenere";
	}

	public String toString(int[] key) {
		StringBuilder builder = new StringBuilder(key.length);
		String alphabet = getCtAlphabet();
		for (int i : key) {
			builder.append(alphabet.charAt(i));
		}
		return builder.toString();
	}

	public void setMaxN(int max) {
		maxN = max;
	}
}
