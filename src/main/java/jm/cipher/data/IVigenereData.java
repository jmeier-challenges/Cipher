package jm.cipher.data;

/**
 * User: T05365A
 * Date: 30.07.12
 * Time: 16:07
 */
public interface IVigenereData {
    String getKey();
	void setKey(String akey);

    void setAlphabet(String alphabet);
    String getAlphabet();
}
