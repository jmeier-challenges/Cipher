package jm.cipher.data;

/**
 * User: johann
 * Date: 17.10.12
 * Time: 14:28
 */
public interface IAtbashData {

    void setAlphabet(String alphabet);
    String getAlphabet();

}
