package jm.cipher.data;

/**
 * User: T05365A
 * Date: 27.07.12
 * Time: 10:28
 */
public interface IMorseData {
    String getDit();
    String getDah();
    String getSpace();
}
