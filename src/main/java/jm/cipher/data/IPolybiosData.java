package jm.cipher.data;

/**
 * User: T05365A
 * Date: 01.10.12
 * Time: 10:18
 */
public interface IPolybiosData {
	void setKey(String key);
	String getKey();

	void setPtAlphabet(String alphabet);
	String getPtAlphabet();

	void setN(int n);
	int getN();

	void setCtAlphabet(String alphabet);
	String getCtAlphabet();

	void setReverse(boolean reverse);
	boolean isReverse();

	void setEqualChars(String equalChars);
	String getEqualChars();
}
