package jm.cipher.data;

/**
 * User: T05365A
 * Date: 06.11.12
 * Time: 09:45
 */
public interface IHandyData {
	enum HANDY_TYPE {NUMBER, REPEATED_NUMBER};
	HANDY_TYPE getType();
	void setType(HANDY_TYPE type);
}
