package jm.cipher.data;

/**
 * User: T05365A
 * Date: 10.08.12
 * Time: 14:16
 */
public interface IConvertNumberData {
    String getFromDigits();
    void setFromDigits(String digits);

    String getToDigits();
    void setToDigits(String digits);
}
