package jm.cipher.data;

/**
 * User: T05365A
 * Date: 01.10.12
 * Time: 12:32
 */
public interface IADFGXData {
	void setKey1(String key1);
	String getKey1();

	void setKey2(String key2);
	String getKey2();

	void setPtAlphabet(String alphabet);
	String getPtAlphabet();

	void setCtAlphabet(String alphabet);
	String getCtAlphabet();
}
