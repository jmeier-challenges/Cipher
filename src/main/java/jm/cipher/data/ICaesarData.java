package jm.cipher.data;

/**
 * User: T05365A
 * Date: 30.07.12
 * Time: 09:30
 */
public interface ICaesarData {

    void setAlphabet(String alphabet);
    String getAlphabet();

    void setPreserveCase(boolean b);
    boolean isPreserverCase();

	void setKey(String aKey);
	String getKey();
}
