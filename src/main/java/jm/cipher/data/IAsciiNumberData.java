package jm.cipher.data;

/**
 * User: T05365A
 * Date: 30.07.12
 * Time: 14:10
 */
public interface IAsciiNumberData {
    String getFromDigits();
}
