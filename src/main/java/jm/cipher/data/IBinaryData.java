package jm.cipher.data;

/**
 * User: T05365A
 * Date: 03.08.12
 * Time: 09:24
 */
public interface IBinaryData {
    String getCoder();
}
