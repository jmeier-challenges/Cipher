package jm.cipher.data;

/**
 * User: johann
 * Date: 13.10.12
 * Time: 11:48
 */
public interface IBrailleData {
    boolean isBinary();

    void setBinary(boolean binary);
}
