package jm.cipher.data;

/**
 * User: T05365A
 * Date: 30.07.12
 * Time: 15:53
 */
public interface IPlayfairData {
    String getKey();
}
