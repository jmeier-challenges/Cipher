package jm.cipher.data;

/**
 * User: T05365A
 * Date: 30.07.12
 * Time: 14:42
 */
public interface IBaconData {
    String getAlphabet();
    String getType();
    boolean getSwap();
    String getRangeA();
    String getRangeB();

}
