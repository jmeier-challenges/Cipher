package jm.cipher.data;

/**
 * User: T05365A
 * Date: 30.07.12
 * Time: 13:47
 */
public interface IAsciiBinaryData {
    String getBitLength();
    void setBitLength(String bitLength);
}
