package jm.cipher.data;

/**
 * User: johann
 * Date: 09.09.12
 * Time: 17:07
 */
public interface IAffineData {
    void setA(int a);
    int getA();
    void setB(int b);
    int getB();
    void setPtAlphabet(String alphabet);
    String getPtAlphabet();

    void setCbc(boolean b);
    boolean isCbc();

    void setIv(int iv);
    int getIv();
}
