package jm.cipher.data;

/**
 * User: T05365A
 * Date: 30.07.12
 * Time: 15:14
 */
public interface IHillData {
    String getPtAlphabet();
    void setPtAlphabet(String alphabet);

    String getKey();
    void setKey(String key);

    void setKeyMatrix(long [][] keyMatrix);
    long[][] getKeyMatrix();

}
