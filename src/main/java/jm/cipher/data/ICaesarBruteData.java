package jm.cipher.data;

/**
 * User: T05365A
 * Date: 30.07.12
 * Time: 14:29
 */
public interface ICaesarBruteData {
    String getAlphabet();
}
