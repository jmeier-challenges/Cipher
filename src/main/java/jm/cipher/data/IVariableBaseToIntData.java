package jm.cipher.data;

import java.util.List;

public interface IVariableBaseToIntData {
    List<String> getBaseChars();
}
