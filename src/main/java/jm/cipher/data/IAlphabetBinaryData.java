package jm.cipher.data;

/**
 * User: T05365A
 * Date: 30.07.12
 * Time: 13:20
 */
public interface IAlphabetBinaryData {
    String getAlphabet();
    String getBitLength();
}
