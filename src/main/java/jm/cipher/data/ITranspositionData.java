package jm.cipher.data;

/**
 * User: johann
 * Date: 12.08.12
 * Time: 12:40
 */
public interface ITranspositionData {
    void setKey(String key);
    String getKey();

    void setKeyLength(int keyLength);
    int getKeyLength();

    void setBlockwise(boolean blockwise);
    boolean isBlockwise();

    void setAlphabet(String alphabet);
    String getAlphabet();
}
