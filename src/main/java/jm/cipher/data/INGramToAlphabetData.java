package jm.cipher.data;

/**
 * User: T05365A
 * Date: 07.08.12
 * Time: 15:18
 */
public interface INGramToAlphabetData {
    String getDelimiters();
    int getN();
    boolean useDelimiters();
}
