package jm.cipher.data;

/**
 * User: T05365A
 * Date: 30.07.12
 * Time: 13:36
 */
public interface IAlphabetNumberData {
    public String getAlphabet();
    public String getFromDigits();
}
