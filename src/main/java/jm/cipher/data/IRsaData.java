package jm.cipher.data;

/**
 * User: johann
 * Date: 13.10.12
 * Time: 18:57
 */
public interface IRsaData {
    enum PT_TYPE {ASCII, ALPHABET, NUMBER}
    void setP(long p);
    long getP();

    void setQ(long q);
    long getQ();

    void setE(long e);
    long getE();

    void setN(long n);
    long getN();

    void setType(PT_TYPE type);
    PT_TYPE getType();

    void setPrivate(boolean p);
    boolean isPrivate();

    void setAlphabet(String alphabet);
    String getAlphabet();
}
