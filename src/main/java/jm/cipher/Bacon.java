package jm.cipher;

import jm.cipher.data.IBaconData;
import jm.tools.Text;

import java.util.HashMap;
import java.util.Map;

public class Bacon extends AbstractCipher {
    public static Map<String, IBaconChar> types;
    static {
        initializeTypes();
    }

	public static String IJ_UV_ALPHABET = "abcdefghiklmnopqrstuwxyz";
	public static String ALPHABET = "abcdefghijklmnopqrstuvwxyz";

	private IBaconChar baconChar;
	private String alphabet = ALPHABET;
	private AlphabetBinary alphabetBinary = new AlphabetBinary();

    private IBaconData data;

	public static void main(String[] args) {
		Bacon bacon = new Bacon();
		IBaconChar baconChar = new ABBaconChar();
		baconChar.setSwap(true);
		bacon.setBaconChar(baconChar);
		System.out.println(bacon.decrypt("ABBABBBABBBBAAAABBBAABAAABBAABABABBABBAAABAAABBAABBBABBBAABBBBBBBBBBABBBAAAABBAAABAAABAABBBBABBBABBBBAABAABAAABBBBBABABBBBABAABBABABBAAABBBABBABBBBBBABBABBBAABABBBAABBABBABBBAABAAABBAAABBBBBBBBABBABABBBABBABBBAABAAABBBAABBABBBABBBBAABABBABBABAAABABAABAAABBBABBABBABABABBBAABABBAABABAAABABAABBBBBABABBABBAABBABBABBAAABAAABAAAABABBBBAAABBBBBBBBBBBBAAABBBABABBBAABBBAABBBBABAAABBAABAABAAAABAAAAABABBABABABAABABAABABABBABBBABAABAABAABAABABABABABBABABAABABBABAABAABBABBABABABABBABABABABBABBABAABABABABABAABABABABAAABABAABABABAABABBABBABAABABABBAABABABAABBABBAABAABBABBAABBABBABABABABAABABABABABBABABABABBABBABABABABBAABABABABAABBABBABBABABBABABAABABBABABAABBABBABABBABABABBABAABBABABABABBAABAABABABABABAABABABABABABABABAABBABABABBAABABABBABBAAABBBABBABABAABAABABABABABBABABABBABABAABBABABABABABABABABABABABABBAABAABABAABABABAABBABABABAABABBAAABAABABABABABABABABABBAABABABABBABAAABABBABABABAABABABABBAABABBABAABBABABABBABBABABABAABABABABABBABABAABBABABABBABBABABABBABAABABABBABBABABABAABABABABABABABAABAABABBABABABABBABBAABABBABBABBABABBABABABABABBABABABBABABABAAABABABABAABABABABABAABABABABABAABABABAABAABABAABABABAABABBABAABABABBABBABABABABABBABABABAABBAABABBAAABAABAABABAABBABABBABBABABABABBAABBABABABABABBABABBAABAABBABABABABBABABABABABABAABABABABABAABBABABABAABABABAABAABABABAABBABBABABBABABAABAABBABAABBABABBABBABABABABABBABABAABABABAABBABABABABABBABABAABBABABBABABABAABABAABABABAABAABBAABAABABAABAABABBABBABABABAAABABABBABABABBAABABBABBABAABABABA"));

		bacon.setAlphabet(ALPHABET);
		baconChar = new UpperLowerBaconChar();
		bacon.setBaconChar(baconChar);
		System.out.println(bacon.decrypt("BacOn's cIpher OR THe baCOnIAN ciPHeR iS a mEthOD Of STEgaNOgraPhy (a MEthod of hidiNg a sECREt mESSaGE As OPposeD to a True cIPhER) dEVIsed by FrAncis BaCOn. a MeSsaGE Is coNceAled in tHe pReseNTaTion OF teXt, rAtheR ThAN Its CONtent. to enCodE a MessAge, Each LEtTER of tHE plAintExt is REpLacEd bY a GROuP oF FiVE Of thE leTteRs 'A' oR 'b'. thIS rEplACeMeNT Is DoNE accorDiNg tO thE AlpHabEt oF THe BACoNIAN cIpher, SHOwn below. note: a SECond VeRsioN Of baCOn's ciPhER UseS A uNIqUE COdE FOR EaCh lEtTeR. iN OtHeR WoRdS, i aNd j eAcH HaS ItS OwN PaTtErN. tHe wRiTeR MuSt mAkE UsE Of tWo dIfFeReNt tYpEfAcEs fOr tHiS CiPhEr. AfTeR PrEpArInG A FaLsE MeSsAgE WiTh tHe sAmE NuMbEr oF LeTtErS As aLl oF ThE As aNd bS In tHe rEaL, sEcReT MeSsAgE, tWo tYpEfAcEs aRe cHoSeN, oNe tO RePrEsEnT As aNd tHe oThEr bS. tHeN EaCh lEtTeR Of tHe fAlSe mEsSaGe mUsT Be pReSeNtEd iN ThE ApPrOpRiAtE TyPeFaCe, AcCoRdInG To wHeThEr iT StAnDs fOr aN A Or a b. To dEcOdE ThE MeSsAgE, tHe rEvErSe mEtHoD Is aPpLiEd. EaCh 'TyPeFaCe 1' LeTtEr iN ThE FaLsE MeSsAgE Is rEpLaCeD WiTh aN A AnD EaCh 'TyPeFaCe 2' LeTtEr iS RePlAcEd wItH A B. tHe bAcOnIaN AlPhAbEt iS ThEn uSeD To rEcOvEr tHe oRiGiNaL MeSsAgE. aNy mEtHoD Of wRiTiNg tHe mEsSaGe tHaT AlLoWs tWo dIsTiNcT RePrEsEnTaTiOnS FoR EaCh cHaRaCtEr cAn bE UsEd fOr tHe bAcOn cIpHeR. bAcOn hImSeLf pRePaReD A BiLiTeRaL AlPhAbEt[2] FoR HaNdWrItTeN CaPiTaL AnD SmAlL LeTtErS WiTh eAcH HaViNg tWo aLtErNaTiVe fOrMs, OnE To bE UsEd aS A AnD ThE OtHeR As b. ThIs wAs pUbLiShEd aS An iLlUsTrAtEd pLaTe iN HiS De aUgMeNtIs sCiEnTiArUm (ThE AdVaNcEmEnT Of lEaRnInG). BeCaUsE AnY MeSsAgE Of tHe rIgHt lEnGtH CaN Be uSeD To cArRy tHe eNcOdInG, tHe sEcReT MeSsAgE Is eFfEcTiVeLy hIdDeN In pLaIn sIgHt. ThE FaLsE MeSsAgE CaN Be oN AnY ToPiC AnD ThUs cAn dIsTrAcT A PeRsOn sEeKiNg tO FiNd tHe rEaL MeSsAgE."));
		System.out.println(bacon.decrypt("BacOnscIpherORTHebaCOnIANciPHeRiSamEthODOfSTEgaNOgraPhyaMEthodofhidiNgasECREtmESSaGEAsOPposeDtoaTruecIPhERdEVIsedbyFrAncisBaCOnaMeSsaGEIscoNceAledintHepReseNTaTionOFteXtrAtheRThANItsCONtenttoenCodEaMessAgeEachLEtTERoftHEplAintExtisREpLacEdbYaGROuPoFFiVEOfthEleTteRsAoRbthISrEplACeMeNTIsDoNEaccorDiNgtOthEAlpHabEtoFTHeBACoNIANcIpherSHOwnbelownoteaSECondVeRsioNOfbaCOnsciPhERUseSAuNIqUECOdEFOREaChlEtTeRiNOtHeRWoRdSiaNdjeAcHHaSItSOwNPaTtErNtHewRiTeRMuStmAkEUsEOftWodIfFeReNttYpEfAcEsfOrtHiSCiPhErAfTeRPrEpArInGAFaLsEMeSsAgEWiThtHesAmENuMbEroFLeTtErSAsaLloFThEAsaNdbSIntHerEaLsEcReTMeSsAgEtWotYpEfAcEsaRecHoSeNoNetORePrEsEnTAsaNdtHeoThErbStHeNEaChlEtTeROftHefAlSemEsSaGemUsTBepReSeNtEdiNThEApPrOpRiAtETyPeFaCeAcCoRdInGTowHeThEriTStAnDsfOraNAOrabTodEcOdEThEMeSsAgEtHerEvErSemEtHoDIsaPpLiEdEaChTyPeFaCeLeTtEriNThEFaLsEMeSsAgEIsrEpLaCeDWiThaNAAnDEaChTyPeFaCeLeTtEriSRePlAcEdwItHABtHebAcOnIaNAlPhAbEtiSThEnuSeDTorEcOvErtHeoRiGiNaLMeSsAgEaNymEtHoDOfwRiTiNgtHemEsSaGetHaTAlLoWstWodIsTiNcTRePrEsEnTaTiOnSFoREaChcHaRaCtErcAnbEUsEdfOrtHebAcOncIpHeRbAcOnhImSeLfpRePaReDABiLiTeRaLAlPhAbEtFoRHaNdWrItTeNCaPiTaLAnDSmAlLLeTtErSWiTheAcHHaViNgtWoaLtErNaTiVefOrMsOnETobEUsEdaSAAnDThEOtHeRAsbThIswAspUbLiShEdaSAniLlUsTrAtEdpLaTeiNHiSDeaUgMeNtIssCiEnTiArUmThEAdVaNcEmEnTOflEaRnInGBeCaUsEAnYMeSsAgEOftHerIgHtlEnGtHCaNBeuSeDTocArRytHeeNcOdInGtHesEcReTMeSsAgEIseFfEcTiVeLyhIdDeNInpLaInsIgHtThEFaLsEMeSsAgECaNBeoNAnYToPiCAnDThUscAndIsTrAcTAPeRsOnsEeKiNgtOFiNdtHerEaLMeSsAgE"));

		baconChar = new CharRangeBaconChar();
		bacon.setBaconChar(baconChar);
	}

	public Bacon() {
		alphabetBinary = new AlphabetBinary();
		alphabetBinary.setBitLength(5);

        setAlphabet(IJ_UV_ALPHABET);
        setBaconChar(new ABBaconChar());
	}

    private static void initializeTypes() {
        types = new HashMap<String, IBaconChar>();
        types.put("AB", getABBaconChar());
        types.put("CharRange", getCharRangeBaconChar());
        types.put("UpperLower", getUpperLowerBaconChar());
    }

    public static String swap(final String ct) {
		StringBuffer swapped = new StringBuffer();
		for (int i = 0; i < ct.length(); i++) {
			char c = Character.toUpperCase(ct.charAt(i));
			if (c == 'A') {
				swapped.append('B');
			} else {
				swapped.append('A');
			}
		}
		return swapped.toString();
	}

	public static IBaconChar getABBaconChar() {
		return new ABBaconChar();
	}

	public static IBaconChar getUpperLowerBaconChar() {
		return new UpperLowerBaconChar();
	}

	public static IBaconChar getCharRangeBaconChar() {
		return new CharRangeBaconChar();
	}

	public String encrypt(String pt) {
        initializeData();
        String stripped = Text.getStripped(pt).toLowerCase();
		String ct = alphabetBinary.encrypt(stripped);
		return binaryToBacon(ct);
	}

	public String decrypt(String ct) {
        initializeData();
		String et = baconToBinary(Text.getStripped(ct));
		return alphabetBinary.decrypt(et);
	}

    public void initializeData() {
        if (data != null) {
            setAlphabet(data.getAlphabet());
            IBaconChar ibc = types.get(data.getType());

            if (ibc instanceof CharRangeBaconChar) {
                ((CharRangeBaconChar) ibc).setAChars(data.getRangeA());
                ((CharRangeBaconChar) ibc).setBChars(data.getRangeB());
            } else if (ibc instanceof ABBaconChar) {
            } else if (ibc instanceof UpperLowerBaconChar) {
            }
            ibc.setSwap(data.getSwap());

            setBaconChar(ibc);
        }
    }

    public String getDisplayname() {
        return "Bacon";
    }

    public String getName() {
        return "Bacon";
    }

    public String getDescription() {
        return "Bacon (muss nochmal überprüft werden)";
    }

    public void setIData(Object data) {
        setData((IBaconData) data);
    }

    private void setData(IBaconData data) {
        this.data = data;
    }

    private String binaryToBacon(final String text) {
		StringBuffer pt = new StringBuffer();
		for (int i = 0; i < text.length(); i++) {
			char c = text.charAt(i);
			if ('0' == c) {
				pt.append('A');
			} else if ('1' == c) {
				pt.append('B');
			} else {
				pt.append(c);
			}
		}
		return pt.toString();
	}

	private String baconToBinary(final String text) {
		StringBuffer bt = new StringBuffer();
		for (int i = 0; i < text.length(); i++) {
			char c = text.charAt(i);
			if (isBaconA(c)) {
				bt.append('0');
			} else if (isBaconB(c)) {
				bt.append('1');
			} else {
				//bt.append(c);
			}
		}
		return bt.toString();
	}

	private boolean isBaconA(char c) {
		return getBaconChar().isBaconA(c);
	}

	private boolean isBaconB(char c) {
		return getBaconChar().isBaconB(c);
	}

	public String getAlphabet() {
		return alphabet;
	}

	public void setAlphabet(final String alphabet) {
		this.alphabet = alphabet;
		alphabetBinary.setAlphabet(alphabet);
	}

	public IBaconChar getBaconChar() {
		return baconChar;
	}

	public void setBaconChar(final IBaconChar baconChar) {
		this.baconChar = baconChar;
	}
}
