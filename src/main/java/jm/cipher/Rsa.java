package jm.cipher;

import jm.cipher.data.IRsaData;
import jm.math.ModuloArithmetik;
import jm.tools.Text;

/**
 * User: johann
 * Date: 13.10.12
 * Time: 17:46
 */
public class Rsa extends AbstractCipher {
    private IRsaData data;
    private boolean isPrivate;


    public static void main(String[] args) throws Exception {
        Rsa rsa = new Rsa();

        int p = 17;
        int q = 11;
        int e = 7;
        int n = p * q;
        rsa.setE(e);
        rsa.setP(p);
        rsa.setQ(q);
        rsa.setN(n);
        rsa.setType(IRsaData.PT_TYPE.ASCII);

        rsa.setPrivate(false);
        rsa.setType(IRsaData.PT_TYPE.NUMBER);
        String pt = "88 66";
        String ct = rsa.encrypt(pt);
        System.out.println(ct);
        System.out.println(rsa.decrypt(ct));

    }

    @Override
    public String encrypt(String pt) {
        try {
            checkPrecondition();
            StringBuilder ct = new StringBuilder();
            String[] lines = pt.split("\n");

            for (String line : lines) {
                if (getType() == IRsaData.PT_TYPE.NUMBER) {
                    String[] words = line.split(" ");
                    for (String word : words) {
                        long pi;
                        try {
                            pi = Long.parseLong(word);
                        } catch (NumberFormatException ex) {
                            pi = -1;
                        }
                        if (pi < 0) {
                            ct.append(word);
                        } else {
                            ct.append(encrypt(pi)).append(" ");
                        }
                    }
                } else {
                    for (char c : line.toCharArray()) {
                        long pi = getValue(c);
                        if (pi < 0) {
                            ct.append(c).append(" ");
                        } else {
                            ct.append(encrypt(pi)).append(" ");
                        }
                    }
                }
                ct.append("\n");
            }
            return ct.toString();
        } catch (Exception ex) {
            return ex.toString();
        }
    }

    private long getD() {
        ModuloArithmetik moduloArithmetik = new ModuloArithmetik((getP() - 1) * (getQ() - 1));
        long d = moduloArithmetik.getInverse(getE());
        return d;
    }

    public Rsa() {
        setIData(new RsaData());
        setAlphabet(Text.LC_ALPHABET);
        setType(IRsaData.PT_TYPE.ASCII);
    }


    private void checkPrecondition() throws Exception {
        long n_1 = (getP() - 1) * (getQ() - 1);
        long ggt = ModuloArithmetik.ggt(n_1, getE());
        if (ggt > 1) {
            throw new Exception("e und (p-1)*(q-1) sind nicht teilerfremd");
        }
    }

    private long encrypt(long pi) {
        long ci;
        long n = getP() * getQ();
        ModuloArithmetik moduloArithmetik = new ModuloArithmetik(n);

        long e = getE();
        if (isPrivate()) {
            e = getD();
        }

        ci = moduloArithmetik.pow(pi, e);
        return ci;
    }

    private int getValue(char c) {
        int i = -1;
        switch (getType()) {
            case ASCII:
                i = c & 0xff;
                break;
            case ALPHABET:
                i = getAlphabet().indexOf(c);
            case NUMBER:
                break;
            default:
                throw new RuntimeException("Unknown type: " + getType());
        }
        return i;
    }

    @Override
    public String decrypt(String ct) throws Exception {
        StringBuilder pt = new StringBuilder();
        long d = getD();
        long e = getE();
        ModuloArithmetik moduloArithmetik = new ModuloArithmetik(getN());

        String[] lines = ct.split("\n");
        for (String line : lines) {
            String[] words = line.split(" ");
            for (String word : words) {
                try {
                    long cl = Long.parseLong(word);
                    long pl;
                    if (isPrivate()) {
                        pl = moduloArithmetik.pow(cl, e);
                    } else {
                        pl = moduloArithmetik.pow(cl, d);
                    }
                    switch (getType()) {
                        case ASCII:
                            pt.append((char) pl);
                            break;
                        case NUMBER:
                            pt.append(Long.toString(pl)).append(" ");
                            break;
                        case ALPHABET:
                            String alphabet = getAlphabet();
                            if (pl < alphabet.length()) {
                                pt.append(alphabet.charAt((int) pl));
                            } else {
                                pt.append(Long.toString(pl));
                            }
                            break;
                        default:
                            throw new RuntimeException("Unknown type: " + getType());
                    }
                } catch (NumberFormatException ex) {
                    pt.append(word).append(" ");
                }
            }
        }

        return pt.toString();
    }

    @Override
    public String getDisplayname() {
        return "Rsa";
    }

    @Override
    public String getName() {
        return "Rsa";
    }

    @Override
    public String getDescription() {
        return "Rsa";
    }

    public void setType(IRsaData.PT_TYPE type) {
        data.setType(type);
    }

    public IRsaData.PT_TYPE getType() {
        return data.getType();
    }

    public long getP() {
        return data.getP();
    }

    public void setP(long p) {
        data.setP(p);
    }

    public long getQ() {
        return data.getQ();
    }

    public void setQ(long q) {
        data.setQ(q);
    }

    public long getE() {
        return data.getE();
    }

    public void setE(long e) {
        data.setE(e);
    }

    public long getN() {
        return data.getN();
    }

    public void setN(long n) {
        data.setN(n);
    }

    @Override
    public void setIData(Object data) {
        this.data = (IRsaData) data;
    }

    @Override
    public void setMemento(Memento memento) {
        if (memento instanceof RsaData) {
            RsaData rsaData = (RsaData) memento;
            setE(rsaData.getE());
            setN(rsaData.getN());
            setP(rsaData.getP());
            setQ(rsaData.getQ());
            setAlphabet(rsaData.getAlphabet());
            setType(rsaData.getType());
        }
    }

    @Override
    public Memento getMemento() {
        RsaData memento = new RsaData();
        memento.setE(getE());
        memento.setN(getN());
        memento.setP(getP());
        memento.setQ(getQ());
        memento.setAlphabet(getAlphabet());
        memento.setType(getType());
        return memento;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setPrivate(boolean isPrivate) {
        this.isPrivate = isPrivate;
    }

    public void setAlphabet(String alphabet) {
        data.setAlphabet(alphabet);

    }
    public String getAlphabet() {
        return data.getAlphabet();
    }

    class RsaData extends Memento implements IRsaData {
        long p, q;
        long e, n;
        private PT_TYPE type;
        private String alphabet;

        @Override
        public void setP(long p) {
            this.p = p;
        }

        @Override
        public long getP() {
            return p;
        }

        @Override
        public void setQ(long q) {
            this.q = q;
        }

        @Override
        public long getQ() {
            return q;
        }

        @Override
        public void setE(long e) {
            this.e = e;
        }

        @Override
        public long getE() {
            return e;
        }

        @Override
        public void setN(long n) {
            this.n = n;
        }

        @Override
        public long getN() {
            return n;
        }

        @Override
        public void setType(PT_TYPE type) {
            this.type = type;
        }

        @Override
        public PT_TYPE getType() {
            return type;
        }

        @Override
        public void setPrivate(boolean p) {
            isPrivate = p;
        }

        @Override
        public boolean isPrivate() {
            return isPrivate;
        }

        @Override
        public void setAlphabet(String alphabet) {
            this.alphabet = alphabet;
        }

        @Override
        public String getAlphabet() {
            return alphabet;
        }
    }
}
