package jm.cipher;

import jm.cipher.data.IAffineData;
import jm.tools.Text;

import java.util.ArrayList;
import java.util.List;

/**
 * User: T05365A
 * Date: 07.09.12
 * Time: 10:40
 */
public class Affine extends AbstractCipher implements IAffineData {
	private String ptAlphabet = Text.LC_ALPHABET;

	private int a; // 1, 3, 5, 7, 9, 11, 15, 17, 19, 21, 23 oder 25
	private int b; // eine beliebige Zahl von 0 bis 25
	private int iv = 0;
	private int currentIv = 0;
	private boolean cbc = false;
    private IAffineData data;

	public static void main(String[] args) throws Exception {
		//Affine cipher = new Affine();
	}

    @Override
	public void setIv(int iv) {
		this.iv = iv;
		this.cbc = true;
	}

    @Override
    public int getIv() {
        return iv;
    }

    @Override
	public String encrypt(String pt) throws Exception {
        StringBuilder ct = new StringBuilder(pt.length());
        if (isValidA(data.getA())) {
            currentIv = data.getIv();
            String stripped = Text.getStripped(pt, data.getPtAlphabet());
            for (char pc : stripped.toCharArray()) {
                int ipc = data.getPtAlphabet().indexOf(pc);
                int cc;
                if (data.isCbc()) {
                    cc = getModulo(ipc ^ currentIv);
                    cc = data.getA() * cc + data.getB();
                    cc = getModulo(cc);
                    currentIv = cc;
                } else {
                    cc = data.getA() * ipc + data.getB();
                    cc = getModulo(cc);
                }

                ct.append(data.getPtAlphabet().charAt(cc));
            }

        } else {
            ct.append("A must be "). append(getValidAs());
        }

        return ct.toString();
	}

	@Override
	public String decrypt(String ct) throws Exception {
		currentIv = data.getIv();
        StringBuilder pt = new StringBuilder(ct.length());

        if (isValidA(data.getA())) {
            for (char cc : ct.toCharArray()) {
                int icc = data.getPtAlphabet().indexOf(cc);
                int pc;
                if (data.isCbc()) {
                    pc = getAInverse() * (icc - data.getB());
                    pc = getModulo(pc);
                    pc = getModulo(pc ^ currentIv);
                    currentIv = icc;
                } else {
                    pc = getAInverse() * (icc - data.getB());
                    pc = getModulo(pc);
                }
                pt.append(data.getPtAlphabet().charAt(pc));
            }
        } else {
            pt.append("A must be ").append(getValidAs());
        }
		return pt.toString();
	}

	private int getModulo(int pc, int length) {
		while (pc < 0) {
			pc += length;
		}
		return pc % length;
	}

	private int getModulo(int pc) {
		return getModulo(pc, data.getPtAlphabet().length());
	}

	private int getAInverse() {
		return getInverse(data.getA());
	}

	@Override
	public String getDisplayname() {
		return "Affine Cipher";
	}

	@Override
	public String getName() {
		return "Affine";
	}

	@Override
	public String getDescription() {
		return "Affine Cipher";
	}

	@Override
	public String getPtAlphabet() {
		return ptAlphabet;
	}

	@Override
	public void setPtAlphabet(String alphabet) {
		ptAlphabet = alphabet;
	}

	public int getA() {
		return a;
	}

	public void setA(int a) {
		this.a = a;
	}

	public int getB() {
		return b;
	}

	public void setB(int b) {
		this.b = b;
	}

	public int ggt(int a, int b) {
		while (b > 0) {
			int rest = a % b;
			a = b;
			b = rest;
		}
		return a;
	}

	public int[] extendedGcd(int a, int b) {
		int[] auv = new int[3];
		int x = 0, lastx = 1;
		int y = 1, lasty = 0;

		while (b > 0) {
			int quotient = a / b;
			int rest = a % b;
			a = b;
			b = rest;

			int x1 = lastx - quotient * x;
			lastx = x;
			x = x1;

			int y1 = lasty - quotient * y;
			lasty = y;
			y = y1;
		}

		auv[0] = a;
		auv[1] = lastx;
		auv[2] = lasty;

		return auv;
	}

	public List<Integer> getValidAs() {
		List<Integer> validAs = new ArrayList<Integer>();
		for (int i = 0; i < data.getPtAlphabet().length(); i++) {
			if (ggt(data.getPtAlphabet().length(), i) == 1) {
				validAs.add(i);
			}
		}
		return validAs;
	}

    public boolean isValidA(int a) {
        return getValidAs().contains(a);
    }

	private Integer getInverse(Integer a) {
		int[] egcd = extendedGcd(data.getPtAlphabet().length(), a);
		return getModulo(egcd[2], data.getPtAlphabet().length());
	}

    @Override
    public void setCbc(boolean b) {
       cbc = b;
    }

    @Override
    public boolean isCbc() {
        return cbc;
    }

    @Override
    public void setMemento(Memento memento) {
        AffineMemento affineMemento = (AffineMemento) memento;
        data.setA(affineMemento.a);
        data.setB(affineMemento.b);
        data.setIv(affineMemento.iv);
        data.setCbc(affineMemento.cbc);
        data.setPtAlphabet(affineMemento.alphabet);
    }

    @Override
    public Memento getMemento() {
        AffineMemento memento = new AffineMemento();
        memento.a = data.getA();
        memento.b = data.getB();
        memento.iv = data.getIv();
        memento.cbc = data.isCbc();
        memento.alphabet = data.getPtAlphabet();
        return memento;
    }

    public Affine() {
        setIData(this);
    }

    @Override
    public void setIData(Object data) {
        this.data = (IAffineData) data;
    }

    class AffineMemento extends Memento {
        public int a,b,iv;
        public boolean cbc;
        public String alphabet;
    }

}
