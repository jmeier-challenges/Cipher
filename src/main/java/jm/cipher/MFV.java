package jm.cipher;

/**
 * User: T05365A
 * Date: 10.07.12
 * Time: 11:12
 */
public class MFV extends AbstractCipher {
    private String alphabet = "1234567890ABCD*#";
    private int[] code = { 1906, 2033, 2174, 1979, 2106, 2247, 2061, 2188, 2329, 2277, 2330, 2403, 2485, 2574, 2150, 2418
    };

    public String encrypt(String pt) throws Exception {
        StringBuffer ct = new StringBuffer();
        for (char pc : pt.toCharArray()) {
            int cc = getTone(pc);
            ct.append(Integer.toString(cc) + " ");
        }
        return ct.toString();
    }

    private int getTone(char c) {
        int pos = alphabet.indexOf(c);
        if (pos < 0) {
            return c;
        } else {
            return code[pos];
        }
    }

    private char getChar(int tone) {
        int pos = -1;
        for (int i = 0; i < code.length; i++) {
            if (code[i] == tone) {
                pos = i;
                break;
            }
        }

        if (pos < 0) {
           return '?';
        } else {
            return alphabet.charAt(pos);
        }
    }

    public String decrypt(String ct) throws Exception {
        StringBuffer pt = new StringBuffer();
        // find integer of length 4

        int pos = 0;
        for (int i = 0; i < ct.length(); i++) {
            String number = getSubstring(ct,i);
            try {
                int tone = Integer.parseInt(number);
                pt.append(getChar(tone));
                i+=3;
            } catch (NumberFormatException ex) {
                pt.append(number.charAt(0));
            }
        }

        return  pt.toString();
    }

    public String getDisplayname() {
        return "MFV";
    }

    public String getName() {
        return "MFV";
    }

    public String getDescription() {
        return "Wandelt die Summe der Frequenzen in die zugehörigen Zeichen/Taste um." +
                "Es werden nur vierstellige Zahlen umgewandelt";
    }

    private String getSubstring(String ct, int i) {
        if (i + 4 < ct.length()) {
           return ct.substring(i, i + 4);
        } else {
           return ct.substring(i);
        }
    }

    public static void main(String[] args) throws Exception {
        MFV MFV = new MFV();
        String message = "1234r560ABCD*";
        String ct = "2(2033) 2(2174) 1(1979) 3(1979) 2(2033) 1(2174) 3(1979) 3(2033) 2(1979) 4(2329) 2(2188) 2(2247) 1(1979) 1(2329) 1(2150) 1(2033) 1(2247) 1(2418) 1(1906) 1(2061) 1(2329) 2(2174) 1(2277) 1(1906) 1(2277) 1(2150) 1(2106) 1(2329) 1(2418) 1(2174) 1(2188) 1(2188) 1(2174) 3(2247) 3(2061) 1(2188) 1(2247) 2(2188) 4(2061) 4(2061) 1(2188) 1(2174) 2(2188) 2(1979) 3(2247) 3(2033) 2(1979)";
        String pt = MFV.decrypt(ct);

        System.out.println(ct);
        System.out.println(pt);
    }
}
