package jm.cipher;

/**
 * User: T05365A
 * Date: 27.08.12
 * Time: 14:27
 */
public class Trithemius extends AbstractCipher {
	private Vigenere vigenere = new Vigenere();

	public Trithemius() {
		vigenere.setKey("abcdefghijklmnopqrstuvwxyz");
	}

	@Override
	public String encrypt(String pt) throws Exception {
		return vigenere.encrypt(pt);
	}

	@Override
	public String decrypt(String ct) throws Exception {
		return vigenere.decrypt(ct);
	}

	@Override
	public String getDisplayname() {
		return "Trithemius";
	}

	@Override
	public String getName() {
		return "Trithemius";
	}

    @Override
	public String getDescription() {
		return "Trithemius-Chiffre ist eine Vigenere-Chiffre mit festem Schluessel (abcdefghijklmnopqrstuvwxyz)";
	}
}
