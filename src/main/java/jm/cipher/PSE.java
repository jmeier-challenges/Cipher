package jm.cipher;

/**
 * User: T05365A
 * Date: 26.07.12
 * Time: 17:26
 *
 *
 * http://www.blinde-kuh.de/sprachen/zahlen1-10.html
 */
public class PSE extends AbstractCipher {
    private String[] elements = {
            "H", "He", "Li", "Be", "B", "C", "N", "O", "F", "Ne",
            "Na", "Mg", "Al", "Si", "P", "S", "Cl", "Ar", "K", "Ca",
            "Sc", "Ti", "V", "Cr", "Mn", "Fe", "Co", "Ni", "Cu", "Zn",
            "Ga", "Ge", "As", "Se", "Br", "Kr", "Rb", "Sr", "Y", "Zr",
            "Nb", "Mo", "Tc", "Ru", "Rh", "Pd", "Ag", "Cd", "In", "Sn",
            "Sb", "Te", "I", "Xe", "Cs", "Ba", "La", "Ce", "Pr", "Nd",
            "Pm", "Sm", "Eu", "Gd", "Tb", "Dy", "Ho", "Er", "Tm", "Yb",
            "Lu", "Hf", "Ta", "W", "Re", "Os", "Ir", "Pt", "Au", "Hg",
            "Tl", "Pb", "Bi", "Po", "At", "Rn", "Fr", "Ra", "Ac", "Th",
            "Pa", "U", "Np", "Pu", "Am", "Cm", "Bk", "Cf", "Es", "Fm",
            "Md", "No", "Lr", "Rf", "Db", "Sg", "Bh", "Hs", "Mt", "Ds",
            "Rg", "Uub", "Uut", "Uuq", "Uup", "Uuh", "Uus", "Uuo" };

    private String separators = "[ ,/]";


    public String encrypt(String pt) throws Exception {
        return "not yet implemented";
    }

    public String decrypt(String ct) throws Exception {
        StringBuffer pt = new StringBuffer();
        String[] rows = ct.split("\n");

        for (String row : rows) {
            String[] numbers = row.split(separators);
            for (String number : numbers) {
                if (number.isEmpty()) {
                    pt.append(" ");
                } else {
                    int ord = Integer.parseInt(number);
                    pt.append(elements[ord - 1]);
                }
            }
            pt.append("\n");
        }

        return pt.toString();
    }

    public String getDisplayname() {
        return "PSE";
    }

    public String getName() {
        return "PSE";
    }

    public String getDescription() {
        return "Nur Entschlüsselung möglich! Ordnungszahlen der Elemente werden in die Abkürzung ungewandelt. 1 = H, 2 = He, usw.";
    }

    @Override
    public boolean canEncrypt() {
        return false;
    }

    public static void main(String[] args) throws Exception {
        PSE pse = new PSE();

        String ct = "21/1/13/76/6/1\n6/68/8/\n1/89/1/53";
        System.out.println(pse.decrypt(ct).toLowerCase());
    }
}
