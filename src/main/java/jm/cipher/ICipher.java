package jm.cipher;

/**
 * User: johann
 * Date: 25.04.11
 * Time: 17:51
 */
public interface ICipher {
    String encrypt(String pt) throws Exception;

    String decrypt(String ct) throws Exception;

    void initializeData();

    String getDisplayname();
    String getName();
    String getDescription();

    void setIData(Object data);

    boolean canEncrypt();
    boolean canDecrypt();

    Memento getMemento();

    void setMemento(Memento memento);
}
