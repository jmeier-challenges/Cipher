package jm.cipher;

import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class VariableBaseToIntTest {

    @Test
    public void mult() {
        List<String> baseChars = Collections.singletonList("01");
        baseChars = Arrays.asList("ab", "012", "01");
        assertEquals(1, VariableBaseToInt.getMult(0, baseChars));
        assertEquals(2, VariableBaseToInt.getMult(1, baseChars));
        assertEquals(6, VariableBaseToInt.getMult(2, baseChars));
    }

    @Test
    public void decrypt() {
        List<String> baseChars = Collections.singletonList("01");

        //assertEquals(4, VariableBaseToInt.decrypt("100", baseChars));
        //assertEquals(5, VariableBaseToInt.decrypt("101", baseChars));

        baseChars = Arrays.asList("012", "01");
        assertEquals(5, VariableBaseToInt.decrypt("21", baseChars));
        assertEquals(1, VariableBaseToInt.decrypt("1", baseChars));
        assertEquals(2, VariableBaseToInt.decrypt("10", baseChars));

        baseChars = Arrays.asList("1234", "abcdef", "1234");
        int result = 0 * 6 * 4 + 0 * 4 + 2;
        assertEquals(1, VariableBaseToInt.decrypt("1a2", baseChars));
        result = 1 * 6 * 4 + 1 * 4 + 0;
        assertEquals(28, VariableBaseToInt.decrypt("2b1", baseChars));

        result = 2 * 6 * 4 + 0 * 4 + 1;
        System.out.println(result);
        assertEquals(result, VariableBaseToInt.decrypt("3a2", baseChars));

        baseChars = Arrays.asList("1234", "abcdef", "01234");
        result = 1 * 6 * 5 + 1 * 5 + 1;
        assertEquals(result, VariableBaseToInt.decrypt("2b1", baseChars));

    }

    @Test
    public void getBaseChars() {
        List<String> baseChars = Arrays.asList("01", "012");

        assertEquals("01", VariableBaseToInt.getBaseChars(0, baseChars));
        assertEquals("012", VariableBaseToInt.getBaseChars(1, baseChars));
        assertEquals("", VariableBaseToInt.getBaseChars(-1, baseChars));
        //assertEquals("", VariableBaseToInt.getBaseChars(5, baseChars));
        assertEquals("", VariableBaseToInt.getBaseChars(0, null));

        baseChars = Collections.singletonList("01");
        assertEquals("01", VariableBaseToInt.getBaseChars(1, baseChars));
    }
    @Test
    public void testIntToBase() {
        System.out.println(intToBase(347, 4));
    }

    public static String intToBase(int i, int base) {
        int m;
        StringBuilder result = new StringBuilder();
        while ((m = i / base) > 0) {
            int r = i % base;
            result.insert(0, r);
            i = m;
        }
        result.insert(0, i % base);
        return result.toString();
    }

    String ct =
            "a3a 3a2 b1a 1a2 a2c 2a2 d1f 2b1 c2a " +
            "4a2 a3b 2b1 a1b 1a2 d1f 3a1 c2a 1d2 " +
            "b1a 1b1 f3e 2d1 a3b 2a3 b2a 3a3 a2a " +
            "4a3 b1b 2b1 d1f 2b2 b2a 4a3 b1c 1f3 " +
            "a1c 2a1 d2a 1b1 a3b 2b1 f2c 2a2 a1d " +
            "2d1 a2a 2c2 a2c 2b1 a1a 2a3 b2b 3a2 " +
            "b1a 1b1 f2a 1b1 a3b 2b3 a1c 1f2 b2b " +
            "3b1 b2a 4a4 a1b 2b1 a1a 2a3 b3b 2a2 " +
            "a1b 1a2 a2c 2a1 b1a 2c2 a2a 4a2 a3a ";

    @Test
    public void lessThanHex1() {
        String alphaChars = "abcdef";
        String numberChars = "1234";

        StringBuilder result = new StringBuilder();
        Map<String, Integer> counts = new HashMap<>();
        StringBuilder number = new StringBuilder();
        for (char c : ct.toCharArray()) {
            if (c == ' ') {
                Integer count = counts.getOrDefault(number.toString(), 0)  + 1;
                counts.put(number.toString(), count);
                result.append(number);
                result.append(c);
                number = new StringBuilder();
            } else if (alphaChars.contains(String.valueOf(c))) {
                number.append(alphaChars.indexOf(c));
            } else if (numberChars.contains(String.valueOf(c))) {
                number.append(numberChars.indexOf(c));
            }
        }

        System.out.println(result);
        System.out.println(counts);
    }
/*
    @Test
    public void lessThanHex1() {
        // 243 chars, Teiler (1, 3, 9, 27, 81, 243)
        String abc = "abcdef";
        String num = "1234";
        List<String> baseCharsNumber = Arrays.asList(num, abc, num);
        List<String> baseCharsAbc = Arrays.asList(abc, num, abc, num, abc, num, abc, num, abc, num, abc, num, abc, num, abc, num, abc, num, abc, num);

        long result = VariableVariableBaseToInt.decrypt("a3a3a2b1a", baseCharsAbc);
        result = VariableVariableBaseToInt.decrypt("a2a4a2a3a", baseCharsAbc);
        System.out.printf("%x\n", result);
        result = VariableVariableBaseToInt.decrypt("c2a1b1a2c2a2a4a2a3a", baseCharsAbc);
        System.out.printf("%x\n", result);
        result = VariableVariableBaseToInt.decrypt("c2a1b1a2c2a2a4a2a3a", baseCharsAbc);
        System.out.printf("%x\n", result);

        System.out.printf("%x\n", VariableVariableBaseToInt.decrypt("aaabacaaaaa", Arrays.asList(abc)));
        System.out.printf("%x\n", VariableVariableBaseToInt.decrypt("01", Arrays.asList("01")));
        System.out.printf("%x\n", VariableVariableBaseToInt.decrypt("01", Arrays.asList("10")));
    }

    @Test
    public void lessThanHex() {
        // 243 chars, Teiler (1, 3, 9, 27, 81, 243)
        String abc = "abcdef";
        String num = "1234";
        List<String> baseCharsNumber = Arrays.asList(num, abc, num);
        List<String> baseCharsAbc = Arrays.asList(abc, num, abc);
        int count = 0;
        Map<String, Integer> counts = new HashMap<>();
        List<String> numbers = new ArrayList<>();
        List<String> abcs = new ArrayList<>();

        for (String number : ct.split(" ")) {
            count += 3;
            if (abc.contains(Character.toString(number.charAt(0)))) {
                long result = VariableVariableBaseToInt.decrypt(number, baseCharsAbc);
                abcs.add(number);
                //System.out.print(result + " ");
            } else {
                long result = VariableVariableBaseToInt.decrypt(number, baseCharsNumber);
                numbers.add(number);
                System.out.print(result + " ");
            }

            int cow = counts.getOrDefault(number, 0);
            cow++;
            counts.put(number, cow);
        }

        System.out.println();
        //System.out.println(abcs);
        System.out.println(numbers);

        System.out.println();
        System.out.println("count: " + count);
        System.out.println(counts);
    }
 */
}
