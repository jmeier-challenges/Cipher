package jm.cipher;

import jm.cipher.data.IMorseData;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class MorseTest {

    @Test
    public void getBinaryString() {
        assertEquals("00000000", Morse.getBinaryString(0));
        assertEquals("11111111", Morse.getBinaryString(255));
        assertEquals("0000000100000000", Morse.getBinaryString(256));
    }

    @Test
    public void twoBitsToMorse() {
        // Todo
    }

    @Test
    public void onesToMorse() {
        assertEquals(".", Morse.onesToMorse(Arrays.asList(1), true));
        assertEquals("-", Morse.onesToMorse(Arrays.asList(3), true));
        assertEquals(".-", Morse.onesToMorse(Arrays.asList(11), true));
    }
}
