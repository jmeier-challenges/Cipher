package cipher;

import jm.cipher.Caesar;
import jm.cipher.dataimpl.CaesarData;
import jm.tools.Text;
import org.junit.Before;
import org.junit.Test;

/**
 * User: johann
 * Date: 28.10.12
 * Time: 12:07
 */
public class CaesarTest {
    private Caesar caesar;

    @Before
    public void setUp() throws Exception {
        caesar = new Caesar();
    }

    //@Test
    public void testEncrypt() throws Exception {
        caesar.setAlphabet(Text.LC_ALPHABET);
        caesar.setKey("t");

        caesar.setPreserveCase(false);
        String pt = "Wolfgang ist ein toller Kerl!";
        String ct = caesar.encrypt(pt);
        String ctCorrect = "Wifzauha cmn ych niffyl Kylf!";
        assert(ct.equals(ctCorrect));

        caesar.setKey("w");
        caesar.setPreserveCase(true);
        ct = caesar.encrypt(pt);
        ctCorrect = "Tlicdxkd fpq bfk qliibo Hboi!";
        assert(ct.equals(ctCorrect));
    }

    //@Test
    public void testDecrypt() throws Exception {
        caesar.setAlphabet(Text.LC_ALPHABET);

        caesar.setPreserveCase(true);
        caesar.setKey("w");
        String ct = "Tlicdxkd fpq bfk qliibo Hboi!";
        String pt = caesar.decrypt(ct);
        String ptCorrect = "Wolfgang ist ein toller Kerl!";
        assert (ptCorrect.equals(pt));

        caesar.setPreserveCase(false);
        caesar.setKey("t");
        ct = "Wifzauha cmn ych niffyl Kylf!";
        pt = caesar.decrypt(ct);
        assert (ptCorrect.equals(pt));
    }

    //@Test
    public void testGetMemento() throws Exception {
        String alphabet = Text.UC_ALPHABET;
        String key = "T";
        boolean preserveCase = false;

        caesar.setAlphabet(alphabet);
        caesar.setPreserveCase(false);
        caesar.setKey("T");

        CaesarData memento = (CaesarData) caesar.getMemento();
        assert (alphabet.equals(memento.getAlphabet()));
        assert (key.equals(memento.getKey()));
        assert (preserveCase == memento.isPreserverCase());
    }

    //@Test
    public void testSetMemento() throws Exception {
        String alphabet = Text.UC_ALPHABET;
        String key = "T";
        boolean preserveCase = false;
        CaesarData memento = new CaesarData();
        memento.setAlphabet(alphabet);
        memento.setKey(key);
        memento.setPreserveCase(preserveCase);

        caesar.setMemento(memento);
        assert (alphabet.equals(caesar.getAlphabet()));
        assert (key.equals(caesar.getKey()));
        assert (preserveCase == caesar.isPreserverCase());
    }

    //@Test
    public void testShift() throws Exception {
        caesar.setAlphabet(Text.LC_ALPHABET);
        caesar.setKey("a");
        assert (caesar.getShift() == 1);

        caesar.setKey("z");
        assert (caesar.getShift() == 0);

        caesar.setShift(0);
        assert ("z".equals(caesar.getKey()));

        caesar.setKey("%");
        System.out.println(caesar.getShift());

        caesar.setPreserveCase(false);
        caesar.setKey("a");
        assert (caesar.getShift() == 1);
        caesar.setKey("A");
        assert (caesar.getShift() != 1);

        caesar.setPreserveCase(true);
        caesar.setKey("a");
        assert (caesar.getShift() == 1);
        caesar.setKey("A");
        assert (caesar.getShift() == 1);
    }
}
